# -*- coding: utf-8 -*-
# add parent directory so that mongo, parse,files can be accessed
import sys
import os
import numpy as np

from sklearn.feature_extraction import DictVectorizer
from sklearn.preprocessing import Imputer
from sklearn.preprocessing import LabelEncoder

sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "/..")

import files
import parse
import mongo

NUMERICAL = 0
CATEGORICAL = 1

# generic join function
def performJoin(project_id, probes, patient_ids, subject_mapping_columns):

    clinical_by_patients, clinical_feature_names = loadClinicalByPatient(project_id)


    microarray_by_patients, microarray_feature_names = transformMicroarrayToPatient(probes, patient_ids)

    subject_mapping = getMapping(project_id, subject_mapping_columns)

    integrated_by_patient = joinMicroToClinical(microarray_by_patients, clinical_by_patients, subject_mapping)

    return integrated_by_patient, microarray_feature_names + clinical_feature_names

def getRemainingProbes(all_probes, remaining_keys):
    remainder = {}
    # copy the remaining keys over
    for key in remaining_keys:
        remainder[key] = all_probes[key]
    return remainder

# takes a dictionary of {probe_name: [probe_values]}, 'unzips' it and then returns the probe_values, transposed so that each column corresponds to the probe values of one one probe, and the names
def microToX(probes):
    X_t = probes.values()
    X = map(list, zip(*X_t))
    return X, probes.keys()

def loadClinicalByPatient(project_id):
    c_file = files.loadClinical(project_id)
    patients, feature_names = parse.getClinicalByPatient(c_file)
    return patients, feature_names

# takes probes, which is {p1: [v11,v12,v13]}, and patient_ids [id1,id2,id3 etc.], returns them to be indexed by patient_ids, and a probe_id vector [p1, p2, p3]
def transformMicroarrayToPatient(probes, patient_ids):

    X_t = probes.values()
    X = map(list, zip(*X_t))

    microarray_patients = {}

    for index, features in enumerate(X):
        microarray_patients[patient_ids[index]] = X[index]

    return microarray_patients, probes.keys()

# gets the dictionary that performs patient mapping from microarray to clinical
def getMapping(project_id, columns_to_use):
    m_file = files.loadMapping(project_id)
    m_index = columns_to_use['m_index']
    c_index = columns_to_use['c_index']
    mapping = dict(parse.extractColumns(m_file, m_index, c_index))
    return mapping

def joinMicroToClinical(micro, clinical, mapping):

    join_result = []

    #micro LEFT JOIN clinical:
    for key, microarray_features in micro.iteritems():
        try:
            clinical_key = mapping[key]
            clinical_features = clinical[clinical_key]
            join_result.append(microarray_features + clinical_features)
        except Exception as inst:
            # not available; that's fine, ignore it
            print inst
    
    return join_result

def splitIntegratedFeatures(integrated, index):
    n_in = np.array(integrated)
    return n_in[:, :index], n_in[:, index:]

def splitIntegratedFeatureNames(feature_names, index):
    return feature_names[:index], feature_names[index:]

def replaceMissingNumbersWithNan(values):
    def replaceWithNan(v):
        try:
            number = float(v)
            return number
        except Exception as inst:
            return np.nan

    return [replaceWithNan(v) for v in values]

def replaceMissingValuesWithNan(values, missing_values):
    def replaceWithNan(v):
        if v in missing_values:
            return np.nan
        else:
            return v
    return [replaceWithNan(v) for v in values]

def castToFloat(values):
    def ignoreNan(v):
        if v is np.nan:
            return v
        else:
            # otherwise, return nan if you've missed a 'missing' value
            try:
                ret = float(v)
            except:
                ret = np.nan
            return ret
    return [ignoreNan(v) for v in values]


def imputeMode(y):
    le = LabelEncoder()
    le.fit(y)

    y_labelled = le.transform(y)

    # get nan encoding to use as imputation, if nan is present
    try:
        nan_encoded = le.transform([np.nan])
    except:
        nan_encoded = np.nan

    imp = Imputer(missing_values = nan_encoded, strategy = 'most_frequent', axis = 1)

    # Fit imputer with the data
    #imp.fit(new_features)

    # Impute y with missing values:
    imputed = imp.fit_transform(y_labelled)

    # need to strip outer list, then convert back to array
    imputed_norm = [int(i) for i in imputed[0]]

    # back to categorical features:
    features_reversed = list(le.inverse_transform(imputed_norm))

    return features_reversed

    # wrapped in an array, so strip it. then, convert imputation into ints
    #return [int(i) for i in imp.fit_transform(y_labelled)[0]]

def imputeMean(y):
    # impute along x axis (row)
    imp = Imputer(missing_values = 'NaN', strategy = 'mean', axis = 1)
    # Impute those values:
    return imp.fit_transform(y)

def createMap(ranked, missing_values):
    mapping = {}

    #first, convert ranked into a dict:
    for key,value in ranked.iteritems():
        mapping[key] = value

    for m in missing_values:
        mapping[m] = np.nan

    return mapping

def dictVectoriser(fdict):
    vec = DictVectorizer()
    converted = vec.fit_transform(fdict).toarray()
    f_names = vec.get_feature_names()
    return f_names, converted

def toValueDict(values, feature_name):
    return [{feature_name: v} for v in values]

def concatenateNumpies(feature_arr):
    np_concated = feature_arr.pop(0).transpose()
    for f in feature_arr:
        np_concated = np.concatenate((np_concated, f.transpose()), axis=1)

    return np_concated

def handleGroundTruth(gt, user_missing, mappings, gt_type):

    missing_values = user_missing + ['']

    # deal with mappings if specified in categorical ground truth:
    if mappings:
        mapping_dict = createMap(mappings, missing_values)
        gt = [mapping_dict[v] for v in gt]

    # replace missing values with nan, then impute missing values:
    if gt_type == CATEGORICAL:
        gt_missing_replaced = replaceMissingValuesWithNan(gt, missing_values)
        gt_imputed = imputeMode(gt_missing_replaced)
    elif gt_type == NUMERICAL:
        gt_missing_replaced = replaceMissingNumbersWithNan(gt)
        gt_imputed = imputeMean(gt_missing_replaced)[0]
    else:
        print 'yikes, not handled!'

    return gt_imputed

# a beefed up version of handleGroundTruth, which deals with integrating the clinical data as well. Returns preprocessed_clinical, preprocessed_names, and y and y_name
def prepareIntegratedClinical(clinical, c_names, clinical_instructions, missing_values):

    def getClinicalValues(c_name):
        index = c_names.index(c_name)
        return clinical[:, index]

    # comprises of {name: '', action: '', ranked: {(can be null)}}
    def handleClinicalFeature(clin):
        name = clin['name']
        values = getClinicalValues(name)
        action = clin['action']

        try:
            ranked = clin['ranked']
        except:
            pass

        if action == "Numeric":
            values = replaceMissingNumbersWithNan(values)
            values = castToFloat(values)
            values = np.array([values])
            values = imputeMean(values)
            return [name], values
        elif action == "Multiclass":
            values = replaceMissingValuesWithNan(values, missing_values)
            values = imputeMode(values)
            return [name], [values]
        elif action == "Binary":
            values_replaced = replaceMissingValuesWithNan(values, missing_values)
            #print features_missing
            values_imputed = imputeMode(values_replaced)
            # make dict
            features_as_arr_of_dicts = toValueDict(values_imputed, name)
            f_names, features_encoded = dictVectoriser(features_as_arr_of_dicts)
            return f_names, features_encoded.transpose()
        elif action == "Manually assign":
            mapping_dict = createMap(ranked, missing_values)
            values = [mapping_dict[v] for v in values]
            values = np.array([values])
            values = imputeMean(values)
            return [name], values

        # there; the three possibilities are covered now.

    # contains
    feature_names = []
    # contains 2D array of features:
    feature_arr = []
    # Missing values:
    missing_values = missing_values + ['']

    # handle ground truth first:
    names, values = handleClinicalFeature(clinical_instructions['ground_truth'])
    y_name = names[0]
    y = values[0]

    num_values = len(y)
    #feature_arr = [np.array([]*num_values)]

    for feature in clinical_instructions['to_integrate']:

        names, values = handleClinicalFeature(feature)
        feature_arr.append(values)
        feature_names += names

    try:
        pre_clinical = concatenateNumpies(feature_arr)
    except:
        pre_clinical = np.array([[] for i in xrange(num_values)])

    return pre_clinical, feature_names, y, y_name

def concatMicroAndClinical(M, m_names, C, c_names):
    names_concated = m_names + c_names
    return np.array(np.concatenate((M, C), axis=1), order='F'), names_concated
