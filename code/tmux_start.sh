#!/bin/bash

SESSIONNAME="indiv"
tmux has-session -t $SESSIONNAME &> /dev/null

if [ $? != 0 ] 
 then
    tmux new-session -s $SESSIONNAME -n indiv -d

	tmux rename-window 'server'
    tmux send-keys -t $SESSIONNAME "source ~/code/imperial/individual_project/code/env/bin/activate" C-m 
    tmux send-keys -t $SESSIONNAME "cd ~/code/imperial/individual_project/code/server" C-m 
    tmux send-keys -t $SESSIONNAME "python server.py" C-m 

	tmux new-window -n 'sass'
	tmux send-keys -t $SESSIONNAME "cd ~/code/imperial/individual_project/code/server/static/css" C-m 
	tmux send-keys -t $SESSIONNAME "source ~/.rvm/scripts/rvm" C-m 
    tmux send-keys -t $SESSIONNAME "sass --watch .:." C-m 

	tmux new-window -n 'jstest'
	tmux send-keys -t $SESSIONNAME "cd ~/code/imperial/individual_project/code/server/tests" C-m 
	tmux send-keys -t $SESSIONNAME "../node_modules/karma/bin/karma start" C-m 

	tmux new-window -n 'python'
    tmux send-keys -t $SESSIONNAME "source ~/code/imperial/individual_project/code/env/bin/activate" C-m 
	tmux send-keys -t $SESSIONNAME "cd ~/code/imperial/individual_project/code/server/routes" C-m 
fi

tmux attach -t $SESSIONNAME
