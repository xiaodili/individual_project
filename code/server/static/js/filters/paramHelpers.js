angular.module('app.filters')
.filter('paramToName', function() {
    var paramLookup = {
        'MAD': 'Median Absolute Variance',
        'VarianceThreshold': 'Low variance',
        'Univariate': 'Univariate filtering',
        'Embedded': 'Embedded feature selection',

        'f_classif': 'ANOVA',
        'f_regression': 'Linear Regression',

        'percentile': 'Percentile',
        'k_best': 'K best',
        'fpr': 'False Positive Rate',
        'fdr': 'False Discovery Rate',
        'fwe': 'Family-wise Error',

        'rfe': 'Recursive feature elimination',
        'lasso': 'LASSO',

        'linear': 'Linear'
    };

    return function(input) {
        return paramLookup[input];
    }
})
.filter('paramToDescription', function() {
    var paramLookup = {
        'MAD': 'Performs a median absolute deviation filter on the microarray data. This is a robust measure of variability of a univariate sample of quantitative data.',
        'VarianceThreshold': 'VarianceThreshold is a simple baseline approach to feature selection. It removes all features whose variance doesn’t meet some threshold. By default, it removes all zero-variance features, i.e. features that have the same value in all samples.',
        'Univariate': 'Univariate feature selection by filtering works by selecting the best features based on univariate statistical tests. It can be seen as a preprocessing step to an estimator. Scikit-learn exposes feature selection routines as objects that implement the transform method.',
        'Embedded': 'Embedded feature selection result are computationally expensive methods but select features according to model classification performance.',

        'f_classif': 'Computes the analysis of variance',
        'f_regression': 'Fits the sum of least squares',

        'percentile': 'Returns the top percentile of features.',
        'k_best': 'K best',
        'fpr': 'False Positive Rate',
        'fdr': 'False Discovery Rate',
        'fwe': 'Family-wise Error',

        'rfe': 'Recursively eliminates features to maximise model performance.',
        'lasso': 'Fits the sum of least squares using the L1 regularisation norm.'

    };
    return function(input) {
        return paramLookup[input];
    }
})
.filter('paramToResultSrc', function() {
    var paramLookup = {
        'MAD': 'partials/selection_results/mad.html',
        'VarianceThreshold': 'partials/selection_results/lowvariance.html',
        'Univariate': 'partials/selection_results/univariate.html',
        'Embedded': 'partials/selection_results/embedded.html'
    }
    return function(input) {
        return paramLookup[input];
    }
})
.filter('paramToSelectionSrc', function() {
    var paramLookup = {
        'MAD': 'partials/selection/mad.html',
        'VarianceThreshold': 'partials/selection/lowvariance.html',
        'Univariate': 'partials/selection/univariate.html',
        'Embedded': 'partials/selection/embedded.html'
    }
    return function(input) {
        return paramLookup[input];
    }
});
