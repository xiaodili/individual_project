# Abstract and motivation

## Stuff to talk about in the background

Basic taxonomy (classification) of feature selection techniques

* During last decade, FS has shifted from illustrative example to becoming a real prerequisite for model building.
* High dimensional nature of microarrays.
* In contrast to other dimensionality reduction techniques, feature selection does not alter original representation of the variables, but only selects a subset of them - thus preserving the original semantics of variables and therefore offering the advantage of interpretability by a domain expert.

Motivation:
a) Avoid overfitting and improve model performance
b) to provide faster and more cost-effective models
c) to gain a deeper insight into underlying process that generated the data

### Filter Methods

Only need to be performed once, then different classifiers can be evaluated
Scale easily to high-dimensional datasets
Computationally simple and fast
Independent of classification algorithm

However, each feature is considered separately - feature dependencies are ignored. May lead to worse classification performance.

Multivariate filter techniques were introduced instead.

#### univariate

Simple and effective - high dimensionality, univariate filter methods are most relevant:

* Output provided is intuitive and easy to understand
* Output of genes can be further explored in literature - don't take into account gene interactions
* Extra computation time required by multivariate gene selection techniques

(chi squared, Euclidean distance, i-test, information gain)

Model-free (Wilcoxon rank sum)

* fast, scalable, independent of the classifier.
* ignores feature dependencies, ignores interaction with the classifier.

#### multivariate techniques

(correlation-based feature selection, markov blanket filter, fast correlation-based feature selection) 

Won't discuss them.

### Wrapper

Higher risk of overfitting than filter techniques, and very computationally intensive, especially if building classifier has high computational cost.

Not in sklearn...

* Simulated annealing
* Randomized hill climbing
* Genetic algorithms
* Estimation of distribution algorithms

### Embedded

* Interacts with the classifier
* Better computational complexity than wrapper methods
* Models feature dependencies

#### LASSO

* L1 regularisation - penalty term which encourages sum of squares of parameters to be small.
* Least absolute shrinkage and selection operator
* Only used in linear regression

#### Recursive feature elimination

Assess relevance of features by looking at intrinsic properties of the data. Relevance score is calculated.

* Wrapper Methods
* Embedded methods

**Advisable practice: pre-reduce seach space using a univariate filter method, then apply wrapper/embedded method - fit computation time to available resources.**

GenePattern

### False Discovery Rate

A statistical method used in multiple hypothesis testing to correct for multiple comparisons - controls expected propotion of incorrectly rejected hypothesis. Greater power, but increased rate of type I errors.

q-value - FDR analogue for the p-value. Q-value of individual hypothesis test is the minimum FRD at which the test may be called significant.

### Family-wise error rate

Even more stringent control over false discovery than compared to the False discovery rate. Reduces the probabily of even one false discovery, as opposed to expected proportion of false discoveries.

### False-positive rate

