\chapter{Results and evaluation}

A walkthrough on how a typical workflow may be conducted, and discussion on how the tool can be extended with additional functionality will now take place.
%Despite calculating and improving classification rates not being the primary goal of the project, the first step of project was to perform a supervised learning algorithm on the integrated dataset of ovarian carcinoma; this is in order to gain insight into the steps involved in preprocessing the data and the computation. The results of the classification are described later on \ref{chap:Preliminary Results}.

\section{Workflow example}

The dataset from TCGA Colon Adenocarcinoma, curated by tranSMART, will be used to demonstrate how a typical workflow might go like. (This dataset and a few others from tranSMART will be made available at \texttt{https://gitlab.doc.ic.ac.uk/xdl12/identifying-biomarkers/tree/master}).

From the main page, the project name is entered \ref{fig:enter_name}. When \textbf{Create} is clicked, the user is brought to the project workspace page, where the necessary files need to be uploaded.

\begin{figure}
    \centering
    \includegraphics[width=1\textwidth]{pictures/workflow/create_project.png}
    \caption{\label{fig:enter_name}Entering the name of the project}
\end{figure}

\subsection{Uploading}
\label{sub:Uploading}

The relevant files, \texttt{Endo\_raw\_data.txt}, \texttt{Endo\_gene\_expression.txt}, \texttt{Endo\_sample\_subject\_mapping.txt} and \texttt{Endo\_custom\_annotation.txt} are uploaded to the Clinical, Microarray, Subject and Annotation sections respectively using the \textbf{Browse} button. After the files are uploaded, metadata such as the number of features and samples in each file are present, as well as a truncated sample of the first few lines of each file \ref{fig:uploaded}.

\begin{figure}
    \centering
    \includegraphics[width=1\textwidth]{pictures/workflow/uploaded.png}
    \caption{\label{fig:uploaded}Uploaded the relevant files}
\end{figure}

\subsection{Mapping}
\label{sub:Mapping}

Using the sample of the subject mapping file, the correct columns for mapping clinical patient ID and the microarray patient ID formarts are selected. By scanning across the first column of the clinical sample and the first row of the microarray sample, it can be seen that the two appropriate columns are \texttt{SUBJECT\_ID} and \texttt{SAMPLE\_ID}. These are selected, and \textbf{Map} is pressed, with the results showing that 54 out of 54 of the patients were correctly mapped \ref{fig:mapped}.

Similarly with the annotation file, by scanning the first column of the microarray file and recognising that the values are present in the \textbf{PROBE\_ID} column, they can be (redundantly) mapped to \textbf{GENE\_SYMBOL}, or perhaps to \textbf{GENE\_ID}.

%todo: pic here
\begin{figure}
    \centering
    \includegraphics[width=1\textwidth]{pictures/workflow/mapped.png}
    \caption{\label{fig:mapped}Mapped subject and annotation}
\end{figure}

\subsection{Feature selection}
\label{sub:Feature selection}

In the \textbf{Perform New Feature Selection} section, a feature selection technique can be selected. It is recommended that before an embedded method is chosen, a computationally cheaper method is chosen first in order to reduce the amount of features to evaluate (one RFE test run using all the microarray data had to be left runnning overnight for the computation to complete).

MAD is selected, and note that the \textbf{Submit} button is greyed out until a number is entered \ref{fig:mad_picked}.

\begin{figure}
    \centering
    \includegraphics[width=1\textwidth]{pictures/workflow/mad_pick.png}
    \caption{\label{fig:mad_picked}MAD chosen as selection technique}
\end{figure}

The page automatically polls (every 5 seconds) to see if the computation is complete; by scrolling up to the selection results, the user can wait for the results from there \ref{fig:processing}. When the results finally show up, it can be seen that almost two thirds of the features have been eliminated using MAD \ref{fig:mad_done}.

\begin{figure}
    \centering
    \includegraphics[width=1\textwidth]{pictures/workflow/processing.png}
    \caption{\label{fig:processing}MAD analysis taking place}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=1\textwidth]{pictures/workflow/mad_done.png}
    \caption{\label{fig:mad_done}MAD analysis done}
\end{figure}

Two more techniques will be applied for illustrative purposes.

By choosing a univariate method, the distributions and types of values for clinical data can be previewed before making a choice of dependent variable to use. The type of value (e.g. categorial or numerical) will be displayed, with different options and parameter choices available for both. As mentioned before, there is an option to `map' each categorical to numerical data if the clinican believes that it makes sense to assign them as such (For example, it might make sense to assign tumour grades to numbers) and perform a univariate filtering according to linear regression. In this case, the dependent variable chosen will be \texttt{recurred\_progressed} and an ANOVA test will be used (in this case, an ANOVA is likely to be a poor choice of analysis due to the tendency of microarray fold change data to not be normally distributed).

It is also seen that there are `Unknown' values present in the clinical data; by adding `Unknown' to the Missing Values, the selection method will know to impute such values instead of treating it as another category \ref{fig:unifilter}. Finally, the Mode is chosen to be percentile, with the 90th percentile of features selected. Note that only now, with all the mandatory fields are filled in, will the \textbf{Submit} button become active.

\begin{figure}
    \centering
    \includegraphics[width=1\textwidth]{pictures/workflow/unifilter.png}
    \caption{\label{fig:unifilter}Previewing the dependent variable for univariate filtering}
\end{figure}

Finally, an embedded method, recursive feature selection is applied. Here, \texttt{recurred\_progressed} is once again chosen as the dependent variable (ground truth) and the interface is similar to that of univariate selection. It is also possible to include clinical features to use, as it has been seen that these features tend to improve classification rates. Inclusion can be specified by ticking their checkbox, although currently the ability to map categorical data into numerical ones has not been implemented into the user interface yet (they are binarised by default). The \texttt{tumor\_grade} feature will be chosen, and a preview of the data can once again be previewed by hovering over the feature name \ref{fig:rfe_clinical}. Note that only a linear kernel is available to select. After C (the regularisation parameter) and the number of features to keep has been chosen, the user finally clicks \textbf{Submit} to complete the request.

\begin{figure}
    \centering
    \includegraphics[width=1\textwidth]{pictures/workflow/rfe_clinical.png}
    \caption{\label{fig:rfe_clinical}Previewing additional clinical features to use in RFE}
\end{figure}

\subsection{Selection Results}
\label{sub:Selection Results}

By clicking on \textbf{View} for the MAD selection method, details about what parameters were used can be seen, as well as a graph showing the 10 top MAD scores \ref{fig:mad_graph}. Scoring down will display a table with all of the probes and their MAD scores displayed. Clicking on a gene name will activate the Reflect plugin, which causes a pop up with relevant information to appear \ref{fig:mad}.

\begin{figure}
    \centering
    \includegraphics[width=1\textwidth]{pictures/workflow/mad_graph.png}
    \caption{\label{fig:mad_graph}Top 10 features filtered according to MAD}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=1\textwidth]{pictures/workflow/mad.png}
    \caption{\label{fig:mad}All surviving features, ranked by MAD, with the reflect popup enabled}
\end{figure}

Similarly, a graph will display the top 10 genes and their P-values for univariate filtering \ref{fig:univariate_results}. For embedded feature selection, the classification rates with stratified 3-fold cross validation is displayed \ref{fig:rfe}, along with the rank of each feature. Note that any clinical features selected will be present as well \ref{fig:rfe_ranks}. It is also possible to follow the links from the Reflect plugin; clicking on `literature' on the LGR5 marker will be taken to the iHOP page, and a cursory search for `cancer' can bring up any information relevant to it \ref{fig:reflect}.

\begin{figure}
    \centering
    \includegraphics[width=1\textwidth]{pictures/workflow/univariate_results.png}
    \caption{\label{fig:univariate_results}Univariate results, ranked by $-\log \text{p-value}$}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=1\textwidth]{pictures/workflow/rfe_ranks.png}
    \caption{\label{fig:rfe_ranks}Ranked features for RFE (a rank of 1 also means that the feature was selected RFE and cross-validated selection of the best number of features).}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=1\textwidth]{pictures/workflow/rfe.png}
    \caption{\label{fig:rfe}Classification scores of threefold stratified cross validation}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=1\textwidth]{pictures/workflow/reflect.png}
    \caption{\label{fig:reflect}iHOP page on LGR5; looks promising!}
\end{figure}

\section{Integrating a new technique}

Initially, the project relied on scikit learn for feature selection techniques, but it is also possible for new ones to be implemented. Towards the end of the project, MAD was integrated as a further `non-supervised' filtering technique as an alternative to variance threshold, which didn't work as well for non-normal distributions. In order to integrate a new technique, it will be shown how the following issues are addressed:

\begin{itemize}
    \item Decide on an identifier name for the technique
    \item Look and behaviour of its parameter selection interface
    \item Implementation of the computation
    \item Look and behaviour of its results page
    \item Final integration step
\end{itemize}

\subsubsection{Identifier}
\label{ssub:Identifier}

A unique string for the technique is required, so that it can be registered to show up in the dropdown in the feature selection section, and so that the server knows how to handle the technique. \texttt{'MAD'} was chosen in this case.

\subsection{Parameter selection}
\label{sub:Parameter selection}

A new HTML partial its corresponding controller needs to be created. By convention, these should be placed in \texttt{static/partials/selection} and \texttt{static/js/controllers/selection} respectively. The goal of the partial is to provide the user with a helpful interface so that the relevant parameters (just the MAD threshold, in this case) can be picked. Built in browsers cues can be used, e.g. \texttt{required} and \texttt{type='number'} attribute specifies a mandatory input field and its parameter type respectively. 

The controller is the direct child of \texttt{controllers/selectionPickerCtrl.js}, which it must call its \texttt{submit(config)} method in order to send the parameter data to the server \ref{lst:submit}.

\begin{lstlisting}[language=JavaScript,caption={\texttt{selection/madCtrl.js} calling \texttt{submit} in  \texttt{selectionPickerCtrl.js} to send the request to the server},label={lst:submit}]
$scope.submit = function() {
    var params = $scope.config; //{threshold: 1}
    $scope.$parent.submit(params);
}
\end{lstlisting}

\subsection{Computation}
\label{sub:Computation}

In \texttt{routes/subprocesses/ml\_handle.py}, the identifier (\texttt{'MAD'}) is used to decide which function to run \ref{lst:ml_handle}.

\begin{lstlisting}[language=Python,caption={\texttt{ml\_handle.py} deciding which routine to call},label={lst:ml_handle}]
handler = ml_instructions['name']
config = ml_instructions['config']

if handler == 'MAD': # newly added
    features_left = MAD(config, probes)
elif handler == 'VarianceThreshold':
    features_left = varianceThreshold(config, probes)
#...more handlers follow
\end{lstlisting}

The requirement for this function is that it must at least return list of probe names that have been selected in order to maintain the feature selection pipeline. Any additional values returned will be available on the client side as the \texttt{augmented\_results} object.

Arguments that can be passed to the dedicted feature selection function are:

\begin{enumerate}
    \item \texttt{config}: the object holding the parameters chosen by the user.
    \item \texttt{probes}: a dictionary of probes/gene names as keys and a list of expression levels as the values.
    \item \texttt{patient\_ids}: a list of patient IDs, the order of which corresponds to the order in the list of expression levels in \texttt{probes}.
    \item \texttt{project\_id}: in case clinical information needs to be retrieved.
    \item \texttt{subject\_mapping\_columns}: contains the indexes of the columns to use as subject mapping, in case integration is required.
\end{enumerate}

\subsubsection{Testing}
\label{ssub:Testing}

The function can be unit tested to confirm that the correct behaviour is achieved; see \texttt{MADSuite} in \texttt{tests/python\_unit/computation\_tests.py} to see how tests can be integrated.

\subsection{Results}
\label{sub:Results}

A new HTML partial its corresponding controller needs to be created. By convention, these should be placed in \texttt{static/partials/selection\_results} and \texttt{static/js/controllers/selection\_results} respectively. A \texttt{results} object is available for the controller to use to visualise the information \ref{lst:results_object}.

\begin{lstlisting}[language=JavaScript,caption={\texttt{results} object available for the MAD controller and partial to use.},label={lst:results_object}]
{
    augmented_results: Object
    num_eliminated_features: 17205
    remaining_features: Array[609]
    selection_method: {
        name: "MAD",
        config: { // originally had been passed to the server from madCtrl.js
            threshold: 1
        }
}
\end{lstlisting}

One point to keep in mind is that the \texttt{results} object is fetched asynchronously by its parent controller; for any additional formatting that needs to be done with the results, its parent provides a \texttt{results\_ready} event which can be listened out for whenever the results are ready \ref{lst:results_ready}.

\begin{lstlisting}[language=JavaScript,caption={\texttt{selection\_results/madResultCtrl.js} listening for when the results are ready.},label={lst:results_ready}]
$scope.$on('results_ready', function() {
    $scope.formatResult(); //format the newly available $scope.results data in order to plot a graph, etc.
});
\end{lstlisting}

\subsection{Integration}
\label{sub:Integration}

In order for the MAD option to show up in the feature selection picker dropdown list, the identifier \texttt{'MAD'} is added to the \texttt{\$scope.selection\_options} array in \texttt{static/js/controllers/\-selectionPickerCtrl.js} \ref{lst:selection_options}.

Additional information about the technique can be added to the \texttt{filters/paramHelpers.js} file, which uses the identifier to look up extra information about its name and description and, most importantly, where to find the HTML for the parameter selection and results page.

Finally, if new JavaScript files were written (remember that it is usually considered good practice to put nontrivial controllers in their own file), they need to be added to \texttt{index.html} \ref{lst:index_add}.

\begin{lstlisting}[language=HTML,caption={Making sure any new JavaScript files written are included},label={lst:index_add}]
    <!--for selection-->
    <script src="js/controllers/selection/madCtrl.js" type="text/javascript" charset="utf-8"></script>

    <!--...-->

    <!--for displaying the results-->
    <script src="js/controllers/selection_results/madResultCtrl.js" type="text/javascript" charset="utf-8"></script>
\end{lstlisting}

\begin{lstlisting}[language=JavaScript,caption={Adding MAD to the selection options in \texttt{selectionPickerCtrl.js}},label={lst:selection_options}]
$scope.selection_options = [
    'MAD', //added here
    'VarianceThreshold',
    'Univariate',
    'Embedded'
];
\end{lstlisting}
