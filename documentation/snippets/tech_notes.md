# Numpy

Slicing:

http://scipy-lectures.github.io/intro/numpy/numpy.html#indexing-and-slicing

Joining (concatenating):

http://docs.scipy.org/doc/numpy/reference/generated/numpy.concatenate.html

Convert back to regular python list with `tolist()`

# Machine learning

## Regarding non-numerical data
(from scilearn documentation):
http://scikit-learn.org/stable/modules/preprocessing.html
http://www.quora.com/Machine-Learning/How-do-you-feed-categorical-features-into-an-SGD-Regressor-model

Often features are not given as continuous values but categorical. For example a person could have features ["male", "female"], ["from Europe", "from US", "from Asia"], ["uses Firefox", "uses Chrome", "uses Safari", "uses Internet Explorer"]. Such features can be efficiently coded as integers, for instance ["male", "from US", "uses Internet Explorer"] could be expressed as [0, 1, 3] while ["female", "from Asia", "uses Chrome"] would be [1, 2, 1].

Such integer representation can not be used directly with scikit-learn estimators, as these expect continuous input, and would interpret the categories as being ordered, which is often not desired (i.e. the set of browsers was ordered arbitrarily).

One possibility to convert categorical features to features that can be used with scikit-learn estimators is to use a one-of-K or one-hot encoding, which is implemented in OneHotEncoder. This estimator transforms each categorical feature with m possible values into m binary features, with only one active.

# Scraping

From TCGA:

	https://wiki.nci.nih.gov/display/TCGA/Retrieving+Clinical+Data

Seems to be really tricky though. Paul Williams seems to have gotten his collated data from here:

	https://tcga-data.nci.nih.gov/docs/publications/

Whereas most of the data is available here:

	https://tcga-data.nci.nih.gov/tcga/dataAccessDownload.htm

The clinical data that is downloaded has a lot more features than the ones emphasised in the publications.

* There is a 

# Scilearn

For datasets, 

* `digits.target` are the ground truths for the digit dataset
* `digits.data` gives access to the features that can be used to classify digit samples.
* `digits.data` is always a 2D array - n_samples \* n_features.

* To show a picture, use:

	import pylab as pl
	pl.imshow(digits.images[-1], cmap=pl.cm.gray_r)

# Numpy

## reshape

* `reshape(a, newshape)` gives a new shape to an array without changing its data, e.g.:


	arr = numpy.arange(15)
	np.reshape(arr, (3,5))

* This method is also available to the numpy array:

	arr = numpy.arange(15)
	arr.reshape((3,5))

* Therefore, the way to flatten a numpy array is:

	arr = arr.reshape((img, -1))


## unique(ar, return_index=False, return_inverse=False)

Finds the unique elements of an array

Returns the sorted unique elements of an array

# Technical Specifications

## Machine learning techniques to expose to user

* Supervised learning

    * Classification 

        * Binary SVM

    * Regression
        
        * Linear classifiation


* Clustering

    * K-means

* Feature Selection

    * Recursive feature elimination
    * LASSO

## Tech reference

### Python file objects

https://docs.python.org/2.4/lib/bltin-file-objects.html
