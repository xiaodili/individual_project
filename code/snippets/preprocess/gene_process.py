import numpy as np
from helpers import toDict

# private data munging functions for gene data
#------------------------------------

# converts all those strings in scientific notation into regular numbers

# transposed argument, so has an array of [['tcga.04.1336, 4.36E-01, 23E-02,...],..]]
def geneProcess(data):

    def toFloat(f):
        try:
            return float(f)
        except:
            return f

    result = []
    for line in data:
        result.append([toFloat(l) for l in line])

    return result

#------------------------------------

# exposed method:
def gene_process(path):
    with open(path) as contents:
        lines = contents.read().strip().split('\n')

    # needs to be transposed:
    data = [row.strip().split('\t') for row in lines]
    data_transposed = map(list, zip(*data))

    # feature names are therefore the first row, minus the first element (which is CLID)
    feature_names = data_transposed.pop(0)[1:]


    data_ready = geneProcess(data_transposed)

    #gene_data = toDict(data_ready[:12])
    gene_data = toDict(data_ready)

    #return feature_names, gene_data
    return feature_names, gene_data

if __name__ == "__main__":
    path = "data/TCGA_OV_gene_expression.txt"
    feature_names, gene_data = gene_process(path)
    print feature_names[:12]
    print gene_data.keys()
