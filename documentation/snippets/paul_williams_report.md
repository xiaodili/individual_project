# Paul's report

## Abstract

* Clinical studies into disease produces lots of data about complex diseases
* Challenge for translational informatics to store and manage this data
* tranSMART developed as a platform at ICL to manage and analyse these datasets
* Integrate gene expression data produced by TCGA into tranSMART
* Produce some scripts to do it
* Differential expression of gene sets analysed
* fold change
* standard T-Tests
* Benjamini-Hochberg multi-test correction
* Panther Pathway analysis
* Significance of the cellular signaling pathways found - P53, Wnt, Integrin, Cadherin, Hedgehog, Apoptosis pathway
* Differential expression of certain genes associated witrh Luminal/ER+ group of breast cancers and certain known oncogenes within the lung cancer dataset

## Introduction

* Translational informatics - area of bioinformatics that is concerned with storing, managing and analysing vast amounts of data available and continually being generated from medical studies.
 * extract, transform, load process - well defined steps for taking data from external source, transforming it into the required format and loading it into the target data

## Background on cancer

* Most important distinction when classifying tumours is determining whether it is benign or malignant
* Benign tumours grow slowly and do not spread
* Malignant tumours spread fast, invade and destroy nearby normal 13k
* TNM staging system - based on size of primary tumour, whether they have spread to nearby lymph nodes
* More stuff about cancer - lifestyle, genetic, virus, environmental
* 90% of cancer observed to have some type of genetic mutation - small percentage are inherited, rest are sporadic (due to environmental exposure)
* oncogenes, tumour suppresor genes, mismatch repair genes
* goes on about cancers.... jesus


24 pages 

## Microarray Procedure

* Solid surface onto which known DNA molecules have been chemically bonded at specific locations.
* Each location is called a probe - contains many replicates of the same molecule
* Probes are chosen to hybridise (merge complementary strands) with mRNA molecule of a single gene.
* Each array can hold tens of thousands of probes


## Differential gene expression anaylsis and fold ratio
 
* Compares expression levels of a specific gene in one sample from another (e.g. a cancerous cell vs. a non-cancerous cell. (exp_A/exp_B).
* Gene expression data published is already the fold change of the cancerous sample compared to the non-cancerous reference cell used as a control.
* Data published by TCGA is already the fold change

## Statistical Significance Testing

* 2-fold typically considered a worthwhile cutoff, although on its own did not generate reliable and reproducible results, so additional techniques were developed to include empirical Bayes and combinatorial rankings using statistical significance

* T-test - checks if means of two groups are statistically different from each other. Calculates the difference of the means relative to their variance.

* Multiple Test Correction

## Data integration

**prognosis**: a forecast of the likely outcome of the situation
**MAD**: Median absolute deviation - measure the level of disperion about the median.

## Pathway analysis

* pathways significantly involved in cancerous state can be found
