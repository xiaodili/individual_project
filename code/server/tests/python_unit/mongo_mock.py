ML_INSTRUCTIONS = None
MAPPING_COLUMNS = None
RESULTS = None


def _setMLInstructions(inst):
    global ML_INSTRUCTIONS
    ML_INSTRUCTIONS = inst

def _setMappingColumns(mapping):
    global MAPPING_COLUMNS
    MAPPING_COLUMNS = mapping

# default behaviour - use all the probes:
def getActiveProbes(project_id):
	raise Exception("no selection steps have taken place")

def readFromQueue(project_id):
	return ML_INSTRUCTIONS

def loadSubjectMapping(project_id):
	return MAPPING_COLUMNS

def appendToSelectionSteps(project_id, result):
    global RESULTS
    RESULTS = result

def removeFromQueued(project_id):
    global ML_INSTRUCTIONS
    ML_INSTRUCTIONS = None
