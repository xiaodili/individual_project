\documentclass[a4paper]{article}

\usepackage[english]{babel}
%\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage[numbers]{natbib}
\usepackage{graphicx}

\title{Literature Review: Identification of Novel Cancer Biomarkers, Through Integration and Analytics of Cancer Array Data}

\author{Xiao Di Li [xdl12]}

\date{\today}

\begin{document}
\maketitle

\tableofcontents

\begin{abstract}
The aim of this project is to apply machine learning algorithms to clinical and microarray data of cancers (specifically, ovarian carcinoma), and to provide an application to allow scientists to analyse and visualise their results in an intuitive and user friendly manner. In particular, the tool should help discover novel biomarkers and/or therapeutic targets. This literature review describes the research done for the project so far and the proposed project roadmap.

\end{abstract}

\section{Background} % (fold)
\label{sec:Background}

\subsection{Introduction}
\label{sub:Introduction}

Cancer prognosis is traditionally based on the statistics derived from the clinical data of patients that have the same type of cancer. Factor such as the type and location of the cancer, the stage of the disease and the cancer's grade (how abnormal the cancer cells look under the microscope), and genetic properties of the cancer are taken into consideration by the doctor. It is advantageous for doctors and scientists to utilise the large amounts genomic information generated by microarrays, and the statistical techniques from the field of machine learning that can be applied to that information in order to help their prognosis.

Gene expression levels can be measured for tens of thousands of genes in only one experiment using microarray technology. In order to process such information, various statistical techniques can be used to determine which genes correlate with various cancer stages. In the last decade particularly, the application of machine learning to microarray data have reached promising classification rates \cite{comparative_ml}.

Translational informatics is an area of bioinformatics that is concerned with the storing, managing and analysing the data generated from medicine studies, and tranSMART is such a platform that stores and analyses such translational data \cite{transmart}. By combining the data from clinical studies with that which is produced by microarray techniques, it has been shown that prognosis accuracy can be improved further \cite{clinical_improvement}.

This project aims to apply machine learning and statistical techniques to cancer datasets and to devise an interface that allows scientists and doctors to do the same, for prognosis and research purposes. A major feature of this tool would be to help identify new biomarkers. Many current attempts at creating an interface for machine learning have been non-open source, haven't integrated clinical data, or lacking a good interface \cite{GEMS}. This project is complementary to tranSMART, which does provide some statistical analysis, such as P-means and distribution graphs, is currently lacking in machine learning algorithms. If possible, integration into the tranSMART platform will be considered.

A brief overview on the assorted fields of study that are relevant to this project will be given in the next section. After that, the current work done on the project and the proposed implementation details will be discussed. Finally, any challenges that are likely to come up, and then stretch goals of the project will be outlined.

\subsection{Microarray technology} % (fold)
\label{sub:Microarray technology}

A microarray is a collection (up to tens of thousands) of DNA spots attached to a solid surface, allowing the expression level of these genes to be measured simultaneously.

Each array location is known as a probe, and each probe is capable of hybridising (binding) to a particular mRNA. Before hybridisation, the mRNA is treated with a fluorescent dye so that after hybridisation with the probe, the abundance of the gene can be measured as light intensity. The result of the experiment is an array of intensities corresponding to each gene of each patient sample under study.

\begin{figure}
	\centering
	\includegraphics[width=0.7\textwidth]{pictures/cDNA-microarray-experiment.jpg}
	\caption{\label{fig:microarray}A diagram of the microarray procedure.}
\end{figure}

% subsection Microarray technology (end)

\subsection{Machine learning} % (fold)
\label{sub:Machine learning}

Machine learning has been informally described as is a field of study that gives computers the ability to learn without being explicitly programmed\cite{samuels}. It has a high overlap with the field of statistics, with approaches such as linear regression being an elemental technique in both fields. Machine learning techniques can be divided into two types of approaches: supervised and unsupervised.

Supervised learning is the task of `inferring a function from labeled training data' \cite{ml_foundations}. For example, if given a set of clinical information such as tumour size, grade, patient age and treatment outcome (favourable or unfavourable), a supervised learning algorithm might be able to predict the treatment outcome of a new patient if given his existing clinical information. An effective supervised learning algorithm that has been popular in the field of bioinformatics is the Support Vector Machine (SVM) \cite{multicategory_microarray}. 

Unsupervised learning takes a set of `unlabelled' data, and then aims to find any classifications in the data, e.g. if the data can be divided up into any classes. For example, if given a set of images of tumours, an unsupervised learning algorithm may be able to spot two clusters; one consisting of benign and the other of malignant tumours. An example of an unsupervised learning technique is the k-means algorithm.

In order to fine-tune the parameters used for classification, a machine learning algorithm requires data in order to `learn'. This is done by presenting it with a `dataset', in this case the gene expression levels from microarray data, and the clinical information provided by the doctor. Each individual attribute (e.g. expression level for gene symbol OLFM4, gender, age etc.) is known as a `feature'. The algorithm will be able to use this information in order to better classify new data as they are presented. Perennial issues in machine learning are missing data, which is common with datasets that have a high number of features, and what is known as the bias-variance tradeoff.

An outline of the bias-variance tradeoff, feature extraction/reduction techniques and the SVM algorithm will now follow.

\subsubsection{Bias-variance tradeoff} % (fold)
\label{ssub:Bias-variance tradeoff}

A dilemma in machine learning presents itself in the form of how the training data should be used; there is danger of `overfitting' the training data, which means that although the prediction error rate in the training data is minimal, the performance of the algorithm is poor when presented with new data. This phenomenon is especially common in datasets which consist of a small number of high-dimensional samples, where undue weight is given to features that only correlate in the training data. In the case of overfitting, the model is said to have high variance.

Another danger is `underfitting' data, in which the performance for a particular training set is low. In this case, the model is said to have high bias; the key features of the data have not been sufficiently learnt. Bias and variance are entwined: attempting to reduce one is likely to increase the other, and thus a tradeoff needs to be made (Figure \ref{fig:biasvariance}).

\begin{figure}
	\centering
	\includegraphics[width=0.7\textwidth]{pictures/biasvariance.png}
	\caption{\label{fig:biasvariance} The tradeoff between bias and variance.}
\end{figure}

One strategy to ensure that the model is optimally fitted is to separate the training data into a training set, a cross validation set and a testing set: the model is trained using the training set, the parameters are adjusted according to whatever produces the lowest error when the cross validation set is presented, and then the final model is assessed using the testing set (but not tuned any further) \cite{andrewng}. 
% subsubsection Bias-variance tradeoff (end)

\subsubsection{Features} % (fold)
\label{ssub:Features}

Microarray techniques produce a high number of gene expression data and, particularly with a relatively small sample size (489) for ovarian cancer and a high number of features (over ten thousand), there is a danger of some machine learning techniques overfitting the data.

Reducing the number of features being used in the model can improve performance, shorten training times and reduce overfitting. The process of doing so is known as `dimensionality reduction', of which its techniques fall under two approaches:


\paragraph{Feature extraction}
transforms the existing features into a lower dimensional space, e.g. expression levels that highly correlate with the levels of other genes can be transformed into one feature. A procedure often used to do this is principal component analysis.

\paragraph{Feature selection}
chooses a subset of all the features to use in the model such that redundant or irrelevant features in the data are omitted. Feature selection techniques are of importance in this project, as finding biomarkers in microarray (and clinical) data is a key focus. Techniques include:

\begin{enumerate}
	\item Selection by correlation: genes are ranked accordingly to its Pearson coefficient to a feature related to treatment outcome; those that have a correlation coefficient above a certain threshold (e.g. 0.3) are included.
	\item Selection by cancer relevance: using medical literature, genes that are most related to ovarian cancer are picked out and are tested to see if they alone can accurately predict the prognosis.
	\item Selection by T-test/P-value: Other statistical methods can determine whether the data sample for a gene is significant, for example by calculating their P-values or performing a T-test (Appendix \ref{sec:Statistical formulae}). Genes that test favourably are selected.
	\item Feature selection during model construction: Techniques such as Lasso and the Recursive Feature Elimination algorithm (with SVM) eliminate features while the model is being built.
\end{enumerate}

% subsubsection Features (end)

\subsubsection{SVM} % (fold)
\label{ssub:SVM}

Given a set of training examples, each marked as belonging to one of two categories (e.g. favourable or unfavourable prognosis), the binary SVM training algorithm is able to assign new data into one category or the other. It is able to accomplish this by finding a `hyperplane' that maximises the distance between the closest datapoints of each class to that line (Figure \ref{fig:hyperplane}).

\begin{figure}
	\centering
	\includegraphics[width=0.5\textwidth]{pictures/optimal-hyperplane.png}
	\caption{\label{fig:hyperplane}An instructive diagram of the linear binary SVM (showing two features).}
\end{figure}

This algorithm can be mathematically expressed as follows:

Given a training set of $n$ examples consisting of $(x_i, y_i)$ pairs, where $x_i$ is a vector with the same number of dimensions as features, and $y_i$ is either 1 or -1 (the ground truth), optimise:

$$\arg\min_{\mathbf{w},\mathbf{\xi}, b } \left\{\frac{1}{2} \|\mathbf{w}\|^2 + C \sum_{i=1}^n \xi_i \right\} $$

subject to

$$y_i(\mathbf{w}\cdot\mathbf{x_i} - b) \ge 1 - \xi_i, ~~~~\xi_i \ge 0$$

where $\bf{w}$ is the normal distance of the point to the plane, $b$ is the bias (offset of hyperplane when passing through origin), $C$ is a constant, and $\xi$ is some non-negative value that measures the degree of misclassification of the data $x_i$. This particular example is known as the `soft margin' method because of its tolerance of some misclassified data ($\xi$).

When considering the hyperplane in the case of two features, using a straight line to divide the two cases is known as using a linear `kernel'. For more complex relationships, other kernels can be used, such as the polynomial, radial and sigmoid kernel.

There are more sophisticated SVM methods such as the multiclass SVM, which is able to classify data into $k$ sets; this is done by creating $k$ binary SVMs (class 1 versus all other classes, class 2 versus all other classes, ... up to class $k$ versus all other classes) and choosing the classification with the largest hyperplane margin \cite{multicategory_microarray}.

SVMs are popular in bioinformatics because they can overcome two common problems that frequently occur in other supervised learning techniques: overfitting, and the computation costs involved in handling datasets with a large number of features. A solution to overfitting is mapping the data into a higher-dimensional space: however, creating a representation of this feature space can be expensive. The SVM algorithm does not require an explicit representation of the feature space; it is able to find the separating hyperplane and state it in terms of vectors and dot products by defining the \textit{kernel function}, which is equivalent to calculating the dot product (i.e. distance) between the hyperplane and a data point in that space  \cite{svm}.

A popular, open source SVM library often used in the field of bioinformatics is LIBSVM\cite{LIBSVM}.

% subsubsection SVM (end)

% subsection Machine learning (end)

\subsection{Dataset} % (fold)
\label{sub:Dataset}

The dataset for ovarian carcinoma contains 489 annotated cases of stage II-IV ovarian carcinoma, current up to August 25, 2010. The standard treatment for these cases are aggressive surgery, followed by platinum/taxane chemotherapy \cite{ovarian_analysis}. The dataset, in a format that can readily stored in tranSMART, contains the following notable features:

\begin{enumerate}
	\item Expression levels for 11864 genes: measured in fold change \cite{transformation}.
	\item Age at diagnosis: a number, e.g. 51.21
	\item Vital status: either dead or alive
	\item Tumor stage: II to IV
	\item Tumor grade: G2 or G3
	\item Residual disease: either `no macroscopic disease' or expression of length, e.g. $>$20mm
	\item Primary therapy outcome: e.g. `complete response' or  `stable disease'
	\item Person neoplasm cancer status: e.g. `tumour free'
	\item Overall survival (in months): e.g. 1.54
	\item Progression free status: e.g. `disease free'
	\item Progression free survival (in months): e.g. 1.54
	\item Platinum free interval: e.g. -3.7
	\item Platinum status: e.g. `too early'
\end{enumerate}

With the exception of the gene expression levels, there are missing data in the other features.

% subsection Dataset (end)

% section Background (end)

\section{Progress} % (fold)
\label{sec:Progress} 

% subsection Problems (end)
\subsection{Activities to Date} % (fold)
\label{sub:Activities to Date}

The main activities to date have been reading published papers on applications of machine learning in bioinformatics, as well as researching implementation methods for the algorithms and interface. More specifically:

\begin{itemize}
	\item Read papers regarding the application of machine learning techniques to cancer diagnosis.
	\item Explored how the tranSMART foundation and web interface works.
	\item Trying out Angular, a web framework for creating the interface.
	\item Trying out D3.js, a web data visualisation tool.
	\item Trying out scikit-learn, a machine learning library in Python.
\end{itemize}

% subsection Activities to Date (end)

\subsection{Forseen Challenges} % (fold)
\label{sub:Challenges}

Although implementation has yet to begin, the following issues have been identified as potential challenges that need to be overcome:

\begin{itemize}
	\item Data treatment: Sci-learn requires that the data be in a certain format (supports LIBSVM formats, as well as its own numpy internal data), whereas the format that tranSMART takes a different sort of format (tab and newline delimited), so preprocessing to the tranSMART data is required.
	\item Incomplete data: Data is missing in important features, such their vital status and age. A basic strategy would be to discard the entire row, but with only 489 samples, each case is valuable. Therefore, strategies to impute (i.e. infer) the missing data from the known must be explored.
	\item tranSMART integration: currently, the tranSMART API has yet to be released.
	\item Interface design: although only one dataset has been studied, it is clear that in order to apply machine learning techniques to tranSMART data, an extra processing step to the data needs to be taken. This is because all the data needs to be `joined' to form one table in order to feed into the machine learning algorithm. (For example table, gene\_expression.txt needs to be transposed, and then the former column headings need to be truncated and modified so that they can be joined onto the patient's barcode in the raw\_data.txt table). An intuitive way of allowing users to perform these operations whilst not being overwhelmed by the quantity of the data is needed.
\end{itemize}

% subsection Challenges (end)

\subsection{Next Stages} % (fold)
\label{sub:Next Stages}

The next stages of this project will be to start the data treatment for the dataset of ovarian carcinoma (HGS-OvCa), and then apply machine learning algorithms to it from the scikit-learn library. Classification models can be evaluated using the train/validate/test procedure described in \ref{ssub:Bias-variance tradeoff},  biomarkers chosen from feature selection techniques discussed in \ref{ssub:Features} can be compared and contrasted with those described in literature \cite{ovarian_analysis}, as well those identified from other biology-specific feature selection methods (e.g. pathway-based models using Thomson Reuters' Metacore \cite{metacore}).

The proposed implementation details are as follows:

\begin{enumerate}
	\item PostgreSQL for storing the dataset
	\item R/Python for the computation; there are libraries such as scikit-learn which has implemented many standard ML techniques, as well as incorporating the popular LIBSVM algorithm\cite{scilearn}.
%\cite{pybrain}
	\item A server framework for connecting it up to the web interface: potentially in Python (either Flask or Django).
	\item A web framework for structuring the interface: Angular (this is what tranSMART is using), with a data visualisation library (e.g. D3.js).
\end{enumerate}

It is anticipated that after the data has been processed into a format that a machine learning library can use, other techniques can be applied to the data without much extra work.

After processing the dataset, the interface should be implemented with the following goals in mind:

\begin {itemize}
	\item A user friendly interface with tutorials and example usage.
	\item Exposure of feature reduction techniques to the user to identify new biomarkers, presented in a format which allows the results to be cross-verified and contrasted with those cited in existing literature.
\end{itemize}

As well as developing the interface, another project aim is to use the tool itself and carry out some original research, such as comparing the biomarker selected and classification results with pathway-based models (i.e. start with pathways as the features, then populate them with gene expression data) such using those provided by Thomson Reuters.

Other secondary goals include:

\begin{itemize}
	\item Apply machine learning algorithms to other tranSMART-ready datasets. It is hoped that the preprocessing of the data can be automated to some extent. Furthermore, it is hoped that by analysing more tranSMART datasets, a better understanding into how this project can complement tranSMART is gained.
	\item Integrate the interface as a plugin, using the tranSMART API (apart from the Python components, the technologies proposed for the project closely mirror those used in tranSMART).
	\item A separate tool or interface that can convert tranSMART format datasets into those that are machine learning compatible.
\end{itemize}


% psycopg2 - python adapter for postgres

% subsection Next stages (end)

% section Progress (end)

\appendix
\section{Statistical formulae} % (fold)
\label{sec:Statistical formulae}

\subsection{Pearson coefficient} % (fold)
\label{sub:Pearson coefficient}

The Pearson coefficient is a measure of the dependence between two variables, X and Y (e.g. the expression level of a gene and the length of platinum-free interval).
Its value ranges from +1 (total positive correlation) to -1 (total negative correlation), and is commonly represented by the Greek symbol $\rho$. Its formula is:

$$ \rho_{X,Y} ={E[(X-\mu_X)(Y-\mu_Y)] \over \sigma_X\sigma_Y} $$

where $ \sigma_X $ is the standard deviation of  $ X $, $ \mu_X $ is the mean of $ X $, and $ E $ is the expectation.

% subsection Pearson coefficient (end)

\subsection{T-test} % (fold)
\label{sub:T-test}

The T-test is a statistical hypothesis test that determines whether two sets of data are significantly different to each other, assuming that both follow a normal distribution, by examining their means. For example, the T-test can be applied to the expression levels of a gene sample that is afflicted with cancer, and one that is cancer free, to see if there's a statistically significant difference between the two sample groups. The formula for calculating the $t$ value for two groups, X and Y, is:

$$ t = \frac{\mu_X - \mu_Y}{\sqrt{\frac{\sigma^2(X)}{n_X} + \frac{\sigma^2(Y)}{n_Y}}} $$

where $\mu$ is the mean, $\sigma$ is the standard deviation and $n$ is the number elements in that sample.

% subsection T-test (end)

% section Statistics (end)

\bibliographystyle{plainnat}
\bibliography{biblio.bib}

\end{document}
