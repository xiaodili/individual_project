import unittest
import sys
import os
import numpy as np

# add the grandparent directory so that the subprocesses directory can be included
sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "/../..")
# modules we are testing:

import routes.subprocesses.ml_handle
import routes

class MADSuite(unittest.TestCase):
    def setUp(self):
        pass

    def testWikiMadFunc(self):
        test_arr = [1, 1, 2, 2, 4, 6, 9]
        result = routes.subprocesses.ml_handle._getMAD(test_arr)
        self.assertEqual(result, 1)

    def testMAD(self):
        config = {
                'threshold': 10
                }
        test_probes = {
                'p1': [1,5.34,3,4,3,2,1], #1
                'p2': [1,1,2,2,4,6,9], #1
                'p3': [10,2,3,4,3,4,1], #1
                'p4': [16,251,40,41,41,20] #10.5
                }
        probes_left, augmented_results = routes.subprocesses.ml_handle.MAD(config, test_probes)
        self.assertEqual(len(probes_left), 1)

class VarianceSuite(unittest.TestCase):
    def setUp(self):
        pass
    def testVarianceThreshold(self):
        config = {
                'threshold': 10
                }
        test_probes = {
                'p1': [4,9], #12.5
                'p2': [-2,5], #9
                'p3': [1,7], #6.25
                }
        probes_left, augmented_results = routes.subprocesses.ml_handle.varianceThreshold(config, test_probes)
        self.assertEqual(len(probes_left), 1)

if __name__ == '__main__':
    unittest.main(verbosity=2)

