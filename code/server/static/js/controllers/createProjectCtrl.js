app.controller("createProjectCtrl", ["$scope", "$http", "$location", function($scope, $http, $location) {

    $scope.create = function(name) {

        $http({
            method: 'POST',
            url: '/projects',
            data: JSON.stringify({
                name: name
            }),
            headers: {'Content-Type': 'application/json'}
        }).
        success(function(resp) {
            $location.path('/projects/' + resp);
        }).
        error(function(err) {
            console.log(err);
        });
    };

}]);
