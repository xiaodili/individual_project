angular.module('app.controllers').controller('selectionPickerCtrl', [
    '$scope',
    '$routeParams',
    '$http',
    'preplot',
    //'paramHelperFilter',
    //function($scope, $routeParams, $http, preplot, paramHelperFilter) {
    function($scope, $routeParams, $http, preplot) {

        $scope.selection_options = [
            'MAD',
            'VarianceThreshold',
            'Univariate',
            'Embedded'
        ];

        //default when testing...
        //$scope.fs_selection_method = $scope.selection_options[2];

        //make alerts
        $scope.alerts = [];
        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };

        // to integrate the information from $scope.config (bound to the model of the form and $scope.chosen to create the appropriate request:

        $scope.submit = function(params) {
            var data = {};
            data.name = $scope.fs_selection_method;
            data.config = params;
            $scope.submitML(data);
        };

        $scope.submitML = function(data) {
            var url = '/projects/' + $routeParams.project_id + '/ml';

            $http({
                method: 'POST',
                url: url,
                data: JSON.stringify(data)
            })
            .success(function(data, status, headers, config) {
                $scope.chosen = undefined;
                $scope.alerts.push({
                    type: "success",
                    msg: "Job successfully submitted! This page will update automatically when the job is done."
                });
                $scope.$emit('resync');
            })
            .error(function(data, status, headers, config) {
                console.log('data: ', data);
                $scope.chosen = undefined;
                $scope.alerts.push({
                    type: "danger",
                    msg: "Yikes, something has gone wrong."
                });
            });
        };
}]);
