describe("Testing controllers", function() {

    beforeEach(module('app'));

    describe('projectWorkspaceCtrl', function() {
        var scope, ctrl, $httpBackend;

        var routeParams = {
            project_id: '123'
        }

        beforeEach(inject(function(_$httpBackend_, _$interval_, $rootScope, $controller) {
            $httpBackend = _$httpBackend_;
            $interval = _$interval_;

            scope = $rootScope.$new();
            ctrl = $controller('projectWorkspaceCtrl', {
                $scope: scope,
                $routeParams: routeParams
            });
        }));

        it('should fetch the project properly', function() {

            var dummy_project = {
                clinical: 'hello clinical'
            };
            $httpBackend.expectGET('/projects/123').respond(dummy_project);

            expect(scope.project).toBeUndefined();
            $httpBackend.flush();
            expect(scope.project.clinical).toBe('hello clinical');
            expect(scope.poll).toBeUndefined();
        });

        it('should poll correctly', function() {
            var dummy_project = {
                clinical: 'hello clinical',
                queued: 'present'
            };
            $httpBackend.expectGET('/projects/123').respond(dummy_project);
            $httpBackend.flush();
            expect(scope.poll).toBeDefined();
        });

        it('poll should cancel correctly', function() {
            var project_queued = {
                clinical: 'hello clinical',
                queued: 'present'
            };
            var project_regular = {
                clinical: 'hello clinical'
            };

            var dummy_outcome = {
                outcome: false
            }
            $httpBackend.expectGET('/projects/123').respond(project_queued);
            $httpBackend.flush();
            $httpBackend.expectGET('/projects/123/check_queue').respond(dummy_outcome);
            $httpBackend.expectGET('/projects/123').respond(project_regular);
            $interval.flush(5000);
            $httpBackend.flush();

            expect(scope.poll).toBeUndefined();
        });
    });

    describe('projectsCtrl', function() {
        var scope, ctrl, $httpBackend;

        beforeEach(inject(function(_$httpBackend_, $rootScope, $controller) {
            $httpBackend = _$httpBackend_;

            modal = {
                open: function(obj) {
                    modal.params = obj;
                }
            };

            scope = $rootScope.$new();
            ctrl = $controller('projectsCtrl', {
                $scope: scope,
                $modal: modal
            });
        }));

        it('should load projects correctly', function() {
            var resp = {
                data: [{
                    name: 'TCGA Ovarian Cancer',
                    id: '123'
                },
                {
                    name: 'TCGA Colon Adenocarcinoma',
                    id: '124'
                }]
            };

            $httpBackend.expectGET('/projects').respond(resp);
            expect(scope.projects).toBeUndefined();
            $httpBackend.flush();
            expect(scope.projects.length).toBe(2);
        });

        it('should delete projects correctly', function() {

            var resp = {
                data: [{
                    name: 'TCGA Ovarian Cancer',
                    id: '123'
                },
                {
                    name: 'TCGA Colon Adenocarcinoma',
                    id: '124'
                }]
            };

            $httpBackend.expectGET('/projects').respond(resp);
            $httpBackend.flush();

            scope.deleteProject(123);
            var callback = modal.params.resolve.info().callback;

            expect(callback).toBeDefined();

            callback();
            var resp = {};
            $httpBackend.expectDELETE('/projects/123').respond(resp);

            var resp = {
                data: [
                {
                    name: 'TCGA Colon Adenocarcinoma',
                    id: '124'
                }]
            };

            $httpBackend.expectGET('/projects').respond(resp);
            $httpBackend.flush();
            expect(scope.projects.length).toBe(1);

        });

    });

    describe('createProjectCtrl', function() {
        var scope, ctrl, $httpBackend, $location;

        beforeEach(inject(function(_$httpBackend_, $rootScope, $controller, _$location_) {
            $httpBackend = _$httpBackend_;

            scope = $rootScope.$new();

            $location = _$location_;

            ctrl = $controller('createProjectCtrl', {
                $scope: scope
                //$location: location
            });
        }));

        it('should create projects properly', function() {
            scope.create('TCGA Ovarian Cancer');

            var resp = '123';

            $httpBackend.expectPOST('/projects').respond(resp);
            $httpBackend.flush();
            expect($location.$$path).toBe('/projects/123');

        });

    });

    describe('headCtrl', function() {
        var scope, ctrl, $httpBackend, $location;

        beforeEach(inject(function($rootScope, $controller, _$location_) {

            scope = $rootScope.$new();

            $location = _$location_;

            ctrl = $controller('headCtrl', {
                $scope: scope
            });
        }));

        it('should recognise current locations', function() {

            $location.$$path = '/projects'

            var result = scope.isActive('projects');
            
            expect(result).toBe(true);
        });
    });
});
