\chapter{Introduction}

\section{Motivation and Objectives}

With advances in technology, biomedical scientists are able to generate huge quantities of omics data for a given disease study. High throughout sequencing methods such as as RNA-Seq and DNA microarrays can be used to sequence entire genomes and can generate gene expression levels for affected versus control cells in a patient, e.g. cancers. There are ongoing efforts to collect and standardise this data; at the same time, numerous platforms and analysis toolkits have emerged to allow bioinformaticians and biomedical researchers access to biological computation, requiring variable levels of prerequisite programming experience.

The area this project's focus on is the analysis of gene expression microarray data, particularly those that have to do with cancer. Due to its high-dimensional nature, it can be difficult for clinician to decide which genes to focus their study on. Various statistical analysis can be applied to microarray data, such as classification, clustering, density estimation \cite{fs_hd_gmd}, which can help make sense of the distribution of data. A machine learning technique known as feature selection can be used to filter, according to some criteria, the set of genes examined down to a more manageable quantity. If clinical information is available alongside a patient's microarray data, it can be used to provide such criteria.

Platforms that focus on providing data storage to such microarray data are numerous, so therefore the standardisation and curation of such datasets is important. Data analysis platforms often require the data to be in certain formats, and although they may provide the facility to convert from one to another, extra work is usually required by the researcher to ensure that their data is compatible for analysis. Furthermore, platforms vary in terms of accessibility (e.g. web vs. local application), complexity and functionality, which can further deter researchers without programming experience.

\begin{table}
\centering
\begin{tabular}{ r|c|c|c| }
    \multicolumn{1}{r}{}
     &  \multicolumn{1}{c}{Main Role}
     & \multicolumn{1}{c}{Interface}
     & \multicolumn{1}{c}{Feature Selection?} \\
    \cline{2-4}

    Galaxy & Analysis & Web (native HTML) & No \\
    \cline{2-4}

    tranSMART & Storage/Analysis & Web (Java) & No \\
    \cline{2-4}

    TCGA & Storage/Analysis & Web & No \\
    \cline{2-4}

    Bioconductor & Analysis & Desktop, (CLI, R) & Yes \\
    \cline{2-4}

    Orange & Analysis & Desktop (GUI, Python) & Yes \\
    \cline{2-4}

    Weka & Analysis & Desktop (GUI, Java) & Yes (but not in GUI) \\
    \cline{2-4}

    SAM & Analysis & Desktop (GUI, R/Excel) & No \\
    \cline{2-4}

    Matlab toolbox & Analysis & Desktop (IDE) & Yes \\
    \cline{2-4}

\end{tabular}
\caption{\label{tab:comparison_of_platforms}Comparison of some analysis/storage platforms}
\end{table}

In the last half decade, an explosion of non-trivial web applications have come into existence, partly due to the adoption of HTML5. User interfaces have been standardised through the ubiquitous adoption of certain popular front-end frameworks, imprinting a certain expectation of functionality in certain interface elements onto users. Through the integration of such standards, it has become possible to design a tool that is both friendly and intuitive to the non-programming researcher.

To summarise, the motivation behind this project arise from the following observations:

\begin{enumerate}
    \item Researchers from a clinical background do not necessarily have a background in programming or statistical modelling that allow them to take advantage of the microarray and clinical data that have been generated from their and other research groups' studies.
    \item Existing platforms such as Galaxy, a premier platform for bioinformatics analysis, do not have an intuitive way of allowing the user to manipulate the data and to provide a frictionless workflow for feature selection with integrated array data. 
    \item There exists established frameworks and libraries which allow the construction of rich web applications.
\end{enumerate}

Therefore it is worthwhile to consider the merits of a tool that takes in array and clinical data of a specified format, performs feature selection and other analysis on the data with minimal configuration necessary, whilst maintaining a familiar and responsive interface.

The objectives of this project is to therefore investigate the potential analysis techniques that may be useful to the biomedical researcher, and then to implement or integrate such functionality into a tool used to identify new biomarkers through a state of the art web interface. Integrating and preprocessing the data will be integrated into the tool. A secondary objective is to use the tool developed to analyse the data generated from various cancer studies see if any of the biomarkers that are identified have been mentioned in existing literature.

Due to the scope and time limitations of the project, it is not intended to completely replace the existing platforms. Instead, the aim is to create an exemplar exploratory tool with a modern, user-friendly interface so that domain experts without programming experience can use it with minimal training.

\section{Contributions}

An extensible tool that can integrate microarray with clinical data was built. Its purpose is to allow clinicians to discover new biomarkers by examining microarray features, which can be filtered according to various statistical and feature selection techniques. The client-facing part of the tool was implemented using established CSS and JS frameworks. Modern tools for testing and web development have also been used to aid the development process. A leading machine learning library has been integrated into the application that allows users to visualise their data, perform feature analysis and compare their results with current literature.

\section{Structure of report}
\label{sec:Structure of report}

\textbf{Background} contains material covering the relevant areas of biology used to generate the data used in the project, such as microarray technology and an overview of some of the established data storage and analysis platforms in that domain. A discussion on the common machine learning techniques that were considered and used in the project is given, including best practices such as cross validation, and an overview of some common techniques. Finally, a discussion on the merits of the proposed technologies that will be used in the project, their benefits, and other alternatives will be given.

\textbf{Design and theory} discusses the specifications for the tool, the formats that it will accept as input, the pre-processing steps that have to take place in order to integrate the clinical and microarray data, and an overview of the concepts and procedures of the statistical and machine learning techniques that will be exposed to the user for the final project.

\textbf{Implementation and methodology} describes the development process of the project, the layout of the code and the design choices made. It will describe the important sections of the application, i.e. the projects, workspace and results page, how the various visual cues were achieved, and what goes on on the server when a user makes a request.

\textbf{Results and evaluation} discusses the final product, what the typical workflow of a clinician using it might be, and the final results that are yielded. The extensibility of the tool is highlighted as the procedure in which an analysis technique, MAD, was integrated into the tool.

\textbf{Conclusions and future work} describes any conclusions drawn from the project and lessons learnt. It covers any technical debt that may have accrued throughout the project, and suggestions to improve upon the tool and ideas for future projects.
