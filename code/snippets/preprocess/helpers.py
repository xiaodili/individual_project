# Debuggy function to make sure we've covered all the possible values for the keys - returns a list of all the values in the list
def getPossibleKeys(arr):
    a_keys= {}
    for a in arr:
        a_keys[a] = None
    return a_keys.keys()

# Returns all non-numerical values in a list
def getPossibleKeysWithoutNumbers(arr):
    a_keys = {}
    for a in arr:
        try:
            float(a)
        except:
            a_keys[a] = None

    return a_keys.keys()

# dictionary using the tcga ids (guaranteed to be unique) as keys for joining onto the gene expression levels later:
def toDict(data):

    dict = {}
    for d in data:
        key = d.pop(0)
        dict[key] = d
    return dict

# returns a 2D array by joining the raw and gene data, ready to be fed into scikit learn's preprocessing module:

def join(dict_1, dict_2, alias=None):

    def default(d):
        return d
    
    # setting the lookup function for foreign key:
    if not alias:
        alias = default

    result = []

    # gene LEFT_JOIN raw:
    for key, value in dict_1.iteritems():
        foreign_key = alias(key)

        try:
            to_append = dict_2[foreign_key]
            result.append(value + to_append)
        except:
            pass

    return result
