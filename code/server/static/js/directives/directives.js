angular.module('app.directives').directive('ipProjects', function() {
    return {
        restrict: 'E',
        templateUrl: '/partials/projects.html',
        controller: 'projectsCtrl'
    }
});

angular.module('app.directives').directive("ipCreateProject", function() {
    return {
        restrict: 'E',
        templateUrl: '/partials/create_project.html',
        controller: 'createProjectCtrl'
    }
});

angular.module('app.directives').directive("ipUploadChecker", function() {
    return {
        restrict: 'E',
        //scope: {
            //project:'@'
        //},
        templateUrl: '/partials/upload_checker.html',
        controller: 'uploadCheckerCtrl'
    }
});

angular.module('app.directives').directive("ipSelectionPicker", function() {
    return {
        restrict: 'E',
        //scope: {
            //project:'@'
        //},
        templateUrl: '/partials/selection_picker.html',
        controller: 'selectionPickerCtrl'
    }
});

angular.module('app.directives').directive("ipDebug", function() {
    return {
        restrict: 'E',
        templateUrl: '/partials/debug.html',
        controller: 'debugCtrl'
    }
});

//nope, still won't let me plot xaxis categories and ylabel. what's going on???
angular.module('app.directives').directive("ipVanillaPlotter", function() {
    return {
        restrict: 'E',
        scope: {},
        templateUrl: '/partials/vanilla_plotter.html',
        controller: 'vanillaPlotterCtrl'
    }
});
