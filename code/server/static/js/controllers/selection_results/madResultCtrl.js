'use strict'

angular.module('app.controllers').controller('madResultCtrl', [
    '$scope',
    '$routeParams',
    '$http',
    '$filter',
    '$sce',
    function($scope,
             $routeParams,
             $http,
             $filter,
             $sce
            ) {

        $scope.sortBySecondElement = function(pair) {
            return pair[1]
        }

        $scope.formatResult = function() {
            $scope.zipped = $filter('orderBy')(_.zip($scope.results.remaining_features, $scope.results.augmented_results.MAD), $scope.sortBySecondElement, true);
            //get top 10 features
            var to_plot = $scope.zipped.slice(0, Math.min(10, $scope.zipped.length))
            var label = 'MAD scores for the top features';

            var y_values = [];
            var x_values = [];
            for (var i = 0, l = to_plot.length; i < l; i ++) {
                var x = to_plot[i][0];
                var y = to_plot[i][1];
                x_values.push(x);
                y_values.push(y);
            }

            $scope.$broadcast('plot_ready', {
                label: label,
                x_values: x_values,
                y_values: y_values
            });

        };

        $scope.$on('results_ready', function() {
            $scope.formatResult(); //format the newly available data in order to plot some graphs with the results etc.
        });

        $scope.changePopup = function(probe) {
            $scope.probe = $sce.trustAsResourceUrl("http://reflect.ws/REST/GetPopup?name=" + probe);
        }
    }
]);
