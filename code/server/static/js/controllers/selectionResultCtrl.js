'use strict'

angular.module('app.controllers').controller('selectionResultCtrl', [
    '$scope',
    '$routeParams',
    '$http',
    '$sce',
    '$filter',
    function($scope,
             $routeParams,
             $http,
             $sce,
             $filter
            ) {

        var getResults = function() {
            var path = '/projects/' + $routeParams.project_id + '/selection_results/' + $routeParams.selection_index;
            $http({
                url:path,
                method: 'GET'
            })
            .success(function(resp) {
                $scope.results = resp.results;
                $scope.name = resp.name;

                // when content is loaded, we can format the presentation
                $scope.$on('$includeContentLoaded', function() {
                    $scope.$broadcast('results_ready');
                });
            })
            .error(function(error) {
                console.log('error: ', error);
            });
        };

        getResults();

    }
]);
