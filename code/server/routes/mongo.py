# -*- coding: utf-8 -*-
from pymongo import MongoClient
from bson import ObjectId

port = 27017
client = MongoClient('localhost', port)
db = client['feature_selection']
projects = db['projects']

# some internal functions
################################################################

def stringifyObjectIDInObj(obj):
    d = {}
    for key in obj:
        if key == '_id':
            d[key] = str(obj[key])
        else:
            d[key] = obj[key]

    return d

def stringifyObjectIDInArr(arr):

    to_return = []

    for a in arr:
        d = stringifyObjectIDInObj(a)
        to_return.append(d)

    return to_return

# end internal functions
################################################################

# Used in projects.py
################################################################

def getProjectNamesAndMeta():
    names = stringifyObjectIDInArr(projects.find({}, {'name': 1, '_id': 1}))
    return names

def fetchProject(project_id):
    #TODO: surely not all subdocuments need to be fetched? Can definitely ignore some.
    project = stringifyObjectIDInArr(projects.find({'_id': ObjectId(project_id)}))
    # Always wrapped in an array, so return the first element (i.e. the first project)
    return project[0]

def createNewProject(project_name):
    project_id = projects.insert({"name": project_name})
    return str(project_id)

def queueExists(project_id):
    _id = ObjectId(project_id)
    result = projects.find({'_id': _id, 'queued': {'$exists': True}}, {"_id": 1})
    if list(result):
        return True
    else:
        return False

def deleteProject(project_id):
    _id = ObjectId(project_id)
    result = projects.remove({'_id': _id})

# Used in file_upload.py
################################################################

def writeClinical(clinical, project_id):
    _id = ObjectId(project_id)
    result = projects.update({'_id': _id}, {'$set': {'clinical': clinical}}, upsert=False)

def writeMicroarray(microarray, project_id):
    _id = ObjectId(project_id)
    result = projects.update({'_id': _id}, {'$set': {'microarray': microarray}}, upsert=False)

def writeMapping(mapping, project_id):
    _id = ObjectId(project_id)
    result = projects.update({'_id': _id}, {'$set': {'mapping': mapping}}, upsert=False)

def writeAnnotation(annotation, project_id):
    _id = ObjectId(project_id)
    result = projects.update({'_id': _id}, {'$set': {'annotation': annotation}}, upsert=False)

def applyMapping(mapping_result, project_id):
    _id = ObjectId(project_id)
    result = projects.update({'_id': _id}, {'$set': {'mapping.result': mapping_result}}, upsert=False)

def getMappingResult(project_id):
    _id = ObjectId(project_id)
    result = projects.find({'_id': _id}, {'mapping.result': 1})
    return result[0]['mapping']['result']

def getAnnotationResult(project_id):
    _id = ObjectId(project_id)
    result = projects.find({'_id': _id}, {'annotation.result': 1})
    return result[0]['annotation']['result']

def applyAnnotation(annotation_result, project_id):
    _id = ObjectId(project_id)
    result = projects.update({'_id': _id}, {'$set': {'annotation.result': annotation_result}}, upsert=False)

def removeSelections(project_id):
    _id = ObjectId(project_id)
    result = projects.update({'_id': _id}, {'$unset': {'selection_steps': ""}})

def removeSubjectMapping(project_id):
    _id = ObjectId(project_id)
    result = projects.update({'_id': _id}, {'$unset': {'mapping.result': ""}})

def removeAnnotationMapping(project_id):
    _id = ObjectId(project_id)
    result = projects.update({'_id': _id}, {'$unset': {'annotation.result': ""}})

def clinicalUploaded(project_id):
    _id = ObjectId(project_id)
    result = projects.find({'_id': _id, 'clinical': {'$exists': True}}, {"_id": 1})
    if list(result):
        return True
    else:
        return False

def microarrayUploaded(project_id):
    _id = ObjectId(project_id)
    result = projects.find({'_id': _id, 'microarray': {'$exists': True}}, {"_id": 1})
    if list(result):
        return True
    else:
        return False

def subjectUploaded(project_id):
    _id = ObjectId(project_id)
    result = projects.find({'_id': _id, 'mapping': {'$exists': True}}, {"_id": 1})
    if list(result):
        return True
    else:
        return False

def annotationUploaded(project_id):
    _id = ObjectId(project_id)
    result = projects.find({'_id': _id, 'annotation': {'$exists': True}}, {"_id": 1})
    if list(result):
        return True
    else:
        return False

def subjectMapped(project_id):
    _id = ObjectId(project_id)
    result = projects.find({'_id': _id, 'mapping.result': {'$exists': True}}, {"_id": 1})
    if list(result):
        return True
    else:
        return False

def annotationMapped(project_id):
    _id = ObjectId(project_id)
    result = projects.find({'_id': _id, 'annotation.result': {'$exists': True}}, {"_id": 1})
    if list(result):
        return True
    else:
        return False

def getMicroarrayPatients(project_id):
    _id = ObjectId(project_id)
    result = projects.find({'_id': _id}, {'microarray.patient_names': 1})
    return result[0]['microarray']['patient_names']

def getClinicalPatients(project_id):
    _id = ObjectId(project_id)
    result = projects.find({'_id': _id}, {'clinical.patient_names': 1})
    return result[0]['clinical']['patient_names']

def getProbeNames(project_id):
    _id = ObjectId(project_id)
    result = projects.find({'_id': _id}, {'microarray.probe_names': 1})
    return result[0]['microarray']['probe_names']

# Selection steps functions
################################################################

def popSelectionStep(project_id):
    _id = ObjectId(project_id)
    result = projects.update({'_id': _id}, {'$pop': {'selection_steps': 1}});

def getSelectionByIndex(project_id, index):
    _id = ObjectId(project_id)
    result = projects.find({'_id': _id}, {'_id': 1, 'name': 1, 'selection_steps': 1})
    result_list = list(result)

    response = {}
    response['results'] = result_list[0]['selection_steps'][index]
    response['name'] = result_list[0]['name']
    return response

# Just before spawning subprocesses (in ml2.py)
################################################################
def writeToQueued(project_id, data):
    _id = ObjectId(project_id)
    result = projects.update({'_id': _id}, {'$set': {'queued': data}}, upsert=False)

def addPID(project_id, pid):
    _id = ObjectId(project_id)
    result = projects.update({'_id': _id}, {'$set': {'queued.pid': pid}}, upsert=False)

def getPID(project_id):
    _id = ObjectId(project_id)
    result = projects.find({'_id': _id}, {'queued.pid': 1})
    return result[0]['queued']['pid']


# Subprocess functions
################################################################

def testFunc():
    print 'hi from mongo'

# Subprocess reads from this
def readFromQueue(project_id):
    _id = ObjectId(project_id)
    result = projects.find({'_id': _id}, {'queued': 1});

    inst = result[0]['queued']

    return inst

# throws KeyError
def getActiveProbes(project_id):
    _id = ObjectId(project_id)
    result = projects.find({'_id': _id}, {'_id': 1, 'selection_steps': {'$slice': -1}})
    result_list = list(result)

    features = result_list[0]['selection_steps'][0]['remaining_features']

    return features

def appendToSelectionSteps(project_id, data):
    _id = ObjectId(project_id)
    projects.update({'_id': _id}, {'$push': {'selection_steps': data}}, upsert=False)

def removeFromQueued(project_id):
    _id = ObjectId(project_id)
    result = projects.update({'_id': _id}, {'$unset': {'queued': ""}})


# Some unit tests
################################################################

if __name__ == '__main__':
    project_id = "53de4cb58c87e90934a2a243" # sample project
    clinical = {
            'meta': [1,2,3],
            'feature_names': ['asd','cas','cas']
            }

    project_id = "53ed2569632fc70fe2af1f9b"
    #result = clinicalUploaded(project_id)
    #result = subjectMapped(project_id)

    #removeSubjectMapping(project_id)
    #result = subjectUploaded(project_id)
    #writeClinical(clinical, project_id)


    # fine
    #result = getMicroarrayPatients(project_id)
    #result = getClinicalPatients(project_id)

    def checkKeyErrorOnEmptySelection():
        sample_project = "53de4cb58c87e90934a2a243" # sample project
        try:
            result = getActiveProbes(sample_project)
            return False
        except:
            return True

    def checkAppendToSelectionSteps():
        rabbits = "53ed2569632fc70fe2af1f9b"
        data = {
                'remaining_features': ['asdasd', 'casd', 'casdaawg'],
                'selection': 'VarianceThreshold',
                'config': {
                    'threshold': 1
                    }
                }
        return appendToSelectionSteps(project_id, data)
