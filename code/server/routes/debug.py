from flask import Blueprint, request, jsonify
import json
import mongo

import os
import sys
import subprocess

debug = Blueprint('debug', __name__)

# path of where this script lies
PATH = os.path.dirname(os.path.realpath(__file__))

@debug.route('/debug/queue_rabbit', methods=['GET'])
def queue_rabbit():
    rabbits = "53ed2569632fc70fe2af1f9b"
    data = {
            'name': 'VarianceThreshold',
            'config': {
                'threshold': 1
                }
            }

    SUBPROCESS_PATH = PATH + '/subprocesses'
    SUBPROCESS_SCRIPT = SUBPROCESS_PATH + '/test.py'


    mongo.writeToQueued(rabbits, data)
    p = subprocess.Popen([sys.executable, SUBPROCESS_SCRIPT])
    child_pid = p.pid
    mongo.addPID(rabbits, child_pid) # don't insert a new queued object if the subprocess has already finished by the time you get around to adding the PID

    return jsonify(**{'outcome': True})
