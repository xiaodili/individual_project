'use strict'

angular.module('app.controllers').controller('univariateResultCtrl', [
    '$scope',
    '$routeParams',
    '$http',
    '$filter',
    '$sce',
    function($scope,
             $routeParams,
             $http,
             $filter,
             $sce
            ) {

        $scope.sortBySecondElement = function(pair) {
            return pair[1]
        }

        $scope.formatResult = function() {
            $scope.zipped = $filter('orderBy')(_.zip($scope.results.remaining_features, $scope.results.augmented_results.log10), $scope.sortBySecondElement, true);
            //get top 10 features
            var to_plot = $scope.zipped.slice(0, Math.min(10, $scope.zipped.length))
            var label = '-log(p values) for the top probes';

            var y_values = [];
            var x_values = [];
            for (var i = 0, l = to_plot.length; i < l; i ++) {
                var x = to_plot[i][0];
                var y = to_plot[i][1];
                x_values.push(x);
                y_values.push(y);
            }
            $scope.$broadcast('plot_ready', {
                label: label,
                x_values: x_values,
                y_values: y_values
            });

        };

        $scope.$on('results_ready', function() {
            $scope.formatResult();
        });

        $scope.changePopup = function(probe) {
            $scope.probe = $sce.trustAsResourceUrl("http://reflect.ws/REST/GetPopup?name=" + probe);
        }
    }
]);
