var app = angular.module("app", [
    //external dependencies
    //----------------------
    
    //for tool tips
    'ui.bootstrap',

    //for graphing
    'highcharts-ng',

    //angular default
	'ngRoute',
    'ngSanitize',

    'app.filters',
    'app.controllers',
    'app.services',
    'app.directives',
    
    //for file uploads
    'angularFileUpload'
]);

//registering them:
var controllers = angular.module('app.controllers', []);
var filters = angular.module('app.filters', []);
var services = angular.module('app.services', []);
var directives = angular.module('app.directives', []);

//Global values string lookup for feature actions

//routing functionality
app.config(['$routeProvider', function($routeProvider) {$routeProvider
   .when('/projects', {
        templateUrl: 'partials/body.html',
        controller: 'bodyCtrl'
    })
    .when('/projects/:project_id', {
        templateUrl: 'partials/project_workspace.html',
        controller: 'projectWorkspaceCtrl'
    })
    .when('projects/:project_id/:filter_step', {
        templateUrl: 'partials/selection_step.html',
        controller: 'selectionDetailedCtrl'
    })
    .when('/projects/:project_id/selection_results/:selection_index', {
        templateUrl: 'partials/selection_result.html',
        controller: 'selectionResultCtrl'
    })
    .when('/about', {
        templateUrl: 'partials/about.html'
    })
    .when('/help', {
        templateUrl: 'partials/help.html'
    })
    .otherwise({
        redirectTo: '/projects'
    });
}]).run(['$rootScope', function($rootScope) {
    $rootScope.debug_enabled = false;
}]);
