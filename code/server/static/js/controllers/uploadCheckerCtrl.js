'use strict'

//can access parent scope, but REMEMBER:
//parent project is being asynchronously fetched!
//
angular.module('app.controllers').controller('uploadCheckerCtrl', [
    '$scope',
    '$routeParams',
    '$http',
    '$location',
    '$upload',
    '$modal',
    function($scope,
             $routeParams,
             $http,
             $location,
             $upload,
             $modal
            ) { 
    

    //initialising the progress:
    $scope.progress = {
        clinical: {
            state: 'idle',
            percent: 0,
            style: {
                width: '0%'
            }
        },
        microarray: {
            state: 'idle',
            percent: 0,
            style: {
                width: '0%'
            }
        },
        mapping: {
            state: 'idle',
            percent: 0,
            style: {
                width: '0%'
            }
        },
        annotation: {
            state: 'idle',
            percent: 0,
            style: {
                width: '0%'
            }
        }
    }

    //populate all alerts to hold zero
    $scope.alerts = {
        clinical: [
            //{ type: 'danger', msg: 'Oh snap! Change a few things up and try submitting again.' },
            //{ type: 'success', msg: 'Well done! You successfully read this important alert message.' }
        ],
        microarray: [],
        mapping: [],
        annotation: []
    };

    $scope.closeAlert = function(index, type) {
        $scope.alerts[type].splice(index, 1);
    };

    var createUploadFileFunc = function(type, file) {
        return function() {

            $scope.progress[type].state = 'uploading';
            $scope.progress[type].progress = 0;

            var url = '/projects/' + $routeParams.project_id + '/upload_' + type;

            $scope.upload = $upload.upload({
                url: url, //upload.php script, node.js route, or servlet url
                method: 'POST',
                data: {
                },
                file: file, // or list of files: $files for html5 only
            }).progress(function(evt) {
                var percent = parseInt(100.0 * evt.loaded/evt.total);
                $scope.progress[type].percent = percent;
                $scope.progress[type].style = {
                    width: percent + '%'
                }
            }).success(function(data, status, headers, config) {
                // file is uploaded successfully
                console.log('great, uploaded!');

                //reset ui
                $scope.progress[type].state = 'idle';
                $scope.progress[type].progress = 0;
                $scope.progress[type].style = {
                    width: '0%'
                };

                //push an alert
                $scope.alerts[type].push({
                    type: 'success',
                    msg: 'File successfully uploaded!'
                });

                //refetch projects document:
                $scope.$emit('resync');
            }).error(function(data, status, headers, config) {
                //reset ui
                $scope.progress[type].state = 'idle';
                $scope.progress[type].progress = 0;
                $scope.progress[type].style = {
                    width: '0%'
                };

                //push an alert
                $scope.alerts[type].push({
                    type: 'danger',
                    msg: data
                });

                $scope.$emit('resync');
            });

        }
    }

    var handleCMMUpload = function(callback) {
        try {

            if ($scope.project.selection_steps.length > 0) {
                //show confirmation modal
                var modalInstance = $modal.open({
                    templateUrl: 'partials/modal.html',
                    controller: 'modalCtrl',
                    resolve: {
                        info: function() {
                            return {
                                header: "Warning",
                                callback: callback,
                                message: "Uploading a different clinical, microarray or mapping file will cause your current selection results to be erased. Continue with upload?"

                            }
                        }
                    }
                });
            }
            else {
                //throw an exception to trigger the cmmCallback
                throw {};
            }
        }
        catch(err) {
            //don't show a popup, jump straight into the function
            callback();
        }
    };

    //clinical, microarray and mapping 
    $scope.uploadClinical = function($files) {
        var file = $files[0];
        var callback = createUploadFileFunc('clinical', file);
        handleCMMUpload(callback);
    };

    $scope.uploadMicroarray = function($files) {
        var file = $files[0];
        var callback = createUploadFileFunc('microarray', file);
        handleCMMUpload(callback);
    };

    $scope.uploadMapping = function($files) {
        var file = $files[0];
        var callback = createUploadFileFunc('mapping', file);
        handleCMMUpload(callback);
    };

    // only activated when both clinical and microarray mappings are picked
    $scope.chooseMappings = function() {
        var c_index = $scope.project.mapping.columns.indexOf($scope.clinical_column);
        var m_index = $scope.project.mapping.columns.indexOf($scope.microarray_column);
        $scope.mapping_loading = true;
        try {
            delete $scope.$parent.project.mapping.result;
        }
        catch(inst) {
            console.log('already deleted!');
        }

        var url = '/projects/' + $routeParams.project_id + '/apply_subject_mapping';

        $http({
            method: 'POST',
            url: url,
            data: JSON.stringify({
                c_index: c_index,
                m_index: m_index
            })
        }).
        success(function(resp) {
            $scope.mapping_loading = false;
            $scope.$emit('resync');
        }).
        error(function(resp) {
        });

    };

    // only activated when both clinical and microarray mappings are picked
    $scope.chooseAnnotations = function() {
        var p_index = $scope.project.annotation.columns.indexOf($scope.probe_column);
        var g_index = $scope.project.annotation.columns.indexOf($scope.gene_column);
        $scope.annotation_loading = true;
        try {
            delete $scope.$parent.project.annotation.result;
        }
        catch(inst) {
            console.log('already absent!');
        }

        var url = '/projects/' + $routeParams.project_id + '/apply_annotation'

        $http({
            method: 'POST',
            url: url,
            data: JSON.stringify({
                p_index: p_index,
                g_index: g_index
            })
        }).
        success(function(resp) {
            $scope.annotation_loading = false;
            $scope.$emit('resync');
        }).
        error(function(resp) {
        });

    };


    //$scope.mapping_loading
    $scope.uploadAnnotation = function($files) {
        var file = $files[0];
        // go straight to the upload, without the modal popup
        var upload_annotation = createUploadFileFunc('annotation', file);
        upload_annotation();
    };

}]);
