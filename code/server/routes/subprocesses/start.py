# -*- coding: utf-8 -*-
import sys
import os

import preprocess
import ml_handle

# add parent directory so that mongo, parse,files can be accessed
sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "/..")
import mongo
import parse
import files

# enable logs
import logging
PATH = os.path.dirname(os.path.realpath(__file__))

def getRemainingProbesWithPatientIDs(project_id):

    m_file = files.loadMicroarray(project_id)
    all_probes, patient_ids = parse.getMicroarrayByProbe(m_file)

    try:
        # if error thrown, then there all probes are active (no feature selection has taken place yet)
        available_probes = mongo.getActiveProbes(project_id)
        remaining_probes = preprocess.getRemainingProbes(all_probes, available_probes)
    except Exception as inst:
        remaining_probes = all_probes

    return remaining_probes, patient_ids

# read the appropriate configuration file back out from mongo using the project_id, preprocess the microarray file to a certain degree, then pass it on to specific ML handlers to do the rest
def start(project_id=None):

    # extract out project_id
    if not project_id:
        try:
            project_id = sys.argv[1]
            start(project_id)
        except Exception as inst:
            pass
    else:

        result = {}
        ml_instructions = mongo.readFromQueue(project_id)
        result['selection_method'] = ml_instructions

        try:

            remaining_probes, patient_ids = getRemainingProbesWithPatientIDs(project_id)


            subject_mapping_columns = mongo.getMappingResult(project_id)

            features_left = ml_handle.handle(project_id, ml_instructions, remaining_probes, patient_ids, subject_mapping_columns)

            # if two values are returned, one of them is the augmented results
            if isinstance(features_left, tuple):
                probes_left = features_left[0]
                augmented_results = features_left[1]
            else:
                probes_left = features_left

            result['remaining_features'] = probes_left
            result['num_eliminated_features'] = len(remaining_probes) - len(probes_left)

            # add some additional info, if additional info is present:
            if augmented_results:
                result['augmented_results'] = augmented_results

        except Exception as inst:
            result['error'] = str(inst)
            logging.exception(inst)

        mongo.appendToSelectionSteps(project_id, result)
        # Finally, regardless of success or failure, write the result of the machine learning technique back into Mongo:
        mongo.removeFromQueued(project_id)

path = PATH + '/../../logs/computation.log'
logging.basicConfig(filename=path, level=logging.DEBUG)

if __name__ == '__main__':
    start()
