from pprint import pprint

import numpy as np

from sklearn import cross_validation
from sklearn import svm
from sklearn.preprocessing import Imputer


FEATURE_DATA = "scijoined.npy"
RAW_FEATURE_DATA = "joined.npy"
FEATURE_NAMES = "names.npy"

# Number of features available for both
NUM_CLINICAL = 12
NUM_MICROARRAY = 11864

GT_INDEX = 1

# Number of folds
k = 4

# function to extract the ground truth from the set
def extractGroundTruth(raw_features, index):

    def toBin(y):
        imp = Imputer(missing_values="NaN", strategy = "most_frequent", axis=1)
        imp.fit(y)
        y_imp = imp.transform(y)[0]

        return y_imp

    # use raw features for this, because they are still binarised:
    y = raw_features[:, index]

    # Impute the missing results
    y = toBin(y)

    # slice out ground truth for the feature name
    return y

# removes the ground truth from the features, if necessary (no, in the case of microarray data)
def removeGroundTruth(features, feature_names, index):

    features_toreturn = np.concatenate((features[:, :index], features[:, index + 1:]), axis=1)
    feature_names_toreturn = np.concatenate((feature_names[:index], feature_names[index + 1:]))

    return features_toreturn, feature_names_toreturn


def load(fd, rfd, fn):
    return np.load(fd), np.load(rfd), np.load(fn)

def sliceFromRight(f, fn, index):
    return f[:,-index:], fn[-index:]

def sliceFromLeft(f, fn, index):
    return f[:,:index], fn[:index]


def integratedData(features, raw_features, feature_names):

    # remove ground truths from features and feature names at GT_INDEX
    features, feature_names = removeGroundTruth(features, feature_names, NUM_MICROARRAY + GT_INDEX)

    #extract the ground truth from raw features into y:
    y = extractGroundTruth(raw_features, NUM_MICROARRAY + GT_INDEX)

    return y, features, feature_names

def clinicalOnly(features, raw_features, feature_names):

    #use clinical data only, just to check things out:
    features, feature_names = sliceFromRight(features, feature_names, NUM_CLINICAL)

    # remove ground truths from features and feature names at GT_INDEX
    features, feature_names = removeGroundTruth(features, feature_names, GT_INDEX)

    #extract the ground truth from raw features into y:
    y = extractGroundTruth(raw_features, NUM_MICROARRAY + GT_INDEX)

    return y, features, feature_names

    #print y
    #print y.tolist().count(1) #alive - 215
    #print y.tolist().count(0) #dead - 273

def microarrayOnly(features, raw_features, feature_names):

    # using microarray data:
    features, feature_names = sliceFromLeft(features, feature_names, NUM_MICROARRAY)

    # no need to remove ground truth from the data

    #extract the ground truth from raw features into y:
    y = extractGroundTruth(raw_features, NUM_MICROARRAY + GT_INDEX)

    return y, features, feature_names


def binLinearSVM(data, target, folds):

    clf = svm.SVC(kernel = "linear", C = 1)
    scores = cross_validation.cross_val_score(clf, data, target, cv = folds)

    # Scores:
    print "Score: "
    print scores

    # Average:
    print "Average: "
    print float(sum(scores))/len(scores)

# Recursive feature elimination:
def getRankings(features, y, feature_names, limit = None):

    # for feature selection
    from sklearn.feature_selection import RFE

    svc = svm.SVC(kernel = "linear", C = 1)
    rfe = RFE(estimator = svc, n_features_to_select = 1, step = 1)
    rfe.fit(features, y)
    ranking = rfe.ranking_.tolist()

    # print out the clinical rankings of feature importance
    if limit:
        upto = min(limit, len(ranking) + 1)
    else:
        upto = len(ranking) + 1

    #print ranking
    for rank in xrange(1, upto):
        feat = ranking.index(rank)
        print feature_names[feat]

if __name__ == "__main__":

    features, raw_features, feature_names = load(FEATURE_DATA, RAW_FEATURE_DATA, FEATURE_NAMES)

    #y, features, feature_names = clinicalOnly(features, raw_features, feature_names)
    y, features, feature_names = microarrayOnly(features, raw_features, feature_names)
    #y, features, feature_names = integratedData(features, raw_features, feature_names)

    # takes too long!
    getRankings(features, y, feature_names, 15)

    #binLinearSVM(features, y, k)
