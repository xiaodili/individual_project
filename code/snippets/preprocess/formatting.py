#Formatting module.

#Converts TCGA.61.041336.01A.01R to TCGA-61-1895

def geneToRaw(gene_id):
    #only the first three parts of the id are needed
    required_parts = gene_id.split('.')[:3]
    return '-'.join(required_parts)

# Testing:
if __name__ == "__main__":
    test1 = "TCGA.61.2012.01A.01R"
    test2 = "TCGA.61.2016.01A.01R"
    test3 = "TCGA.61.2088.01A.01R"

    print geneToRaw(test1)
    print geneToRaw(test2)
    print geneToRaw(test3)

