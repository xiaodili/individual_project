'use strict'

angular.module('app.controllers').controller('rfeResultCtrl', [
    '$scope',
    '$routeParams',
    '$http',
    '$filter',
    '$sce',
    function($scope,
             $routeParams,
             $http,
             $filter,
             $sce
            ) {

        $scope.sortBySecondElement = function(pair) {
            return pair[1]
        }

        $scope.formatResult = function() {
            console.log('$scope.results: ', $scope.results);
            //get top 10 features
            $scope.zipped = $filter('orderBy')(_.zip($scope.results.augmented_results.feature_names, $scope.results.augmented_results.features_ranked), $scope.sortBySecondElement, false);

            var validation_scores = $scope.results.augmented_results.scores;
            var to_plot = _.zip(_.range(1, validation_scores.length + 1), validation_scores)
            var label = 'Stratified cross validated scores';

            var y_values = [];
            var x_values = [];
            for (var i = 0, l = to_plot.length; i < l; i ++) {
                var x = to_plot[i][0];
                var y = to_plot[i][1];
                x_values.push(x);
                y_values.push(y);
            }
            $scope.$broadcast('plot_ready', {
                label: label,
                x_values: x_values,
                y_values: y_values
            });

        };

        $scope.$on('results_ready', function() {
            $scope.formatResult();
            console.log('$scope.results: ', $scope.results);
        });

        $scope.changePopup = function(probe) {
            $scope.probe = $sce.trustAsResourceUrl("http://reflect.ws/REST/GetPopup?name=" + probe);
        }
    }
]);
