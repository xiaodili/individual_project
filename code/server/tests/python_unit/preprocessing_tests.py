import unittest
import sys
import os
import mongo_mock

# add the grandparent directory so that the subprocesses directory can be included
sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "/../..")
# modules we are testing:

import routes.subprocesses.start
import routes.subprocesses.preprocess

class ProbeLoadSuite(unittest.TestCase):

    def setUp(self):
        self.project_id = 'test_case1'

    def testFullProbeFetch(self):

        def getActiveProbes(project_id):
            raise Exception("no selection steps have taken place")

        setattr(mongo_mock, 'getActiveProbes', getActiveProbes)

        remaining_probes, patient_ids = routes.subprocesses.start.getRemainingProbesWithPatientIDs(self.project_id)

        # these need to be in order:
        self.assertEquals(patient_ids, ['P1', 'P2', 'P3'])

        # order does not matter for the probes, as long as they contain the same probes:
        self.assertEqual(sorted(remaining_probes.keys()), sorted(['gene1', 'gene2', 'gene3', 'gene4']))

    def testRemainingProbeFetch(self):


        def getActiveProbes(project_id):
            return ['gene1', 'gene2']

        setattr(mongo_mock, 'getActiveProbes', getActiveProbes)

        remaining_probes, patient_ids = routes.subprocesses.start.getRemainingProbesWithPatientIDs(self.project_id)

        # these need to be in order:
        self.assertEquals(patient_ids, ['P1', 'P2', 'P3'])

        # order does not matter for the probes, as long as they contain the same probes:
        self.assertEqual(sorted(remaining_probes.keys()), sorted(['gene1', 'gene2']))

class PreprocessingSuite(unittest.TestCase):

    def setUp(self):
        self.project_id = 'test_case1'

        def getActiveProbes(project_id):
            raise Exception("no selection steps have taken place")

        setattr(mongo_mock, 'getActiveProbes', getActiveProbes)

        self.probes, self.patient_ids = routes.subprocesses.start.getRemainingProbesWithPatientIDs(self.project_id)
        self.mapping_config = {
                'm_index': 1,
                'c_index': 0
                }

    def test1(self):
        self.assertEqual(1, 1);

    def testTransposeMaintainsOrder(self):
        X, keys = routes.subprocesses.preprocess.microToX(self.probes)
        self.assertEqual(keys, ['gene1', 'gene2', 'gene3', 'gene4'])

    def testJoin(self):
        X, names = routes.subprocesses.preprocess.performJoin(self.project_id, self.probes, self.patient_ids, self.mapping_config)
        self.assertEqual(names, ['gene1', 'gene2', 'gene3', 'gene4', 'col1', 'col2', 'col3'])

class ErrorHandlingSuite(unittest.TestCase):
    def setUp(self):
        self.project_id = 'test_case2'

    def testNoMethod(self):
        routes.subprocesses.start.start(self.project_id)
        result = mongo_mock.RESULTS
        self.assertEqual('error' in result, True)

    def appendWorks(self):
        pass

if __name__ == '__main__':
    # overloading the mongo module with the mock object
    routes.subprocesses.start.mongo = mongo_mock

    #unittest.main(verbosity=2)

    suite = unittest.TestLoader().loadTestsFromTestCase(ErrorHandlingSuite)
    unittest.TextTestRunner(verbosity=2).run(suite)

    #unittest.main(verbosity=2)
