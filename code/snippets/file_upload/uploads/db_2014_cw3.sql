-- Q1 returns (first_name)

SELECT DISTINCT CASE
                WHEN POSITION(' ' IN name)=0 THEN name
                ELSE SUBSTRING(name FROM 1 FOR POSITION(' ' IN name)-1)
                END AS first_name
FROM   person
ORDER BY first_name;

-- Q2 returns (born_in,popularity)

SELECT   born_in,
         COUNT(born_in) AS popularity
FROM     person
WHERE    born_in IS NOT NULL
GROUP BY born_in
ORDER BY popularity DESC,
         born_in;

-- Q3 returns (house,seventeenth,eighteenth,nineteenth,twentieth)

SELECT house,
       COUNT(CASE WHEN EXTRACT(YEAR FROM accession) BETWEEN 1600 AND 1699
                  THEN accession ELSE null END) AS seventeenth,
       COUNT(CASE WHEN EXTRACT(YEAR FROM accession) BETWEEN 1700 AND 1799
                  THEN accession ELSE null END) AS eighteenth,
       COUNT(CASE WHEN EXTRACT(YEAR FROM accession) BETWEEN 1800 AND 1899
                  THEN accession ELSE null END) AS nineteenth,
       COUNT(CASE WHEN EXTRACT(YEAR FROM accession) BETWEEN 1900 AND 1999 
                  THEN accession ELSE null END) AS twentieth
FROM   monarch
WHERE  house IS NOT NULL
GROUP BY house
ORDER BY house; 

-- Q4 returns (name,age)

SELECT person.name,
       EXTRACT(YEAR FROM AGE(MIN(child.dob),person.dob)) AS age
FROM   person 
       JOIN person AS child 
       ON (person.name=child.father OR person.name=child.mother)
GROUP BY person.name,
       person.dob
ORDER BY person.name;

-- Q5 returns (father,child,born)

SELECT person.name AS father,
       child.name AS child,
       CASE 
       WHEN child.name IS NOT NULL
       THEN RANK() OVER (PARTITION BY child.father ORDER by child.dob)
       ELSE null
       END AS born
FROM   person 
       LEFT JOIN person AS child 
       ON person.name=child.father
WHERE  person.gender='M'
ORDER BY person.name,
       born;

-- Q6 returns (monarch,prime_minister)

SELECT DISTINCT monarch.name AS monarch,
       prime_minister.name AS prime_minister
FROM   monarch,prime_minister
WHERE  -- No later prime minister coming before the monarch acceeded
       NOT EXISTS (SELECT *
                   FROM   prime_minister AS later_prime_minister
                   WHERE  later_prime_minister.entry<=monarch.accession
                   AND    later_prime_minister.entry>prime_minister.entry)
       -- No monarch before this prime ministers entry
AND    NOT EXISTS (SELECT *
                   FROM   monarch AS later_monarch
                   WHERE  prime_minister.entry>=later_monarch.accession
                   AND    later_monarch.accession>monarch.accession)
AND    monarch.house IS NOT NULL
ORDER BY monarch,
       prime_minister;

