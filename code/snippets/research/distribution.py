# -*- coding: utf-8 -*-

import matplotlib
#import scipy.stats as stats
import csv
import numpy as np

#matplotlib.rcParams['backend'] = 'TkAgg'

import matplotlib.pyplot as plt
#plt.plot([1,2,3,4])
#plt.ylabel('some numbers')
#plt.show()

samples = []
samples_exponential = []

#FILE='data/TCGA_OV_gene_expression.txt'
#FILE='data/Endo_gene_expression.txt'
FILE='data/TCGA_breast_gene_expression_2.txt'
#FILE='data/CRC_gene_expression _draft.txt'

with open(FILE, 'r') as contents:
    reader = csv.reader(contents, delimiter='\t')
    headers = reader.next()
    for i, row in enumerate(reader):
        if i%1000 == 0:
            samples.append(row[1:])

samples_float = [np.array(s).astype('float') for s in samples]

negatives = []

sample = samples_float[8]
histogram = np.histogram(sample, 13)

x = histogram[1][1:]
y = histogram[0]
plt.plot(x,y)
plt.show()

print stats.normaltest(sample)
print np.histogram(samples_float[0], 15)

#plt.plot([1,2,3,4])
#plt.ylabel('some numbers')
#plt.show()
