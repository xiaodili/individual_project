import subprocess
import sys
import os
import time
import signal

PATH = os.path.dirname(os.path.realpath(__file__))
SUBPROCESS_PATH = PATH + '/.'
SUBPROCESS_SCRIPT = SUBPROCESS_PATH + '/child.py'

p = subprocess.Popen([sys.executable, SUBPROCESS_SCRIPT, 'hi son'])
child_pid = p.pid

print 'child_pid:', child_pid

#time.sleep(5)

#print 'killing child process...'
#os.kill(child_pid, signal.SIGTERM)

print 'main done'
