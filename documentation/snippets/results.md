# SVM without clinical data

Using clinical data only and the vital status as the ground truth, prediction rates are:

    [ 0.7295082   0.80327869  0.77868852  0.73770492]

Averaging out to be:

    0.762295081967

* ML technique: binary SVM with linear kernel, C = 1
* Cross validation: 4 fold
* Num features: 11
* Num samples: 488

# SVM with only microarray data

Using microarray data only and the vital status as the ground truth, prediction rates are:

    [ 0.57377049  0.56557377  0.62295082  0.60655738]

Averaging out to be:

    0.592213114754

* ML technique: binary SVM with linear kernel, C = 1
* Cross validation: 4 fold
* Num features: 11863
* Num samples: 488

# SVM with integrated data

Using microarray with clinical data and the vital status as the ground truth, prediction rates are:

    [ 0.63114754  0.60655738  0.64754098  0.63934426]

Averaging out to be:

    0.631147540984

* ML technique: binary SVM with linear kernel, C = 1
* Cross validation: 4 fold
* Num features: 11874
* Num samples: 488

# Clinical feature rankings using recursive feature elimination

1. PERSONNEOPLASMCANCERSTATUS
1. ProgressionFreeStatus
1. PlatinumFreeInterval (mos)*
1. ProgressionFreeSurvival (mos)#
1. PlatinumStatus
1. AgeAtDiagnosis (yrs)
1. OverallSurvival(mos)
1. TUMORRESIDUALDISEASE
1. TUMORSTAGE
1. TUMORGRADE
1. PRIMARYTHERAPYOUTCOMESUCCESS

* ML technique: binary SVM with linear kernel, C = 1
* Num features: 11
* Num samples: 488

# Microarray feature rankings using recursive feature elimination
