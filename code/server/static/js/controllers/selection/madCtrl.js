'use strict'

angular.module('app.controllers').controller('madCtrl', [
    '$scope',
    function($scope) {

        $scope.submit = function() {
            var params = $scope.config;
            $scope.$parent.submit(params);
        }

    }
]);
