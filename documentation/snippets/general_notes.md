# Microarray data 

## Creation of unified Expression Data set

(From supplementary information: http://www.nature.com/nature/journal/v474/n7353/extref/nature10166-s1.pdf)

Data  from  each  platform  were  normalized  and  summarized  separately,  as  described  above, resulting  in  gene  expression  estimates  for  each  sample  and  gene  on  each  platform.  Relative gene expression values were calculated per platform by subtracting the mean expression value across patients from the gene estimate and dividing by the standard deviation across patients.  Factor  analysis  was  applied  to  integrate  three  expression  values  (one  for  each  platform),  on genes  present  at  all  three  platforms  (n  =  11,864).  The  factor  analysis  provided  estimates  of relative  gene  expression  scaled  to  have  the  same  underlying  variation  among  patients  for  all genes.  We  rescaled  the  unified  gene  expression  of  each  gene  by  estimates  of  the  standard deviation across patients. To obtain a single estimate of standard deviation per gene, we took the  median  absolute  deviation  (MAD)  for  each  platform  and  then  averaged  these  estimates, restricting to those platforms with high correlation to the unified gene estimates. This gave a single estimate of variation per gene that was then used to rescale the unified gene estimates. 

# Cancer 
Stage that cancer is diagnosed at is most important factor in predicting survival, grouped labelled 1-4.  
Of all women diagnosed, only 4% are diagnosed with stage 2 ovarian cancer.  
Of these women, 50% will live for at least 5 years after being diagnosed.  
This drops to 20% for stage 3, and 6% for stage 4 
http://www.cancerresearchuk.org/cancer-help/type/ovarian-cancer/treatment/statistics-and-outlook-for-ovarian-cancer

# Glossary

**drug target**: A biological target such as protein/nucleic acid whose activity can be modified by an external stimulus.

# Pathway analysis

**KEGG**: Kyoto Encyclopedia of Genes and Genomes - contains wealth of data associated with pathways, genes, genomes and reaction information.

+p: phosphorylation
-p: dephosphorylation
+u: ubiquitination
+g: glycosylation

... etc.

---> activation

---| inhibition

--|-- dissociation

http://www.genome.jp/kegg/document/help_pathway.html

Pathway enrichment anaylsis is subset of functional class scoring. 

Functional class scoring - convert individual gene results into gene function results.

**cofunctional genes**

Assortment of methods for converting gene expression levels into p-values

* Overrepresentation analysis: thresholds p-values at 5%, significant genes ( less than 5%) are compared to the corresponding population of genes being studied using Fischer's exact test to see if there is an overrepresentation of significant genes in the gene category.

* Functional class scoring: Summarises the p-values for all genes in a gene category (e.g. cell killing, developmental process, locomotion).

* GO is a set of associations from biological phases to specific genes that are either chosen by curators or done automatically. Designed to rigorously encapsulate the known relationships between biological terms and all genes that are instances of these terms.
* GO annotations can complement microarray analysis

Lecture on what pathway enrichment anaysis does: http://lectures.molgen.mpg.de/Algorithmische_Bioinformatik_WS1213/lec14a_Shamir_GO_GSEA.pdf


**SNP**: Single Nucleotide Polymorphism. At a certain position in the DNA sequence, some people will have one base, and other people will have a different base. The two forms of SNPs are called 'alleles'.

# PCA (prinicpal component analysis)

Tries to find convert a set of observations of possibly correlated variables into a set of linearly uncorrelated variables.

# Useful links

Bunch of papers on SVM: http://www.support-vector.net/bioinformatics.html

# Scikit learn

* Dataset is a dictionary-like object with two items:

    * data: 2D array of n_samples vs. n_features
    * target: length of n_features, contains list of features

* Might be able to do this using pandas. Actually, never mind.

* Feature vectors NEED to be encoded as numerical data. Can do this with the LabelEncoder of sklearn:

	http://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.LabelEncoder.html#sklearn.preprocessing.LabelEncoder

	//not this:
	>http://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.DictVectorizer.html

* Support here: 

	http://en.wikipedia.org/wiki/Feature_vector


##
