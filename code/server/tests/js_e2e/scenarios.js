'use strict'

describe('homepage', function() {
    it('should redirect to the correct page', function() {

        browser.get('http://localhost:5000');

        browser.getLocationAbsUrl().then(function(url) {
            expect(url.split('#')[1]).toBe('/projects');
        });
    });

    describe('homepage', function() {
        var project_name = element(by.id('project_name'));
        var create_button = element(by.id('create_project'));
        beforeEach(function() {
            browser.get('http://localhost:5000');
        });

        it('should be able to delete and create projects', function() {
            project_name.sendKeys('new_sample_project');
            create_button.click();

            //project has definitely been created. Confirm this with getting the element of the newly created element, then deleting it.
        });
    });

});
