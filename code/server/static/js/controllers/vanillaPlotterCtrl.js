angular.module('app.controllers').controller('vanillaPlotterCtrl', [
    '$scope',
    function($scope) {

        $scope.chartSeries = [
            {}
        ];

        //$scope.chartConfig = {
            //options: {
                //chart: {
                    //type: 'column'
                //},
                //plotOptions: {
                    //series: {
                        //stacking: ''
                    //},
                    //column: {
                        //pointPadding: 0,
                        //borderWidth: 0,
                        //groupPadding: 0,
                        //shadow: false
                    //}
                //},
                //legend: {
                    //enabled: false
                //}
            //},
            //xAxis: {
            //},
            //yAxis: {
                //title: {
                    //text: $scope.arg.label
                //}
            //},
            //series: $scope.chartSeries,
            //title: {
                //text: $scope.label
            //},
            //credits: {
                //enabled: false
            //},
            //loading: false,
            //size: {}
        //};


        $scope.plot = function(arg) {

            //$scope.categories = ['asda'];

            //var data = $scope.to_plot;
            //var y_values = [];
            //var x_values = [];
            //for (var i = 0, l = data.length; i < l; i ++) {
                //var x = data[i][0];
                //var y = data[i][1];
                //x_values.push(x);
                //y_values.push(y);
            //}

            //console.log('arg: ', arg);
            //$scope.arg.label = arg.label;
            ////$scope.chartConfig.xAxis.categories = ['cas','dpg'];
            //$scope.chartSeries[0].data = arg.y_values;
            //$scope.chartConfig.xAxis.categories = arg.x_values;
            //console.log('arg.x_values: ', arg.x_values);
            //console.log('hi');

            createConfig(arg);

            //Some kind of bug with Highcharts? X categories won't get plotted for some reason
            
            //$scope.chartSeries[0].data = y_values;
            //$scope.chartConfig.xAxis.categories = x_values;
            //$scope.chartConfig.xAxis.categories[1] = 'cas';

            //
            //$scope.chartConfig.xAxis.categories.push(x_values);
            //for (var i = 0, l = x_values.length; i < l; i ++) {
                //var v = x_values[i];
                //$scope.chartConfig.xAxis.categories.push(v);
            //}
            //console.log('$scope.chartConfig.xAxis.categories[1]: ', $scope.chartConfig.xAxis.categories[1]);
            //console.log('$scope.chartConfig.xAxis.categories[1]: ', $scope.chartConfig.xAxis.categories[1]);
            //$scope.chartConfig.yAxis.title.text = $scope.label;
            //console.log('changing');

            //this.chartConfig.xAxis.categories.push(1,2,3,4,5,6,7,8,10);
            //.push(1,2,3,4,5,6,7,8,10);

            //this.chartConfig.options.chart.type = 'column';

            //$scope.chartConfig.series[0].data = [1,2,3,4,5,6,7,8,9,10];
            
            //$scope.chartConfig.yAxis.title.text = "dogs";
            //$scope.chartSeries[0].data = y_values;

            //delete $scope.chartConfig;

        };

        var createConfig = function(arg) {

            $scope.chartConfig = {
                options: {
                    chart: {
                        type: 'column'
                    },
                    plotOptions: {
                        series: {
                            stacking: ''
                        },
                        column: {
                            pointPadding: 0,
                            borderWidth: 0,
                            groupPadding: 0,
                            shadow: false
                        }
                    },
                    legend: {
                        enabled: false
                    }
                },
                xAxis: {
                    categories: arg.x_values,
                    type: 'category',
                    title: {
                        text: ''
                    },
                    labels: {
                        enabled: false
                    }
                },
                yAxis: {
                    title: {
                        text: arg.label
                    }
                },
                series: [{
                    data: arg.y_values
                }],
                title: {
                    text: arg.label
                },
                credits: {
                    enabled: false
                },
                loading: false,
                size: {}
            };

        }

        $scope.$on('plot_ready', function(event, arg) {
            $scope.plot(arg);
        });

}]);
