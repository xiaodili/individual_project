# Mongo structure

## Collections

### Projects

See projects.py for the current sample output

{
    name: "sample_project",
    meta: {
        //stuff that would appear in the projects page
    },
    clinical: {
        meta: {
            num_features: int,
            num_samples: int,
            sample: "preprocessed text to display"
        },
        feature_names: [
            "vital_status","height","shoe_size", "tumor_size"
        ],
        features: {
            f1: [...],
            f2: [...]
        },
        features_for_plot: {
            "vital_status": ['LIVING','DEAD','LIVING','DEAD','LIVING'],
            "height": [13,14,123,435,435]
        }
    },
    microarray: {
        "meta": {
            "num_features": 8,
            "num_samples": 5,
            "sample": "CLID\tperson1\tperson2\tperson3\t...\nperson1\t12\t13\t14\nperson2\t12\t13\t14\t31\nperson3\t2\t123\t931\n..."
            },
        "feature_names": [
            "GENE1","GENE2","GENE3", "GENE4", "GENE5","GENE6","GENE7","GENE8"
            ],
        "features": {
            "person1": [12,13,14,13,12,13,14,13],
            "person2": [12,13,14,31,41,51,61,23],
            "person3": [2,123,931,123,13,13,14,12],
            "person4": [3123,435,03,312,13,14,35,46],
            "person5": [3123,435,31,3,31,41,53,63]
            }
    },
    filters: {[
        {features_left: [probe_id12, probe_id412, ...],
        filter_method: {
            //filter method stuff here
        },
        {features_left: [],
        //and so on
        }
    ]},
    mapping: {
        m_to_c: {},
        c_to_m: {}
    },
    user: "Eddie",
    queued_job: {
        //same stuff that's in filter_method
    }
}

# Input files

## Clinical
## Microarray
## Mapping

## Annotation

# Routes

## parse2

### clinical = parseClinical(c_file)

clinical is clinical part from the project document
status is {'outcome': True}

## create_project

## Subprocess for ML

config = {
    choices: {
        kernel: "",
        C: int,
        shrinking: bool,
        degree: int,
        ground_truth: Object,
        ml: "SVM"
        
    },
    actions: [{
        //project.staged from projectWorkspaceCtrl
    }],
    name: "TCGA_raw.txt", //name
    missing_values: []
}

# Controller scopes

## resultCtrl

    results: {
        dataset: "TCGA_raw.txt",
        results: [12,13,14,14],
        svm_type: Binary/Multiway,
        ground_truth: "VITAL_STATUS"

    }



## projectWorkspaceCtrl

    $scope.project = {
        name: "Name of Project",
        raw: [], //array of raw files available to be chosen
        staged: [ //array of raw files chosen (staged)
            {
                name: "", //Name of raw file chosen
                features: [{ //an array of features

                    size: int, //Number of samples (rows)
                    values: [], //List of the possible values
                    action: "", //Either binary, manual assign, numeric or discard
                    name: "",
                    formatted: "",
                    ranked_values: [
                        {
                            value: "",
                            weight: int

                        }
                    ]
                    }, {
                        ...
                    }
                ],
                values_to_ignore: [] //values to be ignored

            }
        ]
    }


# Request and response formats

## /projects/<project_name>.json [GET]

Request: Nothing

Response: {
    name: String,
    raw: [
        String1,
        String2,
        String3
    ]
}
## /projects/<project_name>/raw/<raw_file> [GET]

Request: Name of raw file: String

Response: {
    name: Name of raw file, (isn't this redundant?)
    features: [
        {
        name: String,
        size: int,
        values: [],
        action: Binary | Manual assign | Numeric | Discard
        },
        {
        name: String,
        size: int,
        values: [],
        action: Binary | Manual assign | Numeric | Discard
        }
    ]
}

## /projects/<project_name>/results/<result> [GET]

Request: result name

Response: {
    status: bool,
    results: {
    }
}

## Finish preprocessing

Request: {
    null values: [],
    field_actions: {
        {
        header: String,
        action: [values_sorted] | binary
        },
        {
        header: String,
        action: [values_sorted] | binary
        }
    }
}
