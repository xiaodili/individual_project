import unittest
import sys
import os
import mongo_mock

# add the grandparent directory so that the subprocesses directory can be included
sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "/../..")
# modules we are testing:

import routes.subprocesses.start
import routes.subprocesses.preprocess

class VarianceThresholdSuite(unittest.TestCase):
    def setUp(self):
        self.project_id = 'test_case1'
        # injecting appropriate values into mongo_mock:
        self.inst = {
                'name': 'VarianceThreshold',
                'config': {
                    'threshold': 1
                    }
                }
        mongo_mock._setMLInstructions(self.inst)

        def getActiveProbes(project_id):
            raise Exception("no selection steps have taken place")

        mapping = {
                'm_index': 1,
                'c_index': 0
                }

        mongo_mock._setMappingColumns(mapping)

        self.probes, self.patient_ids = routes.subprocesses.start.getRemainingProbesWithPatientIDs(self.project_id)

    def testVarianceThreshold(self):
        probes, aug = routes.subprocesses.ml_handle.varianceThreshold(self.inst['config'], self.probes)
        # low variance gene has been removed
        self.assertEquals('gene4' in probes, False)

class SampleOVSuite(unittest.TestCase):
    def setUp(self):
        self.project_id = 'test_from_ov'
        self.variance_inst = {
                'name': 'VarianceThreshold',
                'config': {
                    'threshold': 0.5
                    }
                }

        self.uni_inst = {
                'name': 'Univariate',
                'config': {
                    'ground_truth': 'VITALSTATUS',
                    'gt_type': 'Categorical',
                    'func': 'f_classif',
                    'mode': 'k_best',
                    'param': 20
                    }
                }

        self.embedded_inst = {
                'name': 'Embedded',
                'func': 'rfe',
                'kernel': 'linear',
                'select_k': 1,
                'config': {
                    'clinical_instructions': {
                        'ground_truth': {
                            'name': 'VITALSTATUS',
                            'action': 'Multiclass',
                            },
                        'to_integrate': [
                            {
                                'name': 'TUMORGRADE',
                                'action': 'Binary'
                                },
                            {
                                'name': 'TUMORSTAGE',
                                'action': 'Binary'
                                }
                            ]
                        },
                    }
                }

        def getActiveProbes(project_id):
            raise Exception("no selection steps have taken place")

        setattr(mongo_mock, 'getActiveProbes', getActiveProbes)

        self.subject_mapping_columns = {
                'm_index': 3,
                'c_index': 2
                }

        self.probes, self.patient_ids = routes.subprocesses.start.getRemainingProbesWithPatientIDs(self.project_id)

    def testProbesLoaded(self):
        self.assertEquals(len(self.probes),200)

    def testJoin(self):
        X, names = routes.subprocesses.preprocess.performJoin(self.project_id, self.probes, self.patient_ids, self.subject_mapping_columns)
        self.assertEquals(len(X), 488)
        self.assertEquals(len(names), 212)

    def testVariance(self):
        probes, aug = routes.subprocesses.ml_handle.varianceThreshold(self.variance_inst['config'], self.probes)
    def testUnivariate(self):
        probes, aug = routes.subprocesses.ml_handle.univariate(self.project_id, self.uni_inst['config'], self.probes, self.patient_ids, self.subject_mapping_columns)
        self.assertEquals(len(probes), 20)
    def testEmbedded(self):
        probes, aug = routes.subprocesses.ml_handle.rfe(self.project_id, self.embedded_inst['config'], self.probes, self.patient_ids, self.subject_mapping_columns)

if __name__ == '__main__':
    # overloading the mongo module with the mock object
    routes.subprocesses.start.mongo = mongo_mock
    unittest.main(verbosity=2)


    # unintegrated tests

    #def checkUnivariate():
        #rabbits = "53fc88fc632fc72fb191dc0e"
        ##rabbits = "53ed2569632fc70fe2af1f9b"

        #data = {
                #'name': 'Univariate',
                #'config': {
                    #'ground_truth_index': 1,
                    #'missing_values': ['Missing'],
                    #'mapping': {
                        #},
                    #'score_func': 'f_classif',
                    #'mode': 'percentile',
                    #'param': 10
                    #}
                #}

        #mongo.writeToQueued(rabbits, data)
        #start(rabbits)
        #mongo.removeFilters(rabbits)

    #def checkRFE():
        #rabbits = "53ed2569632fc70fe2af1f9b"
        #data = {
                #'name': 'rfe',
                #'config': {
                    #'alpha': 1,
                    #'kernel': 'linear',
                    #'C': 1,
                    #'select_k': 20,
                    #'missing_values': ['Missing'],
                    #'fit_boolean': False,
                    #'normalize': False,
                    #'precompute': 'auto',
                    #'clinical_instructions': {
                        #'ground_truth': {
                            #'name': 'VITALSTATUS',
                            #'action': "Multiclass" # has to be numerical, right? Or manually assign. Then, it would still be numerical.
                            #},
                        #'to_integrate': [
                            ##{
                                ##'name': 'TUMORSTAGE',
                                ##'action': 'Binary'
                                ##},
                            #{
                                #'name': 'AgeAtDiagnosis (yrs)',
                                #'action': 'Numeric'
                                #},
                            #{
                                #'name': 'TUMORRESIDUALDISEASE',
                                #'action': 'Manually assign',
                                #'ranked': {
                                    #'No Macroscopic disease': 0,
                                    #'1-10 mm': 10,
                                    #'11-20 mm': 20,
                                    #'>20mm': 30
                                    #}
                                #}
                            #]
                        #}
                    #}
                #}

        #mongo.writeToQueued(rabbits, data)
        #start(rabbits)
        ##mongo.removeFilters(rabbits)

    #def checkChi2():
        #new_project = "53fc88fc632fc72fb191dc0e"
        #data = {
                #"name": "Univariate",
                #'config': {
                    #'ground_truth_index': 1,
                    #'missing_values': ['Missing'],
                    #'mapping': {
                        #},
                    #'score_func': 'chi2',
                    #'mode': 'percentile',
                    #'param': 10
                    #}
                #}

        #mongo.writeToQueued(new_project, data)
        #start(new_project)
        #mongo.removeFilters(new_project)

    #def checkLasso():
        #rabbits = "53ed2569632fc70fe2af1f9b"
        #data = {
                #'name': 'Lasso',
                #'config': {
                    #'alpha': 1,
                    #'missing_values': ['Missing'],
                    #'fit_boolean': False,
                    #'normalize': False,
                    #'precompute': 'auto',
                    #'clinical_instructions': {
                        #'ground_truth': {
                            #'name': 'ProgressionFreeSurvival (mos)#',
                            #'action': "Numeric" # has to be numerical, right? Or manually assign. Then, it would still be numerical.
                            #},
                        #'to_integrate': [
                            ##{
                                ##'name': 'TUMORSTAGE',
                                ##'action': 'Binary'
                                ##},
                            #{
                                #'name': 'AgeAtDiagnosis (yrs)',
                                #'action': 'Numeric'
                                #},
                            #{
                                #'name': 'TUMORRESIDUALDISEASE',
                                #'action': 'Manually assign',
                                #'ranked': {
                                    #'No Macroscopic disease': 0,
                                    #'1-10 mm': 10,
                                    #'11-20 mm': 20,
                                    #'>20mm': 30
                                    #}
                                #}
                            #]
                        #}
                    #}
                #}
