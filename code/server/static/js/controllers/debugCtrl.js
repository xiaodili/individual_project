'use strict'

angular.module('app.controllers').controller('debugCtrl', [
    '$scope',
    '$http',
    '$rootScope',
    function($scope, $http, $rootScope) {

        $scope.addToQueue = function() {
            var url = '/debug/queue_rabbit'
            $http({
                method: 'GET',
                url: url
            })
            .success(function(data, status, headers, config) {
                console.log('data: ', data);
                // so cool! it works so nicely.
                $rootScope.$broadcast('resync');
            })
            .error(function(data, status, headers, config) {
            });
        }
    }
]);
