angular.module('app.services').factory("preplot", function() {

    //stricter comparison for numeric values
    var isNumeric = function (value) {
        if(/^(\-|\+)?([0-9]+(\.[0-9]+)?|Infinity)$/
           .test(value))
               return true;
           return false;
    };

    return {
        //returns either true (numerical) or false (categorical)
        guessNumerical: function(arr, missing_values) {

            var total = 0;
            var number = 0;
            var NUMBER_THRESHOLD = 0.6;

            for (var i = 0; i < arr.length; i++) {

                var value = arr[i];

                if (isNumeric(value)) {
                    number += 1;
                    total += 1;
                }

                else if (!missing_values || missing_values.indexOf(arr[i]) != -1) { 
                    total += 1;
                }
                
            }

            if (number > total * NUMBER_THRESHOLD) {
                return 'Numerical';
            }
            else {
                return 'Categorical';
            }
        },
        toNumerical: function(arr, bin_size) {

            var numbers = arr.filter(isNumeric);

            
            //map them all to be numbers, just to be safe:
            numbers = numbers.map(function(elem) {
                return parseFloat(elem);
            });

            numbers.sort(function(a, b) {
                return a - b;
            });

            var min = numbers[0];
            var max = numbers[numbers.length - 1];
            var interval = (max - min)/bin_size;

            //create the boundaries for the buckets, and the buckets themselves
            var bucket_boundaries = []
            var bucket = [];
            for (var i = 1; i < bin_size; i++) {
                bucket_boundaries.push(min + i*interval);
                bucket.push([]);
            }
            bucket_boundaries.push(max);
            bucket.push([]);

            var bucket_index = 0;

            while (numbers.length != 0) {
                number = numbers.shift();
                while (number > bucket_boundaries[bucket_index]) {
                    bucket_index += 1;
                }
                bucket[bucket_index].push(number);
            }

            console.log('bucket_boundaries: ', bucket_boundaries);

            //labels:
            labels = [];
            for (var i = 0; i < bin_size; i++) {
                if (i == 0) {
                    labels.push(min + ' to ' + bucket_boundaries[i].toPrecision(3));
                }
                else {
                    labels.push(bucket_boundaries[i-1].toPrecision(3) + ' to ' + bucket_boundaries[i].toPrecision(3));
                }
            }

            values = bucket.map(function(elem) {
                return elem.length;
            });


            return [labels, values];
        },
        toCategorical: function(arr) {
            tally = {};
            for (var i = 0; i < arr.length; i++) {
                if (!tally.hasOwnProperty(arr[i])) {
                    tally[arr[i]] = 1;
                }
                else {
                    tally[arr[i]] += 1;
                }
            }

            //populate the values
            labels = [];
            values = [];
            for (var key in tally) {
                if (tally.hasOwnProperty(key)) {
                    labels.push(key);
                    values.push(tally[key]);
                }
            }

            return [labels, values];
        }
    };
});
