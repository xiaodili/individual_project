'use strict'

angular.module('app.controllers').controller('lowvarianceCtrl', [
    '$scope',
    function($scope) {

        $scope.submit = function() {
            var params = $scope.config;
            $scope.$parent.submit(params);
        }

    }
]);
