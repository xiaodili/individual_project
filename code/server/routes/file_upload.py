from flask import Blueprint, request, jsonify
from pprint import pprint
import parse
import mongo
import json
import files

uploads = Blueprint('uploads', __name__)

# transition enums
CLINICAL_UPLOADED = 0
MICROARRAY_UPLOADED = 1
SUBJECT_UPLOADED = 2
ANNOTATION_UPLOADED = 3
SUBJECT_APPLIED = 4
ANNOTATION_APPLIED = 5

# uploading
#--------------------------------------------------

@uploads.route('/projects/<project_id>/upload_clinical', methods=['POST'])
def upload_clinical(project_id):

    clinical = request.files['file']

    try:
        handleClinical(clinical, project_id)
        handleUpdate(CLINICAL_UPLOADED, project_id)
        return 'ok', 200
    except Exception as inst:
        return str(inst), 500

@uploads.route('/projects/<project_id>/upload_microarray', methods=['POST'])
def upload_microarray(project_id):

    microarray = request.files['file']

    try:
        handleMicroarray(microarray, project_id)
        handleUpdate(MICROARRAY_UPLOADED, project_id)
        return 'ok', 200
    except Exception as inst:
        return str(inst), 500

# not implemented yet
@uploads.route('/projects/<project_id>/upload_mapping', methods=['POST'])
def upload_mapping(project_id):

    mapping = request.files['file']

    try:
        handleMapping(mapping, project_id)
        handleUpdate(SUBJECT_UPLOADED, project_id)
        return 'ok', 200
    except Exception as inst:
        return str(inst), 500

# not implemented yet
@uploads.route('/projects/<project_id>/upload_annotation', methods=['POST'])
def upload_annotation(project_id):

    annotation = request.files['file']

    try:
        handleAnnotation(annotation, project_id)
        handleUpdate(ANNOTATION_UPLOADED, project_id)
        return 'ok', 200
    except Exception as inst:
        return str(inst), 500

# apply subject and annotation mapping
#--------------------------------------------------

# not implemented yet
@uploads.route('/projects/<project_id>/apply_subject_mapping', methods=['POST'])
def apply_mapping(project_id):

    data = json.loads(request.data)

    c_index = data['c_index']
    m_index = data['m_index']

    try:
        applyMapping(c_index, m_index, project_id)
        handleUpdate(SUBJECT_APPLIED, project_id)
        return 'ok', 200
    except Exception as inst:
        return str(inst), 500

# not implemented yet
@uploads.route('/projects/<project_id>/apply_annotation', methods=['POST'])
def apply_annotation(project_id):

    data = json.loads(request.data)

    p_index = data['p_index']
    g_index = data['g_index']

    try:
        status = applyAnnotation(p_index, g_index, project_id)
        handleUpdate(ANNOTATION_APPLIED, project_id)
        return 'ok', 200
    except Exception as inst:
        return str(inst), 500

# internal functions that do all the work, and that can be tested in the __main__ part
#--------------------------------------------------
    
# open and parse the file, write results of it into mongo
def handleClinical(clinical_file, project_id):
    clinical_file = files.writeClinical(project_id, clinical_file)
    clinical = parse.parseClinical(clinical_file)
    mongo.writeClinical(clinical, project_id)

# open and parse the file, write results of it into mongo
def handleMicroarray(microarray_file, project_id):
    microarray_file = files.writeMicroarray(project_id, microarray_file)
    microarray = parse.parseMicroarray(microarray_file)
    mongo.writeMicroarray(microarray, project_id)

def handleMapping(mapping_file, project_id):
    mapping_file = files.writeMapping(project_id, mapping_file)
    mapping = parse.parseMapping(mapping_file)
    mongo.writeMapping(mapping, project_id)

def handleAnnotation(annotation_file, project_id):
    annotation_file = files.writeAnnotation(project_id, annotation_file)
    annotation = parse.parseAnnotation(annotation_file)
    mongo.writeAnnotation(annotation, project_id)

def applyMapping(clinical_index, microarray_index, project_id):

    # read the file
    subject_file = files.loadMapping(project_id)
    # parse the file to get the relevant columns, currently as a tuple
    columns_zipped = parse.extractColumns(subject_file, microarray_index, clinical_index)
    m_to_c = dict(columns_zipped)

    # get patient names from the microarray and clinical files
    micro_patients = mongo.getMicroarrayPatients(project_id)
    clinical_patients = mongo.getClinicalPatients(project_id)

    c_set = set(clinical_patients)

    # iterate through the columns, see how many matches both microarray and clinical files 
    num_mapped = 0
    for m in micro_patients:
        try:
            c = m_to_c[m]
            if c in c_set:
                num_mapped += 1
        except Exception as inst:
            print inst
            # can't find the mapping in either the subject file provided, or it wasn't included in the clinical file
            pass

    # successful mapping. record results into mongo:
    result = {}
    result['c_index'] = clinical_index
    result['m_index'] = microarray_index
    result['mapped'] = num_mapped

    mongo.applyMapping(result, project_id)

def applyAnnotation(probe_index, gene_index, project_id):
    try:
        # read the file
        annotation_file = files.loadAnnotation(project_id)
        # parse the file to get the relevant columns, currently as a tuple
        columns_zipped = parse.extractColumns(annotation_file, probe_index, gene_index)
        p_to_g = dict(columns_zipped)

        # get the probe names
        probe_names = mongo.getProbeNames(project_id)

        # iterate through the columns, see how many keys you can match with probe name
        num_mapped = 0
        for p in probe_names:
            try:
                g = p_to_g[p]
                num_mapped += 1
            except Exception as inst:
                print inst
                # can't find the mapping in either the subject file provided, or it wasn't included in the clinical file
                pass

        # successful mapping. record results into mongo:
        result = {}
        result['p_index'] = probe_index
        result['g_index'] = gene_index
        result['mapped'] = num_mapped


        mongo.applyAnnotation(result, project_id)

    except Exception as inst:
        print str(inst)
        return False

    return True

# deletes any appropriate subdocuments from the mongodb
def handleUpdate(action_num, project_id):

    if action_num == CLINICAL_UPLOADED:
        # remove all selection steps
        # if microarray is uploaded subject is uploaded and subject mapping is applied, recalculate subject
        removeAllSelectionSteps(project_id)
        if microarrayUploaded(project_id) and subjectUploaded(project_id) and subjectMapped(project_id):
            recalculateSubject(project_id)

    elif action_num == MICROARRAY_UPLOADED:
        # remove all selection steps
        # if clinical is uploaded and subject is uploaded and subject mapping is applied, recalculate subject
        # if annotation is uploaded and annotation is applied, recalculate annotation
        removeAllSelectionSteps(project_id)
        if clinicalUploaded(project_id) and subjectUploaded(project_id) and subjectMapped(project_id):
            recalculateSubject(project_id)

        if annotationUploaded(project_id) and annotationMapped(project_id):
            recalculateAnnotation(project_id)

    elif action_num == SUBJECT_UPLOADED:
        # remove all selection steps
        # delete subject calculation
        removeAllSelectionSteps(project_id)
        deleteSubjectMapping(project_id)

    elif action_num == ANNOTATION_UPLOADED:
        # delete annotation calculation
        deleteAnnotationMapping(project_id)

    elif action_num == SUBJECT_APPLIED:
        # remove all selection steps
        removeAllSelectionSteps(project_id)
        pass
    elif action_num == ANNOTATION_APPLIED:
        # do nothing
        pass
    else:
        print 'Yikes, something has gone wrong - action enum doesn\'t correspond to anything.'

def removeAllSelectionSteps(project_id):
    mongo.removeSelections(project_id)

def clinicalUploaded(project_id):
    return mongo.clinicalUploaded(project_id)

def microarrayUploaded(project_id):
    return mongo.microarrayUploaded(project_id)

def subjectUploaded(project_id):
    return mongo.subjectUploaded(project_id)

def annotationUploaded(project_id):
    return mongo.annotationUploaded(project_id)

def annotationMapped(project_id):
    return mongo.annotationMapped(project_id)

def subjectMapped(project_id):
    return mongo.subjectMapped(project_id)

def recalculateSubject(project_id):
    # fetch indices used in current subject mapping
    result = mongo.getMappingResult(project_id)
    m_index = result['m_index']
    c_index = result['c_index']
    # call applyMapping again
    applyMapping(c_index, m_index, project_id)

def recalculateAnnotation(project_id):
    # fetch indices used in current annotation mapping
    result = mongo.getAnnotationResult(project_id)
    p_index = result['p_index']
    g_index = result['g_index']
    # call applyAnnotation again
    applyAnnotation(p_index, g_index, project_id)
    pass

def deleteAnnotationMapping(project_id):
    mongo.removeAnnotationMapping(project_id)

def deleteSubjectMapping(project_id):
    mongo.removeSubjectMapping(project_id)

if __name__ == '__main__':
    # can do some testing here...

    #project_id = "53de4cb58c87e90934a2a243"
    #clinical = open("../projects/sample_project/raw/TCGA_OV_raw_data.txt", 'r')
    #status = handleClinical(clinical, project_id)

    #project_id = "53de4cb58c87e90934a2a243"
    #microarray = open("../projects/sample_project/raw/TCGA_OV_gene_expression.txt" , "r")
    #status = handleMicroarray(microarray, project_id)

    project_id = "53ed2569632fc70fe2af1f9b"
    recalculateSubject(project_id)

    print "done"
