angular.module('app.controllers').controller('featureExplorerCtrl', [
    '$scope',
    'preplot',
    function($scope, preplot) {

        $scope.chartSeries = [
            {}
        ];

        $scope.chartConfig = {
            options: {
                chart: {
                    type: 'column'
                },
                plotOptions: {
                    series: {
                        stacking: ''
                    },
                    column: {
                        pointPadding: 0,
                        borderWidth: 0,
                        groupPadding: 0,
                        shadow: false
                    }
                },
                legend: {
                    enabled: false
                }
            },
            xAxis: {
            },
            yAxis: {
                title: {
                    text: "Frequency"
                }
            },
            series: $scope.chartSeries,
            title: {
                text: 'Clinical information will be previewed here.'
            },
            credits: {
                enabled: false
            },
            loading: false,
            size: {}
        };

        // so you don't need to keep calculating it over and over again on selection
        $scope.memoised = {
        };

        $scope.plotIndex = function(ind) {
            var name = $scope.$parent.project.clinical.feature_names[ind];
            $scope.plot(name);
        }

        $scope.plot = function(name) {

            if (typeof(name) != 'undefined' && name != null) {

                var plot_data;

                if ($scope.memoised[name]) {
                    var plot_data = $scope.memoised[name];
                } 

                else {

                    var data = $scope.$parent.project.clinical.features_for_plot[name];
                    $scope.numerical = preplot.guessNumerical(data, $scope.$parent.missing_values);

                    if ($scope.numerical == 'Numerical') {
                        //5 bins should be okay!
                        plot_data = preplot.toNumerical(data, 5);
                    }
                    else {
                        plot_data = preplot.toCategorical(data);
                    }

                    //finally, memoise it
                    $scope.memoised[name] = plot_data;

                }

                $scope.chartConfig.title.text = name;
                $scope.chartConfig.xAxis.categories = plot_data[0];
                $scope.chartSeries[0].data = plot_data[1];

            }
            else {
                //clear/initialise it here
                $scope.numerical = null;
                $scope.chartConfig.xAxis.categories = [];
                $scope.chartSeries[0].data = [];
                $scope.chartConfig.title.text = 'Clinical information will be previewed here.';
            }
        };

}]);
