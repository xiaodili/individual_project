// Karma configuration
// Generated on Sat Jun 21 2014 17:57:31 GMT+0100 (BST)

module.exports = function(config) {
    config.set({

        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: '..',

        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: ['jasmine'],

        // list of files / patterns to load in the browser
        files: [
            //library files
            'static/js/libs/angular.min.js',
            'static/js/libs/angular-mocks.js',
            'static/js/libs/angular-file-upload.min.js',
            'static/js/libs/angular-route.min.js',
            'static/js/libs/angular-sanitize.js',
            'static/js/libs/jquery-2.1.1.min.js',
            'static/js/libs/highcharts.js',
            'static/js/libs/highcharts-ng.min.js',
            'static/js/libs/ui-bootstrap-tpls-0.11.0.min.js',

            //the main app
            'static/js/*.js',
            'static/js/controllers/*.js',
            'static/js/directives/*.js',
            'static/js/filters/*.js',
            'static/js/services/*.js',

            //'static/js/test_js.js',
            // the files that contain the tests
            //'tests/js_unit/**/*.js'
            'tests/js_unit/**/controllers.js'
        ],

        // list of files to exclude
        exclude: [
        'static/js/*.swp',
        'static/js/libs/*.swp'
        ],


        // preprocess matching files before serving them to the browser
        // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
        preprocessors: {

        },


        // test results reporter to use
        // possible values: 'dots', 'progress'
        // available reporters: https://npmjs.org/browse/keyword/karma-reporter
        reporters: ['progress'],


        // web server port
        port: 9876,


        // enable / disable colors in the output (reporters and logs)
        colors: true,


        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,


        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,


        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        browsers: ['Chrome'],


        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: false
    });
};
