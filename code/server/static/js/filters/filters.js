angular.module('app.filters').filter('percent', function() {
    return function(input) {
        if (input == 100) {
            return 'Analysing...'
        }
        else {
            return input + '%';
        }
    };
})
.filter('guesstype', function() {
    return function(input) {
        if (input === false) {
            return 'Categorical';
        }
        else if (input === true) {
            return 'Numerical';
        }
    }
}) //these have been taken out of the implementation, but might put them back in later.
.filter('low_variance_helper', function() {

    var paramLookup = {
    };

    return function(input) {
        //return '<strong>boo</strong>'
        return '';
    }
})
.filter('univariate_helper', function() {
    var paramLookup = {
        groundTruth: function(gt) {
            if (gt) {
                return "<p><strong>" + gt + "</strong> will be used as the dependent variable.</p>";
            }
            else {
                return "";
            }
        },
        type: function(t) {
            if (t) {
                return "<p>Its values will be considered <strong>" + t.toLowerCase() + "</strong>.</p>";
            } 
            else {
                return "";
            }
        },
        mapping: function(m) {
            if (m) {
                return "<p>This will be accomplished by using the user defined mapping.</p>"
            }
            else {
                return "";
            }
        },
        func: function(f) {
            if (f) {
                var f = f.sk_func;
                if (f == 'f_classif') {
                    return "<p>The scoring function is ANOVA (Analysis of Variance)</p>"
                }
                else if (f == 'chi2') {
                    return "<p>The chi-squared value will be calculated for each micrroarray/class combination.</p>"
                }
                else if (f == 'f_regression') {
                    return "<p>A standard linear regression analysis is done: the feature are orthogonalised with respect to the dependent class, and then the cross correlation is computed. Finally, this is converted to an F-score and p-value.</p>"
                }
            }
            else {
                return "";
            }
        },
        mode: function(m, param) {
            console.log('m: ', m);
            console.log('param: ', param);
            if (param && m) {
                if (m.sk_mode == 'percentile') {
                    return "<p>The top <strong>" + param + "</strong>% of features will be kept.</p>"
                }
                else if(m.sk_mode == 'k_best') {
                    return "<p>The top <strong>" + param + "</strong> number of features will be kept.</p>";
                }
                else if(m.sk_mode == 'fpr') {
                    return "<p>All features with a p-value or less of <strong>" + param + "</strong> according to a False Positive Rate test are kept.</p>";
                }
                else if(m.sk_mode == 'fdr') {
                    return "<p>All features with a p-value or less of <strong>" + param + "</strong> for an estimated false discovery rate (using the Benjamini-Hochberg procedure) are kept.</p>";
                }
                else if(m.sk_mode == 'fwe') {
                    return "<p>All features with a p-value or less of <strong>" + param + "</strong> corrresponding to the Family-wise error rate are kept.</p>";
                }
                else {
                    return "";
                }

            }
            else {
                return "";
            }
        }
    };

    return function(input) {
        var to_return = 
            paramLookup.groundTruth(input.ground_truth) +
            paramLookup.type(input.gt_type) +
            paramLookup.mapping(input.gt_mapping) +
            paramLookup.func(input.func) +
            paramLookup.mode(input.mode, input.param);

        return to_return;
    };
});
