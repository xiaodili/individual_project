# Installation

* (optional) `virtualenv env` to create a virtual environment, then `source env/bin/actviate` to enter it.

	* `sudo pip install virtualenv` to install virtualenv

## Python dependencies

* Flask: `pip install Flask`
* requests: `pip install requests`
* numpy: `pip install numpy`
* scipy: `pip install scipy`
* matplotlib: `pip install matplotlib`

### Scikit learn dependenices

Essential:

	sudo apt-get install build-essential python-dev libatlas-dev libatlas3-base liblapack-dev gfortran libpng12-dev libfreetype6-dev

* scikit-learn: `pip install -U scikit-learn` (requires numpy and scipy to be installed first).

## MongoDB dependencies

* `sudo apt-get install MongoDB`
* `pip install pymongo`

Finally, navigate to `server/` and run `python server.py`

# Development

## Sass and Bourbon (Ruby)

(Optional) Install RVM:

* `curl -sSL https://get.rvm.io | bash`

Install ruby, if not installed:

* `rvm install ruby`

Install sass and bourbon:

* `gem install sass`
* `gem install bourbon`

Install bourbon files in `static/css/` directory:

* `bourbon install`

Compile `.scss` files with:

	sass input.scss output.css

Continuously watch files/directories with:

	# inidividual file
	sass --watch input.scss:output.css

	# directory
	sass --watch .:.


## Testing

### JavaScript

Make sure [node](http://nodejs.org/) (and npm) are installed.

* `sudo apt-get install nodejs`
* `sudo apt-get install npm`

#### Behaviour driven testing with Karma

In code root (`project/code/server`) ([source](http://karma-runner.github.io/0.12/intro/installation.html)):

1. `npm install karma --save-dev`
1. `npm install karma-jasmine karma-chrome-launcher --save-dev`

Run Karma with:

	./node_modules/karma/bin/karma start

### End to end testing with protractor

In code root (`project/code/server`) ([source](https://github.com/angular/protractor/blob/master/docs/tutorial.md)):

1. `npm install protractor`
1. `./node_modules/protractor/bin/webdriver-manager update`
1. `./node_modules/protractor/bin/webdriver-manager start`

Run Protractor with:

    ./node_modules/protractor/bin/protractor tests/protractor.conf.js


## VM

Connect to the VM with `ssh root@146.169.32.149`.

### From fresh install

Install git: `apt-get install git`
Install python2.7-dev (otherwise, numpy won't compile): `apt-get install python2.7-dev`
Install pip: `apt-get install python-pip`

