'use strict'

angular.module('app.controllers').controller('univariateCtrl', [
    '$scope',
    'preplot',
    function($scope, preplot) {
        $scope.univariate_options = {
            scoring_functions: {
                Categorical: ['f_classif'],
                Numerical :['f_regression']
            },
            modes: [
                'percentile',
                'k_best',
                'fpr',
                'fdr',
                'fwe'
            ]
        }

        //update the type
        $scope.updateUniGTType = function(type) {
            $scope.params.gt_type = type;//do nothing for now
        };

        $scope.clearUniGTMapping = function() {
            $scope.params.gt_mapping = undefined;
            $scope.params.func = undefined;
            $scope.params.gt_type = 'Categorical'
        };

        $scope.updateUniGTType = function(type) {
            $scope.params.gt_type = type;//do nothing for now
        };

        $scope.addUniGTMapping = function() {
            $scope.params.gt_mapping = {};
            $scope.params.func = undefined;
            $scope.params.gt_type = 'Numerical'
            var data = $scope.project.clinical.features_for_plot[$scope.params.ground_truth];
            var plot_data = preplot.toCategorical(data);
            var labels = plot_data[0];
            for (var i = 0, l = labels.length; i < l; i ++) {

                var v = labels[i];

                if ((!$scope.params.missing_values || $scope.params.missing_values.indexOf(v) === -1) && v !== '') {
                    $scope.params.gt_mapping[v] = undefined;
                }

            }
        };

        $scope.$watch('params.missing_values', function(new_choice, old_choice) {
            if ($scope.params && $scope.params.gt_mapping) {
                $scope.addUniGTMapping();
            }
        });

        $scope.submit = function() {
            console.log('$scope.params: ', $scope.params);
            $scope.$parent.submit($scope.params);
        };
    }
]);
