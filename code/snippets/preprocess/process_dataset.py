import numpy as np

from raw_process import raw_process
from scipreprocess import scipreprocess, scipreprocessWithHotEncoding
from gene_process import gene_process
from helpers import join
from formatting import geneToRaw

#for debuggin'
from pprint import pprint

# Pipeline so far:

# Reading from TCGA data:
TCGA = 0
RAW_DATA_PATH = "../data/TCGA_OV_raw_data.txt"
GENE_DATA_PATH = "../data/TCGA_OV_gene_expression.txt"
FEATURE_DATA = "joined.npy"
FEATURE_NAMES = "names.npy"


# Applying preprocessing from scikit learn:
PREPROCESS = 1
PREPROCESSED = "preprocessed.npy"
#SCI_FEATURE_DATA = "scijoined.npy"

#hot encoding
FEATURE_DATA_HOT = "scijoined_hot.npy"
FEATURE_NAMES_HOT = "names_hot.npy"

if __name__ == "__main__":

    if TCGA == 1:
        # Extact, category to numerical.
        # raw_data is a dict of {'TCGA-61-1895: [features], ...}
        raw_feature_names, raw_data = raw_process(RAW_DATA_PATH)

        # Get the gene expressions. 
        # gene_expressions is a dict of {'TCGA.04.133601A.01R: [features], ...}
        gene_feature_names, gene_expressions = gene_process(GENE_DATA_PATH)

        # Join them (needs to be joined, identifier strings need to be gotten rid off before values are standardised)

        raw_gene = join(gene_expressions, raw_data, geneToRaw)

        # To numpy array, so that it can be saved
        np_raw_gene = np.array(raw_gene)
        np.save(FEATURE_DATA, np_raw_gene)

        # Concatentate the gene feature names and the raw features (make sure to remove the brpatientbarcode from the raw feature names first)
        feature_names = np.array(gene_feature_names + raw_feature_names[1:])
        np.save(FEATURE_NAMES, feature_names)

    # preprocessing using scikit learn
    if PREPROCESS == 1:

        # Load up finished steps from TCGA extraction step:
        np_raw_gene = np.load(FEATURE_DATA)
        feature_names = np.load(FEATURE_NAMES)

        #data_preprocessed = scipreprocess(np_raw_gene)

        # automatic binary encoding
        data_preprocessed, hot_names = scipreprocessWithHotEncoding(np_raw_gene, feature_names)

        np_data_preprocessed = np.array(data_preprocessed)
        np.save(FEATURE_DATA_HOT, np_data_preprocessed)

        np_hot_names = np.array(hot_names)
        np.save(FEATURE_NAMES_HOT, hot_names)

