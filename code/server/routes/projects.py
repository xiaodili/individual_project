# -*- coding: utf-8 -*-
from flask import Blueprint, request, jsonify, json

# for paths etc.
import os

import mongo
import files

projects = Blueprint('projects', __name__)

@projects.route('/projects', methods=['GET'])
def to_projects():
    response = {}
    response['data'] = []

    try:
        response['data'] = mongo.getProjectNamesAndMeta()
        return jsonify(**response)
    except Exception as inst:
        return str(inst), 500

@projects.route('/projects', methods=['POST'])
def create_project():
    data = json.loads(request.data)
    project_name = data["name"]

    try:
        project_id = mongo.createNewProject(project_name)
        #finally, write the appropriate subdirectory for it:
        files.createNewProject(project_id)
        return str(project_id), 200
    except Exception as inst:
        return str(inst), 500

@projects.route('/projects/<project_id>', methods=['DELETE'])
def delete_project(project_id):
    try:
        mongo.deleteProject(project_id)
        files.deleteProject(project_id)
        return 'ok', 200
    except Exception as inst:
        return str(inst), 500

@projects.route('/projects/<project_id>/check_queue', methods=['GET'])
def check_queue(project_id):
    if mongo.queueExists(project_id):
        response = {'outcome': True}
    else:
        response = {'outcome': False}

    return jsonify(**response)

@projects.route('/projects/<project_id>', methods=['GET'])
def get_project(project_id):
    try:
        project = mongo.fetchProject(project_id)
        return jsonify(**project)
    except Exception as inst:
        return str(inst), 500

if __name__ == "__main__":
    pass
