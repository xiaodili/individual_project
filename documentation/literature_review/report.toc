\select@language {english}
\contentsline {section}{\numberline {1}Background}{1}
\contentsline {subsection}{\numberline {1.1}Introduction}{1}
\contentsline {subsection}{\numberline {1.2}Microarray technology}{2}
\contentsline {subsection}{\numberline {1.3}Machine learning}{2}
\contentsline {subsubsection}{\numberline {1.3.1}Bias-variance tradeoff}{3}
\contentsline {subsubsection}{\numberline {1.3.2}Features}{4}
\contentsline {paragraph}{Feature extraction}{4}
\contentsline {paragraph}{Feature selection}{5}
\contentsline {subsubsection}{\numberline {1.3.3}SVM}{5}
\contentsline {subsection}{\numberline {1.4}Dataset}{6}
\contentsline {section}{\numberline {2}Progress}{7}
\contentsline {subsection}{\numberline {2.1}Activities to Date}{7}
\contentsline {subsection}{\numberline {2.2}Forseen Challenges}{7}
\contentsline {subsection}{\numberline {2.3}Next Stages}{8}
\contentsline {section}{\numberline {A}Statistical formulae}{9}
\contentsline {subsection}{\numberline {A.1}Pearson coefficient}{9}
\contentsline {subsection}{\numberline {A.2}T-test}{9}
