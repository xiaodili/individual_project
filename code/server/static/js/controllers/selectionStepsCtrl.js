angular.module('app.controllers').controller('selectionStepsCtrl', [
    '$scope',
    '$location',
    '$routeParams',
    '$modal',
    '$http',
    function($scope, $location, $routeParams, $modal, $http) {
        $scope.examineRemainingFeatures = function(selection_index) {
            var path = '/projects/' + $routeParams.project_id + '/selection_results/' + selection_index;
            $location.path(path);
        };

        $scope.confirmDelete = function(index) {

            var callback = function() {
                var path = '/projects/' + $routeParams.project_id + '/selection_results/' + index;
                $http({
                    method: 'DELETE',
                    url: path,
                    data: JSON.stringify({
                        total: $scope.project.selection_steps.length
                    })
                }).
                success(function(resp) {
                    $scope.$emit('resync');
                }).
                error(function(resp) {
                    console.log('resp: ', resp);
                });
            };

            //open up a confirmation modal, before deletion
            var modalInstance = $modal.open({
                templateUrl: 'partials/modal.html',
                controller: 'modalCtrl',
                resolve: {
                    info: function() {
                        return {
                            header: "Confirm delete",
                            callback: callback,
                            message: "This feature selection step and others that follow will be irretrievably deleted. Continue?"

                        }
                    }
                }
            });
        };

        $scope.killProcess = function() {
            var callback = function() {
                var path = '/projects/' + $routeParams.project_id + '/kill_compute'
                $http({
                    method: 'GET',
                    url: path
                }).
                success(function(resp) {
                    console.log('successful. resyncing');
                    $scope.$emit('resync');
                }).
                error(function(resp) {
                    console.log('resp: ', resp);
                });
            };

            var modalInstance = $modal.open({
                templateUrl: 'partials/modal.html',
                controller: 'modalCtrl',
                resolve: {
                    info: function() {
                        return {
                            header: "Confirm cancel computation",
                            callback: callback,
                            message: "This ongoing feature selection will be canceled. Continue?"

                        }
                    }
                }
            });
        };
}]);
