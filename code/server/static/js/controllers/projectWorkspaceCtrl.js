'use strict'

angular.module('app.controllers').controller('projectWorkspaceCtrl', [
    '$scope',
    '$routeParams',
    '$http',
    '$location',
    '$interval',
    function($scope,
             $routeParams,
             $http,
             $location,
             $interval
            ) {

    //poll, if polling for queued
    $scope.poll = undefined;
    var cancelPoll = function(p) {
        $interval.cancel(p);
        return undefined;
    };

    var createPoll = function() {

        return $interval(function() {
            
            var url = '/projects/' + $routeParams.project_id + '/check_queue';
            $http({ method: 'GET',
                url: url
            })
            .success(function(data, status, headers, config) {
                // if queued has been deleted, then cancel the poll, and then resync.
                if (!data.outcome) {
                    $scope.poll = cancelPoll($scope.poll);
                    getProject();
                }
                else {
                    //do nothing
                }
            })
            .error(function(data, status, headers, config) {
            });

        }, 5000);
    };

    var getProject = function() {
        var path = '/projects/' + $routeParams.project_id;
        $http
        .get(path)
        .success(function(data) {
            console.log('data: ', data);
            $scope.project = data;

            //destroy any polls, if they exist
            if ($scope.poll) {
                $scope.poll = cancelPoll($scope.poll);
            }

            // should another poll be created?
            if ($scope.project.queued) {
                $scope.poll = createPoll();
            }

        })
        .error(function(resp) {
            console.log('resp: ',resp);
        });

        $scope.$on('$destroy', function() {
            $interval.cancel($scope.poll);
        });
    };

    //initialising:
    getProject();

    //when stuff changes, make sure to resync!
    $scope.$on('resync', function() {
        getProject();
    });

    //debugging purposes
    $scope.logProject = function() {
        console.log('$scope.project: ', $scope.project);
    };

}]);
