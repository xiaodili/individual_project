'use strict'

angular.module('app.controllers').controller('embeddedCtrl', [
    '$scope',
    'preplot',
    function($scope, preplot) {
        $scope.embedded_options = {
            methods: {
                Categorical: ['rfe'],
                Numerical: ['lasso']
            }
        }

        $scope.rfe_options = {
            kernel: ['linear']
        }

        //update the type
        $scope.updateUniGTType = function(type) {
            $scope.params.gt_type = type;//do nothing for now
        };

        $scope.clearUniGTMapping = function() {
            $scope.params.gt_mapping = undefined;
            $scope.params.func = undefined;
            $scope.params.gt_type = 'Categorical'
        };

        $scope.updateUniGTType = function(type) {
            $scope.params.gt_type = type;//do nothing for now
        };

        $scope.addUniGTMapping = function() {
            $scope.params.gt_mapping = {};
            $scope.params.func = undefined;
            $scope.params.gt_type = 'Numerical'
            var data = $scope.project.clinical.features_for_plot[$scope.params.ground_truth];
            var plot_data = preplot.toCategorical(data);
            var labels = plot_data[0];
            for (var i = 0, l = labels.length; i < l; i ++) {

                var v = labels[i];

                if ((!$scope.params.missing_values || $scope.params.missing_values.indexOf(v) === -1) && v !== '') {
                    $scope.params.gt_mapping[v] = undefined;
                }

            }
        };

        $scope.$watch('params.missing_values', function(new_choice, old_choice) {
            if ($scope.params && $scope.params.gt_mapping) {
                $scope.addUniGTMapping();
            }
        });

        // initialising:
        $scope.params = {
            to_integrate: {}
        }

        $scope.submit = function() {

            //get a type from the clinical feature:
            var getType = function(c_name) {
                var arr = $scope.project.clinical.features_for_plot[c_name];
                return convertToFeatureType(preplot.guessNumerical(arr, $scope.params.missing_values));
            }

            var convertToFeatureType  = function(type) {
                if (type === 'Categorical') {
                    return 'Binary';
                } else {
                    return 'Numeric';
                }
            }

            var convertToGTType = function(type) {
                if (type == 'Categorical') {
                    return 'Multiclass';
                } else {
                    return 'Numeric';
                }
            };

            var getClinicalTypes = function(to_integrate_obj) {
                var to_return = [];
                for (key in to_integrate_obj) {
                    if (to_integrate_obj.hasOwnProperty(key) && to_integrate_obj[key]) {
                        to_return.push({
                            name: key,
                            action: getType(key)
                        });
                    }
                }
                return to_return;
            };
            var data = {};
            //copy over the parameters from the scope
            for (var key in $scope.params) {
                if ($scope.params.hasOwnProperty(key)) {
                    data[key] = $scope.params[key];
                }
            }

            data.clinical_instructions = {
                to_integrate: getClinicalTypes($scope.params.to_integrate),
                ground_truth: {
                    name: $scope.params.ground_truth,
                    action: convertToGTType($scope.params.gt_type)
                }
            }

            //get rid of redundant keys:
            delete data.to_integrate;
            delete data.gt_type;
            delete data.ground_truth;

            console.log('data: ', data);
            $scope.$parent.submit(data);
        };
    }
]);
