from flask import Blueprint, request, jsonify
import json
import mongo
import requests
from HTMLParser import HTMLParser
import urllib
import parse
import files

selection = Blueprint('selection', __name__)

@selection.route('/projects/<project_id>/selection_results/<index>', methods=['GET'])
def get_selection_result(project_id, index):
    try:
        zero_index = int(index) - 1
        response = getSelectionByIndex(project_id, zero_index)

        # if annotation is present, perform mapping:
        if mongo.annotationMapped(project_id):
            annotation_results = mongo.getAnnotationResult(project_id)
            annotation_file = files.loadAnnotation(project_id)
            annotation_mapping = dict(parse.extractColumns(annotation_file, annotation_results['p_index'], annotation_results['g_index']))
            performAnnotationMapping(response['results'], annotation_results)

        return jsonify(**response)

    except Exception as inst:
        return str(inst), 500

def performAnnotationMapping(result, annot_mapping):
    probe_names = result['remaining_features']
    new_probe_names = []
    for p in probe_names:
        try:
            new_probe_names.append(annot_mapping[p])
        # if no mapping exists, just use regular probe name
        except:
            new_probe_names.append(p)

    result['remaining_features'] = new_probe_names


def getSelectionByIndex(project_id, index):
    return mongo.getSelectionByIndex(project_id, index)

@selection.route('/projects/<project_id>/selection_results/<index>', methods=['DELETE'])
def splice_features(project_id, index):
    data = json.loads(request.data)
    total = data['total']
    try:
        spliceFeatures(project_id, int(index), total)
        return 'ok', 200
    except Exception as inst:
        return str(inst), 500

def spliceFeatures(project_id, index, length):
    times = length - index

    for i in xrange(times):
        mongo.popSelectionStep(project_id)

if __name__ == '__main__':
    def checkSpliceFeatures():
        rabbits = "53ed2569632fc70fe2af1f9b"
        total = 1
        index = 0

        try:
            spliceFeatures(rabbits, index, total)
            return True
        except:
            return False

    #if checkSpliceFeatures():
        #print 'checkSpliceFeatures passed'
    #else:
        #print 'checkSpliceFeatures failed'

    try: 
        print 'making request now...'
        makeReflectCall()
    except Exception as inst:
        print inst
