# -*- coding: utf-8 -*-
import os
import shutil

# path of where this script lies
PATH = os.path.dirname(os.path.realpath(__file__))

def createNewProject(project_id):
    project_path = PATH + '/../projects/' + project_id

    if not os.path.exists(project_path):

        os.makedirs(project_path)

        raw_path = project_path + '/raw'
        os.makedirs(raw_path)
    else:
        print 'already exists'

def deleteProject(project_id):
    project_path = PATH + '/../projects/' + project_id

    if os.path.exists(project_path):
        shutil.rmtree(project_path)

def writeClinical(project_id, c_file):
    project_path = PATH + '/../projects/' + project_id
    clinical_file_path = project_path + '/raw/clinical.txt'
    c_file.save(clinical_file_path)

    return open(clinical_file_path, 'r')

def writeMicroarray(project_id, m_file):
    project_path = PATH + '/../projects/' + project_id
    microarray_file_path = project_path + '/raw/microarray.txt'
    m_file.save(microarray_file_path)

    return open(microarray_file_path, 'r')

def writeMapping(project_id, m_file):
    project_path = PATH + '/../projects/' + project_id
    mapping_file_path = project_path + '/raw/mapping.txt'
    m_file.save(mapping_file_path)

    return open(mapping_file_path, 'r')

def writeAnnotation(project_id, a_file):
    project_path = PATH + '/../projects/' + project_id
    annotation_file_path = project_path + '/raw/annotation.txt'
    a_file.save(annotation_file_path)

    return open(annotation_file_path, 'r')

def loadAnnotation(project_id):
    project_path = PATH + '/../projects/' + project_id
    annotation_file_path = project_path + '/raw/annotation.txt'
    return open(annotation_file_path, 'r')

def loadMapping(project_id):
    project_path = PATH + '/../projects/' + project_id
    mapping_file_path = project_path + '/raw/mapping.txt'
    return open(mapping_file_path, 'r')

def loadClinical(project_id):
    project_path = PATH + '/../projects/' + project_id
    clinical_file_path = project_path + '/raw/clinical.txt'
    return open(clinical_file_path, 'r')

def loadMicroarray(project_id):
    project_path = PATH + '/../projects/' + project_id
    microarray_file_path = project_path + '/raw/microarray.txt'
    return open(microarray_file_path, 'r')


if __name__ == '__main__':

    # creating a new project
    #project_id = "53de4cb58c87e90934a2a243"
    #createNewProject(project_id)

    #oh... turns out this can't be mocked. Needs to be a fileobject. yikes...
    #clinical = open("../projects/sample_project/raw/TCGA_OV_raw_data.txt", 'r')
    #clinical = writeClinical(project_id, clinical)

    print 'done'
