from flask import Flask, request, jsonify
from pprint import pprint


# import the relevant routes:
from routes.projects import projects
from routes.file_upload import uploads
from routes.selection import selection
from routes.ml import ml
from routes.debug import debug

# Serve requests from / with the static directory
app = Flask(__name__, static_url_path='')

# you need to register the routes as well
app.register_blueprint(projects)
app.register_blueprint(uploads)
app.register_blueprint(selection)
app.register_blueprint(ml)
#from routes.ml import ml
app.register_blueprint(debug)

# Always serve index.html
@app.route('/')
def root():
    return app.send_static_file('index.html')

if __name__ == '__main__':

    # Turn on debugging mode
    app.debug = True

    # Turn on logging if debug mode is off
    if app.debug is not True:   

        import logging
        from logging.handlers import RotatingFileHandler
        file_handler = RotatingFileHandler('logs/errors.log', maxBytes=1024 * 1024 * 100, backupCount=20)
        file_handler.setLevel(logging.ERROR)
        formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
        file_handler.setFormatter(formatter)
        app.logger.addHandler(file_handler)

    # Run the app
    app.run()

