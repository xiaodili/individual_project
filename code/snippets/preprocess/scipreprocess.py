from sklearn import preprocessing
import numpy as np

# Private methods:

def hotEncodeFeatures(data, names, indices):
    #enc = preprocessing.OneHotEncoder()
    #enc.fit()
    #enc.transform().toarray()
    data_to_return = []
    name_to_return = []
    for i in xrange(len(names)):
        if i not in indices:

            # append the name:
            name_to_return.append(names[i])

            # append the column:
            if len(data_to_return) == 0:
                print "this is okay"
                data_to_return = data[:,i]
            else:
                print data_to_return.shape
                print data[:,i].shape
                data_to_return = np.concatenate(data_to_return, data[:,i])

        # otherwise, these need to be hot encoded
        else:
            enc = preprocessing.OneHotEncoder()
            to_encode = [[s] for s  in data[:,i].tolist()]
            enc.fit(to_encode)
            to_append = enc.transform(to_encode).toarray()
            to_append = np.array(to_append)
            data_to_return = np.concatenate(data_to_return, to_append)

            #get number of encodings:
            num_encodings = len(to_append[0])

            for n in xrange(num_encodings):
                name_to_return.append(name[i] + str(n))
    
    return data_to_return, name_to_return


# Exposed methods:
#------------------------------
# manual intervention method:

def scipreprocess(data):

    # First, impute the missing data:
    Imputer = preprocessing.Imputer

    # impute along the y axis: 0 (columns)
    imp = Imputer(missing_values = 'NaN', strategy = 'mean', axis = 0)

    # Fit imputer with the data
    imp.fit(data)

    # Impute those values:
    data = imp.transform(data)

    # Perform standardisation.
    scaler = preprocessing.StandardScaler().fit(data)

    return scaler.transform(data)

# currently receiving 
def scipreprocessWithHotEncoding(data, names):

    #print 'data dims:', data.shape

    clinical = data[:,-12:]

    clinical_names = names[-12:]

    #print 'clinical dims:', clinical.shape

    microarray = data[:,:-12]
    microarray_names = names[:-12]

    # First, impute the missing data:
    Imputer = preprocessing.Imputer

    ## impute along the y axis: 0 (columns). Because of hot encoding, use most frequent imputation instead
    imp = Imputer(missing_values = 'NaN', strategy = 'most_frequent', axis = 0)

    ## Fit imputer with the data:
    imp.fit(clinical)

    # impute for clinical:
    clinical_imputed = imp.transform(clinical)

    #print clinical_names

    # features selected for hot encoding (everything apart from age and vital status)
    clinical_encoded, names_encoded = hotEncodeFeatures(clinical_imputed, clinical_names, [2, 4, 5, 8, 11])

    print 'encoded clinical dimensions:', len(clinical_encoded[0]), len(clinical_encoded)

    return np.concatenate(micorarray,clinical_encoded), microarray_names + names_encoded
