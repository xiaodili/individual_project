import csv
from pprint import pprint

PREVIEW_COLUMNS = 5
PREVIEW_ROWS = 4

# parses the file and returns everything in the clinical section
def parseClinical(c_file):

    # internal functions
    def createFeaturesForPlot(f_names, f_bucket):
        ffp = {}
        for i in xrange(len(f_names)):
            #ffp[f_names[i]] = {'data': f_bucket[i]}
            ffp[f_names[i]] = f_bucket[i]

        return ffp

    clinical = {}
    meta = {}
    num_samples = 0

    # getting the header
    reader = csv.reader(c_file, delimiter='\t')

    # feature names are all the entries in the first row apart from the first one (which contains patient name field)
    headers = reader.next()

    feature_names = headers[1:]
    sample = '\t'.join(headers[:PREVIEW_COLUMNS]) + '\t...\n'

    patient_names = []

    # preparing the structures required for features_for_plot
    features_bucket = [[] for i in xrange(len(feature_names))]

    # getting the rest of it:
    for row in reader:
        num_samples += 1

        # get four lines, 5 tabs:
        if num_samples <= PREVIEW_ROWS:
            sample += '\t'.join(row[:PREVIEW_COLUMNS]) + '\n'

        for i, r in enumerate(row):

            # append first entry to patient names
            if i == 0:
                patient_names.append(r)
            # otherwise, append to the features
            else:
                features_bucket[i-1].append(r)


    sample += '...'

    # creating the meta object:
    meta['num_features'] = len(feature_names)
    meta['num_samples'] = num_samples
    meta['sample'] = sample

    # creating features_for_plot:
    features_for_plot = createFeaturesForPlot(feature_names, features_bucket)

    # creating the clinical object:
    clinical['meta'] = meta
    clinical['feature_names'] = feature_names
    clinical['patient_names'] = patient_names
    clinical['features_for_plot'] = features_for_plot

    print sample

    return clinical

def guessCategory():
    pass

# return the microarray object as defined in the mock projects.py
def parseMicroarray(m_file):
    reader = csv.reader(m_file, delimiter='\t')

    headers = reader.next()

    patient_names = headers[1:]
    sample = '\t'.join(headers[:PREVIEW_COLUMNS]) + '\t...\n'

    num_samples = len(patient_names)
    num_features = 0

    microarray = {}
    meta = {}
    probe_names = []

    for row in reader:
        num_features += 1

        # get four lines, 5 tabs:
        if num_features <= PREVIEW_ROWS:
            sample += '\t'.join(row[:PREVIEW_COLUMNS]) + '\n'

        probe_names.append(row[0])

    sample += '...'

    meta['num_features'] = num_features
    meta['num_samples'] = num_samples
    meta['sample'] = sample

    microarray['meta'] = meta
    microarray['probe_names'] = probe_names
    microarray['patient_names'] = patient_names

    return microarray

# return the mapping object as defined in themock projects.py
def parseMapping(m_file):
    reader = csv.reader(m_file, delimiter='\t')
    headers = reader.next()
    columns = headers
    sample = '\t'.join(headers) + '\n'
    num_mappings = 0

    for row in reader:
        num_mappings += 1

        if num_mappings <= PREVIEW_ROWS:
            sample += '\t'.join(row) + '\n'

    sample += '...'

    mapping = {}
    meta = {}
    meta['num_mappings'] = num_mappings
    meta['sample'] = sample

    mapping['meta'] = meta
    mapping['columns'] = columns

    return mapping

def parseAnnotation(a_file):
    reader = csv.reader(a_file, delimiter='\t')
    headers = reader.next()
    columns = headers
    sample = '\t'.join(headers) + '\n'
    num_annotations = 0

    for row in reader:
        num_annotations += 1

        if num_annotations <=PREVIEW_ROWS:
            sample += '\t'.join(row) + '\n'

    sample += '...'

    meta = {}
    meta['num_annotations'] = num_annotations
    meta['sample'] = sample

    annotation = {}
    annotation['meta'] = meta
    annotation['columns'] = columns

    return annotation

def extractColumns(as_file, ind1, ind2):
    reader = csv.reader(as_file, delimiter='\t')
    headers = reader.next()

    ind1_list = []
    ind2_list = []

    for row in reader:
        try:
            ind1_list.append(row[ind1])
            ind2_list.append(row[ind2])
        except:
            pass

    return zip(ind1_list, ind2_list)

# For subprocess functionality: return the probe as converted to floats, and also the order the patient IDs have been passed in
def getMicroarrayByProbe(m_file):
    reader = csv.reader(m_file, delimiter='\t')
    patient_ids = reader.next()[1:]

    probe_dict = {}

    for row in reader:
        key = row[0]
        features = row[1:]
        probe_dict[key] = [float(f) for f in features]

    return probe_dict, patient_ids

def getClinicalByPatient(c_file):
    reader = csv.reader(c_file, delimiter='\t')
    clinical_feature_names = reader.next()[1:]
    patient_dict = {}
    for row in reader:
        key = row[0]
        features = row[1:]
        patient_dict[key] = features
    
    return patient_dict, clinical_feature_names

