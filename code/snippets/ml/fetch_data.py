# module used to copy files
from shutil import copyfile

FROM = "../preprocess/"
TO = "./"

FILES = ["scijoined.npy", "names.npy", "joined.npy","scijoined_hot.npy", "names_hot.npy"]

print "copying:"

for f in FILES:
    print f
    copyfile(FROM + f, TO + f)
    print "done"
