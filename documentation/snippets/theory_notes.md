# Machine learning

* Microarray technology allows scientists to monitor expression of genes on a genomic scale.
* Increases the possibility of cancer classification and diagnosis at the gene expression level.


## Supervised Machine learning methods

### SVM (support vector machines)

* Aim is to obtain the maximum margin.
* Four basic kernels: linear, polynomial, radial basic function, and sigmoid.
* binary SVM can predict the outcomes of samples, e.g. either positive or negative, by finding the hyperplane with the maximum margin.
* The C parameter tells the SVM optimisation how much to avoid misclassifying each training example. For large values of C, optimisation will choose a smaller margin hyperplane to avoid misclassifying the points. For small values of C, SVM will look for larger max margin at the cost of misclassifying more points.

### SVM with recursive feature elimination

### RBF (radial basis function kernel) 

### Neural Nets

Two common ANN algorithms are the Multi-layer perceptron (MLP) and the radial basis function (RBF) networks.

##### MLP (Multilayer perceptron)

Feedforward artificial neural network model that maps sets of input data to set of appropriate outputs.
Multiple layers of nodes in a directed graph, with each layer fully connected to the next one.
Each node is a neuron with a nonlinear activation function.
Supervised learning technique called backpropagation for training the network.

#### Decision Tree

* Leaves represent classifications and branches represent conjunctions of features that lead to those classifications.
* Multiple output attributes are not allowed in decision trees, algorithms are unstable.
* Slight variations in training data can result in different attribute selections at each choice point within the tree.

Example algorithms are ID3, which is based on the CLS (Concept Learning System).

#### Bayesian network

* Bayesian network represents independencies over a set of variables in a given joint probability distribution (JPD).
* Nodes correespond to variables of interest, and arcs between two nodes represent statistical dependence between variables.

#### Random Forests

## Unsupervised Machine learning methods

* Known as unsupervised clustering.
* Divide groups into predetermined number of groups in a manner that maximises a specific function.
* Common unsupervised learning methods are k-means algorithm, Farthest First Traversal Algorithm, Density-based clustering, Expectation Maximisation (EM) Clustering are four common methods used.

### K-means

### Nearest Neighbours

Plots the decision boundaries for each class.

The datum in question is classified as the majority of its N nearest neighbours .

The basic classification uses uniform weights (each neighbor contributes the same amount of influence to swaying the datum); another options would be to give more weight to the neighbours that are closer to the datum.

### DBC (density based clustering)

### Expectation-maximisation algorithm

## Feature selection

* Data contains redundant or irrelevant features.
* Can be divided into wrapper and filter model.
* Wrapper model uses predictive accuracy of a mining algorithm to determin goodness of a selected subset.
* Wrapper methods generally result in better performance than filter methods, because latter suffers from potential drawback that the feature selection principle and the classification step do not necesarily optimise the same objective function.
* Filter model is more efficent in term of computation, thus are better suited to high dimensional data sets.

## Cross validation

Partitioning into three sets greatly reduces the number of samples which can be used to learning the model. Instead, do cross-validation instead:

* Test set held out for final evaluation, validation set no longer required. Instead, training set split into *k* smaller sets, known as folds.
* Model is trained using k-1 folds.
* Model is validated using the remaining part of the data
* Performance measure by k-fold cross validation is average of values computed in the loop.
* sklearn provides the `cross_val_score`helper function on the estimator and the dataset.
*
### V-fold cross validation

* Cross validation in general assesses how the results of a statistical analysis will generalise to an independent data set.
* Goal is prediction; one wants to estimate how accurately a predictive model will perform in practice.
* Cross validation tests the model in the training phase, i.e. the validation dataset, in order to limit problems like overfitting, and to address Type III problems (correctly rejecting null hypothesis for the wrong reasons).

# Cancer

## Tumor stage

Staging describes the severity of a person’s cancer based on the size and/or extent (reach) of the original (primary) tumor and whether or not cancer has spread in the body. 

e.g. IIA, IIB, IIC

Assume linear, according to this table:

http://www.cancer.org/cancer/ovariancancer/detailedguide/ovarian-cancer-staging

## Tumor grade

Tumor grade is the description of a tumor based on how abnormal the tumor cells and the tumor tissue look under a microscope. It is an indicator of how quickly a tumor is likely to grow and spread.

e.g. G1, G2, G3

# RDF

Resource description framework format, designed to be a metadata data model.

## SQARQL

An implementation of the RDF query language

# Pipeline

Label encode > Impute > OneHotEncoder > Standardisaion
