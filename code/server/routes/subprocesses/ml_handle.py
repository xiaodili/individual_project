# -*- coding: utf-8 -*-
import sklearn.feature_selection as fs
from sklearn.linear_model import Lasso
import numpy as np
from sklearn.svm import SVC
from sklearn import preprocessing
from sklearn import cross_validation
from sklearn.feature_selection import RFECV

# need this to convert microarray to 2d array
import preprocess

NUMERICAL = 0
CATEGORICAL = 1

# internal functions
# --------------------------------------------------

# --------------------------------------------------

# Returns biased variance
def varianceThreshold(config, probes):
    # Get the transpose:
    X, probe_names = preprocess.microToX(probes)

    threshold = config['threshold']

    sel = fs.VarianceThreshold(threshold=threshold)
    sel.fit(X)
    #try:
        #sel.fit(X)
    ## if no feature in X fits this criteria, return empty list
    #except Exception as inst:
        #return [], []

    # with the indices, True indicates that the probe feature has been selected
    selected_indices_mask = sel.get_support()

    probes_left = []
    augmented_results = {
            'variances': []
            }
    # iterate through the indices, then return the accepted ones
    for i, select in enumerate(selected_indices_mask):
        if select:
            probe = probe_names[i]
            probes_left.append(probe)
            augmented_results['variances'].append(sel.variances_[i])

    return probes_left, augmented_results

# internal function for MAD handler
def _getMAD(arr):
    """ Median Absolute Deviation: a "Robust" version of standard deviation.
    Indices variabililty of the sample.
    https://en.wikipedia.org/wiki/Median_absolute_deviation 
    """
    arr = np.ma.array(arr).compressed() # should be faster to not use masked arrays.
    med = np.median(arr)
    return np.median(np.abs(arr - med))

# MAD handler
def MAD(config, probes):
    X = probes.values()
    probe_names = probes.keys()
    mad_threshold = config['threshold']
    X_mads = [_getMAD(x) for x in X]
    support_mask = np.array(X_mads) > mad_threshold

    probes_left = []
    augmented_results = {
            'MAD': []
            }
    for i, select in enumerate(support_mask):
        if select:
            probe = probe_names[i]
            probes_left.append(probe)
            augmented_results['MAD'].append(X_mads[i])

    return probes_left, augmented_results

# Univariate selection handler
def univariate(project_id, config, probes, patient_ids, subject_mapping_columns):

    def getGroundTruthEnum(ground_truth_type):
        if ground_truth_type == 'Categorical':
            return CATEGORICAL
        else:
            return NUMERICAL

    # perform join for the correct ordering:
    integrated_X, feature_names = preprocess.performJoin(project_id, probes, patient_ids, subject_mapping_columns)

    # split it up again, in converting to numpy format:
    X, clinical = preprocess.splitIntegratedFeatures(integrated_X, len(probes))
    probe_names, c_names = preprocess.splitIntegratedFeatureNames(feature_names, len(probes))

    # extract out information about the ground truth
    gt_name = config['ground_truth']
    gt_index = c_names.index(gt_name)
    ground_truth = clinical[:, gt_index]
    gt_type = getGroundTruthEnum(config['gt_type'])

    # carry out imputation, any additional mapping
    try:
        missing_values = config['missing_values']
    except:
        missing_values = []

    try: 
        mappings = config['mappings']
    except:
        mappings = {}

    y = preprocess.handleGroundTruth(ground_truth, missing_values, mappings, gt_type)

    # Running the univariate function here:

    # pick the function out of the module
    func = getattr(fs, config['func'])

    mode = config['mode']
    param = config['param']
    sel = fs.GenericUnivariateSelect(score_func = func, mode = mode, param = param)
    sel.fit(X, y)
    selected_indices_mask = sel.get_support()

    selected_probes = []
    augmented_results = {
            'log10': []
            }
    # iterate through the indices, then return the accepted ones
    for i, select in enumerate(selected_indices_mask):
        if select:
            probe = probe_names[i]
            selected_probes.append(probe)
            # return the log10 of p values
            augmented_results['log10'].append(-np.log10(sel.pvalues_[i]))

    return selected_probes, augmented_results


# RFE handler
def rfe(project_id, config, probes, patient_ids, subject_mapping_columns):

    # perform join for the correct ordering:
    integrated_X, feature_names = preprocess.performJoin(project_id, probes, patient_ids, subject_mapping_columns)

    # split it up again, in converting to numpy format:
    microarray, clinical = preprocess.splitIntegratedFeatures(integrated_X, len(probes))

    probe_names, c_names = preprocess.splitIntegratedFeatureNames(feature_names, len(probes))

    clinical_instructions = config['clinical_instructions']
    try:
        missing_values = config['missing_values']
    except:
        missing_values = []

    pre_clinical, pre_c_names, y, y_name = preprocess.prepareIntegratedClinical(clinical, c_names, clinical_instructions, missing_values)

    X, names_final = preprocess.concatMicroAndClinical(microarray, probe_names, pre_clinical, pre_c_names)

    # standardise it:
    X = preprocessing.scale(X.astype(np.float))
    y = preprocessing.LabelEncoder().fit_transform(y)

    try:
        kernel = config['kernel']
    except:
        config['kernel'] = 'linear'

    try:
        C = config['C']
    except:
        config['C'] = 1

    svc = SVC(kernel = str(kernel), C = C)

    # unimplemented here
    try:
        select_k = config['select_k']
    except:
        select_k = 'auto';
        config['select_k'] = 'auto'

    rfecv = RFECV(estimator = svc, cv=cross_validation.StratifiedKFold(y), step = 1, scoring='accuracy')
    rfecv.fit(X, y)

    scores = cross_validation.cross_val_score(svc, X, y, cv = cross_validation.StratifiedKFold(y))

    probes_left = []

    # can decide what to do with this on the client later
    augmented_results = {}
    augmented_results['feature_names'] = names_final
    augmented_results['features_ranked'] = rfecv.ranking_.tolist()

    augmented_results['scores'] = scores.tolist()

    # create a complete ranking for augmented results:

    if select_k == 'auto':
        for i, e in enumerate(rfecv.support_):
            if e and i < len(probe_names):
                probes_left.append(probe_names[i])
    else:

        probes_rankings = rfecv.ranking_[:len(probe_names)]
        probe_indexed = list(enumerate(probes_rankings))
        probe_indexed.sort(key = lambda x: x[1])

        if select_k < len(probe_indexed):
            probes_left = [probe_names[x[0]] for x in probe_indexed[:select_k]]
        else:
            probes_left = [probe_names[x[0]] for x in probe_indexed]

    return probes_left, augmented_results

# LASSO handler; not implemented
def lasso(project_id, config, probes, patient_ids, subject_mapping_columns):

    # perform join for the correct ordering:
    integrated_X, feature_names = preprocess.performJoin(project_id, probes, patient_ids, subject_mapping_columns)

    # split it up again, in converting to numpy format:
    microarray, clinical = preprocess.splitIntegratedFeatures(integrated_X, len(probes))

    probe_names, c_names = preprocess.splitIntegratedFeatureNames(feature_names, len(probes))

    clinical_instructions = config['clinical_instructions']
    try:
        missing_values = config['missing_values']
    except:
        missing_values = []

    pre_clinical, pre_c_names, y, y_name = preprocess.prepareIntegratedClinical(clinical, c_names, clinical_instructions, missing_values)

    X, names_final = preprocess.concatMicroAndClinical(microarray, probe_names, pre_clinical, pre_c_names)

    alpha = config['alpha']
    clf = Lasso(alpha=alpha)
    clf.fit(X, y)

    augmented_results = {}
    augmented_results['notable_clinical_features'] = []
    probes_left = []
    selected_indices = [i for i, e in enumerate(clf.coef_) if e != 0]
    for s in selected_indices:
        if s < len(probe_names):
            probes_left.append(probe_names[s])
        else:
            augmented_results['notable_clinical_features'].append(names_final[s])

    # can augment further actually!
    return probes_left, augmented_results

# Handler for embedded method
def embedded(project_id, config, probes, patient_ids, subject_mapping_columns):
    if config['func'] == 'rfe':
        return rfe(project_id, config, probes, patient_ids, subject_mapping_columns)
    else: # lasso
        return lasso(project_id, config, probes, patient_ids, subject_mapping_columns)

# Jumping off point
def handle(project_id, ml_instructions, probes, patient_ids, subject_mapping_columns):

    handler = ml_instructions['name']
    config = ml_instructions['config']

    if handler == 'MAD':
        features_left = MAD(config, probes)
    elif handler == 'VarianceThreshold':
        features_left = varianceThreshold(config, probes)
    elif handler == 'Univariate':
        features_left = univariate(project_id, config, probes, patient_ids, subject_mapping_columns)
    elif handler == 'Embedded':
        features_left = embedded(project_id, config, probes, patient_ids, subject_mapping_columns)
    else:
        raise LookupError("Yikes, can't seem to find the handler: " + handler)

    return features_left
