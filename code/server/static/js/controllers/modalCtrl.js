angular.module('app.controllers').controller("modalCtrl", ['$scope', '$modalInstance', 'info', function($scope, $modalInstance, info) {
    $scope.info = info;
    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    };
    $scope.ok = function() {
        $modalInstance.dismiss('cancel');
        info.callback();
    };
}]);
