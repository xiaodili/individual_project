# -*- coding: utf-8 -*-

# This needs to pass in the configuration options for the classifier, from mongodb. However, the subprocess needs to be able to be able to write to the filter step
# 
from flask import Blueprint, request, jsonify, json
import subprocess
import sys
import os
import mongo
import signal

ml = Blueprint('ml', __name__)

# path of where this script lies
PATH = os.path.dirname(os.path.realpath(__file__))

# write the appropriate configuration files to the mongo queue
# then spawn the subprocess that reads from the db to process that request
@ml.route('/projects/<project_id>/ml', methods=['GET', 'POST'])
def handle_ml(project_id):

    data = json.loads(request.data)
    try:
        result = handleML(project_id, data)
        return 'ok', 200
    except Exception as inst:
        print inst
        return str(inst), 500

# function that actually does the work (write to mongo, spawn subprocess)
def handleML(project_id, data):

    SUBPROCESS_PATH = PATH + '/subprocesses'
    SUBPROCESS_SCRIPT = SUBPROCESS_PATH + '/start.py'

    mongo.writeToQueued(project_id, data)

    # potential race condition:
    p = subprocess.Popen([sys.executable, SUBPROCESS_SCRIPT, project_id])

    child_pid = p.pid
    mongo.addPID(project_id, child_pid) # don't insert a new queued object if the subprocess has already finished by the time you get around to adding the PID

@ml.route('/projects/<project_id>/kill_compute', methods=['GET'])
def kill_compute(project_id):
    result = killCompute(project_id)
    return jsonify(**{'outcome': result})

def killCompute(project_id):
    try:
        # if the task has already been completed successfully, getPID will throw a keyerror
        pid = mongo.getPID(project_id)

        try:
            os.kill(pid, signal.SIGTERM)
        except:
            # already dead
            pass

        mongo.removeFromQueued(project_id)
        return True
    except Exception as inst:
        # queue has already been taken off; keyerror on pid
        print inst
        return False

if __name__ == '__main__':

    def checkVarianceThreshold():
        #sample_project = '53ed2569632fc70fe2af1f9b'
        rabbits = "53ed2569632fc70fe2af1f9b"
        data = {
                'name': 'VarianceThreshold',
                'config': {
                    'threshold': 1
                    }
                }
        return handleML(rabbits, data)

    def checkUnivariate():
        rabbits = "53ed2569632fc70fe2af1f9b"

        # take this from the documentation here:
        # http://scikit-learn.org/stable/modules/generated/sklearn.feature_selection.GenericUnivariateSelect.html#sklearn.feature_selection.GenericUnivariateSelect
        data = {
                'name': 'Univariate',
                'config': {
                    'ground_truth_index': 1,
                    'missing_values': ['Missing'],
                    'mapping': {
                        },
                    'score_func': 'f_classif',
                    'mode': 'percentile',
                    'param': 10
                    }
                }

        return handleML(rabbits, data)

    def checkProcessKill():
        rabbits = "53ed2569632fc70fe2af1f9b"
        pid = 18736
        mongo.addPID(rabbits, pid)
        return killCompute(rabbits)

    def checkLasso():
        rabbits = "53ed2569632fc70fe2af1f9b"
        data = {
                'name': 'Lasso',
                'config': {
                    'alpha': 1,
                    'missing_values': ['Missing'],
                    'fit_boolean': False,
                    'normalize': False,
                    'precompute': 'auto',
                    'clinical_instructions': {
                        'ground_truth': {
                            'name': 'VITALSTATUS',
                            'action': 'Multiclass' # has to be numerical, right? Or manually assign. Then, it would still be numerical.
                            },
                        'integrated': [
                            {
                                'name': 'TUMORSTAGE',
                                'action': 'Binary'
                                },
                            {
                                'name': 'AgeAtDiagnosis (yrs)',
                                'action': 'Numeric'
                                }
                            ]}
                    }
                }

        return handleML(rabbits, data)

    def checkrfe():
        pass


    #if checkVarianceThreshold():
        #print 'checkVarianceThreshold passed'
    #else:
        #print 'checkVarianceThreshold failed'

    if checkUnivariate():
        print 'checkUnivariate passed'
    else:
        print 'checkUnivariate failed'

    #if checkProcessKill():
        #print 'checkProcessKill passed'
    #else:
        #print 'checkProcessKill failed'
