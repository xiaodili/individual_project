# -*- coding:utf-8 -*-

import csv
import copy

#path_name = "../../projects/sample_project/raw/"
#file_name = "TCGA_OV_raw_data.txt"

# copied from parse
def returnInfo(filename):
    # get number of lines:
    #with open(filename, 'r') as contents:
        #size = len(contents.readlines()) - 1

    column_dict = {}

    # increment this size up
    num_samples = 0

    with open(filename, 'r') as contents:

        biotab_reader = csv.DictReader(contents, delimiter='\t')
        for row in biotab_reader:
            num_samples += 1
            # iterate through the keys. add them to column dict if it doesn't exist, and then append the values
            for key in row:

                if key not in column_dict:
                    column_dict[key] = []

                column_dict[key].append(row[key])

    # finally, compress them into a set:
    column_dict_set = {}

    print num_samples

    for key in column_dict:
        potential_values = list(set(column_dict[key]))
        column_dict_set[key] = potential_values

    to_return = []

    for key in column_dict_set:
        values = copy.copy(column_dict_set[key])
        new_obj = { 'name': key, 'size': num_samples, 'values': values}
        to_return.append(new_obj)

    return to_return

result = returnInfo('../../projects/sample_project/raw/TCGA_OV_raw_data.txt')

with open('ov_output.txt', 'w') as output:
    for r in result:
        output.write(str(r))
        output.write('\n')

print "done"
