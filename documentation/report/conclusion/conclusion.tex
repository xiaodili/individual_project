\chapter{Conclusion}

\label{ch:Conclusions}

\section{Summary of Thesis Achievements}

An understanding of microarray technology, the existence of current biomedical research platforms and how statistical and machine learning techniques was gained. The importance of standardisation of microarray data (e.g. with MIAME) and clinical vocabulary was appreciated. Furthermore, insight into the current standard of web development and practices and how they can improve the user experience and workflow was gained.

An exploratory tool exposed feature selection methods with the purpose of investigating new microarray biomarkers from clinical and microarray cancer data, was developed. The user interface for the tool was implemented using native web technologies and, with the AngularJS framework, allowed for a rich user experience to be created. The results of each selection process could be chained so that the elimination of features became an iterative process. Selection by MAD, variance, univariate filtering and recursive feature elimination with SVM were integrated. Finally, integration with the Reflect API allowed for results to be instantaneously looked up in existing literature.

Considerations in how the tool could be made extensible were given. Test suites were set up for the client and server components. It is hoped that with the addition of more analysis tools and clinician's feedback, it might one day become a cornerstone in a researcher's toolbox.

\section{Future Work}

There are several ways the tool can be further improved upon:

\subsection{Optimisations}
\label{sub:Optimisations}

Once in production, to reduce the amount of \texttt{GET} requests made to server in order to obtain the required JavaScript files, the code base can be `minified' such that they are concatenated into the smallest number of files possible; local variables are also renamed and unnecessary whitespace trimmed so that the minified file is as small as possible. Tools such as AngularJS's ngmin module and Google's Closure Compiler can be used to achieve this goal.

\subsection{Technical debt and refactoring}
\label{sub:Code structure}

There are a few areas of the tool identified to be candidates for refactoring:

\begin{enumerate}
    \item Whether a feature is numerical or categorical should be encoded as enumerations in the client (right now, the strings 'Numerical' and 'Categorical' are being used to represent them).
    \item The two pre-processing functions in \texttt{routes} should be refactored into just one, as some of the functionality is duplicated. Additionally, they should be refactored in such a way that other techniques can easily make use of them.
\end{enumerate}

Additionally, due to the specifications of the tool not being firmly set, code coverage in testing could be greatly improved upon.

\subsection{Data analysis and other features}
\label{sub:Data analysis and other features}

In the analysis of variance, there is an assumption that the microarray data is normally distributed. If this is not the case, the ANOVA  technique exposed by the scikit-learn library will not work well. In any case, a microarray plotter should be implemented in order for the user to easily see whether what analysis technique is most suited, and non-parametric tests such as the Mann–Whitney U (for two groups) and the Kruskal-Wallis test can also be integrated.

Another addition to the feature selection workflow is feature reduction - that is, how it addresses the issue of genes that are functionally similar to each other may be mass selected or eliminated at the expense of functionally different genes. In this case, a pathway analysis might prove to be more helpful in regards to identifying the broader functional role of a gene. Integration with pathway tools such as Panther can be investigated, as well as dimensionality reduction techniques.

Other microarray formats, particularly those with one or two extra rows of headers but consistent bodies (e.g. GCT), should be made compatible with the tool, as this can be done with minimal effort.

The concept of reproducibility and transparency are important concepts, as advocated by the Galaxy Project. Currently, feature selection steps can be viewed using a unique URI:  \texttt{projects/\-<project\_id>/selection\_results/<step\_index>}, which can be shared with other users. However, selection steps can be easily deleted and new ones can take their place, so it would be better for each selection step to have their own unique identifier. Moreover, it would also be instructive for unfruitful selection steps to be stored as well, so that the same steps are not retraced. An alternative to the linear arrangement of conventional workflows would be a tree structure, displaying all branches of selection exploration. Ways to implement this would be utilise a new \texttt{Results} MongoDB collection, and to use a vector based charting library such as D3js for the visualisation.

\subsection{Integration with tranSMART over the proposed RESTful API}
\label{sub:Integration with tranSMART over the proposed RESTful API}

TranSMART provides the ideal source of integrated data, as demonstrated by the ease at which the tool can consume its data. In preparation for its announced REST API \cite{transmart_rest}, it can be seen that there are proposals to read information out of the database. For example, \texttt{GET /studies/<studyId>/subjects} returns the URIs of all the subjects present in a given study. Investigation into how clinical and gene expression data may be exported can streamline obtaining the required data.

Another worthwhile project would be to integrate the visual cues and trappings of the tool into the tranSMART web interface, as some of the UI elements can be somewhat obscure without tooltips for guidance. This is true for other platforms such as Galaxy as well, which have integrated many of the other analysis tools. Exploration into how a new visual layer can be built over the tranSMART, using to an established framework such as Bootstrap to achieve a consistent look and feel across different platforms could also be investigated.
