angular.module('app.controllers').controller("projectsCtrl", [
    '$scope',
    '$http',
    '$modal',
    function($scope,
             $http,
             $modal
            ) {

    var getProjects = function() {
        $scope.loading = true;

        $http.get("/projects").
        success(function(resp) {
            $scope.projects = resp.data;
            $scope.loading = false;
        }).
        error(function(data) {
            console.log(data);
            $scope.loading = false;
        });
    };
    //for initialisation, just need to call getProjects()
    getProjects();

    $scope.deleteProject = function(_id) {

        var callback = function() {

            var url = '/projects/' + _id;

            $http({
                method: 'DELETE',
                url: url
            })
            .success(function(resp) {
                getProjects();
            })
            .error(function(resp) {
                console.log('resp: ', resp);
            });
        }



        var modalInstance = $modal.open({
            templateUrl: 'partials/modal.html',
            controller: 'modalCtrl',
            resolve: {
                info: function() {
                    return {
                        header: "Warning",
                        callback: callback,
                        message: "Are you sure you want to delete this project?"
                    }
                }
            }
        });
    };



}]);
