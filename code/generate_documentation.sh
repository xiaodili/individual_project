#!/bin/bash

DOC_PATH="../documentation/generated/js"

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
JS="/server/static/js/"
JS_DIR=$DIR$JS

FILES=(*.js)

DEST_DIR=$DIR"/../documentation/generated/js"

docco -o $DEST_DIR $JS_DIR$FILES
