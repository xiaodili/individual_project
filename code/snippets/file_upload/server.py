from flask import Flask, request, jsonify
from pprint import pprint

# to get the file path
import os

# for file uploads
from werkzeug.utils import secure_filename

#app = Flask(__name__)

UPLOAD_FOLDER = './uploads'

# Serve requests from / with the static directory
app = Flask(__name__, static_url_path='')
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

ALLOWED_EXTENSIONS = set(["txt", "sql"])

# Serve index.html
@app.route('/')
def root():
    return app.send_static_file('index.html')

@app.route('/test_upload', methods=['POST'])
def testPost():

    def allowed_file_name(filename):
        return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

    file = request.files['file']

    if file and allowed_file_name(file.filename):
        filename = secure_filename(file.filename)
        print os.path.join(app.config['UPLOAD_FOLDER'], filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

    # project name is given in this field:
    #print request.form['myObj']

    #data = request.form
    #print 'printing request data...'
    #pprint(data)

    response = {}
    response['dogs'] = 'yep, working nicely'

    return jsonify(**response)

if __name__ == '__main__':
    
    # Turn on debugging mode
    app.debug = True

    # Run the app
    #app.run(port=6000)
    #app.run(port=5000)
    app.run()
