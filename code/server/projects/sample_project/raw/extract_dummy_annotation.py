import csv

annotation = ["column1\tcolumn2"]

# map everything to itself

with open('TCGA_OV_gene_expression.txt', 'r') as contents:
    reader = csv.reader(contents, delimiter='\t')
    headers = reader.next()
    for r in reader:
        annotation.append(r[0] + '\t' + r[0])

with open('annotation_dummy.txt', 'w') as output:
    for a in annotation:
        output.write(a + '\n')
