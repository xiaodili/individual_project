from helpers import toDict
from numpy import nan

# private data munging function for raw data
#--------------------------------------

def rawProcess(data):

    #MISSING_VALUE = None
    MISSING_VALUE = nan

    def bcPatientBarCode(barcodes):
        # do nothing; it needs to be processed in the gene_expression file and joined from there
        return barcodes

    def ageAtDiagnosis(ages):
        # just convert these into numbers. Assign None if age is missing
        return [float(a) if a else MISSING_VALUE for a in ages]

    def vitalStatus(statuses):
        # 0 for dead, 1 for alive, None for unknown

        vit_map = {
                '': MISSING_VALUE,
                "LIVING": 1,
                "DECEASED": 0
        }

        return [vit_map[s] for s in statuses]

    # taking these to be linear in seriousness:
    #http://en.wikipedia.org/wiki/Cancer_staging

    def tumorStage(tumors):

        #Tumors are graded IIA, IIB, IIC, IIIA, IIIB, IIIC, IV. "" included
        stage_map = {
                '': MISSING_VALUE,
                'IIA': 0,
                'IIB': 1,
                'IIC': 2,
                'IIIA': 3,
                'IIIB': 4,
                'IIIC': 5,
                'IV': 6
        }

        return [stage_map[t] for t in tumors]

    def tumorGrade(tumors):

        #Gradings are either G2, G3. "" included
        grade_map = {
                '': MISSING_VALUE,
                "G2": 0,
                "G3": 1
        }

        return [grade_map[t] for t in tumors]

    def residualDisease(residues):

        # Features are either 11-20mm, 1-10 mm, No Marcoscopic disease, > 20mm. "" included

        residue_map = {
                '': MISSING_VALUE,
                "No Macroscopic disease": 0,
                "1-10 mm": 1,
                "11-20 mm": 2,
                ">20mm": 3
        }

        return [residue_map[r] for r in residues]

    def primaryTherapyOutcomeSuccess(outcomes):

        outcome_map = {
                '': MISSING_VALUE,
                "COMPLETE RESPONSE": 0,
                "PARTIAL RESPONSE": 1,
                "STABLE DISEASE": 2,
                "PROGRESSIVE DISEASE": 3
        }
        return [outcome_map[o] for o in outcomes]

    def personNeoplasmCancerStatus(neoplasms):

        neoplasm_map = {
                '': MISSING_VALUE,
                "TUMOR FREE": 0,
                "WITH TUMOR": 1
        }
        return [neoplasm_map[n] for n in neoplasms]

    def overallSurvival(survivals):

        #Positive floats, "" included
        return [float(a) if a else MISSING_VALUE for a in survivals]

    def progressionFreeStatus(statuses):

        status_map = {
                '': MISSING_VALUE,
                "DiseaseFree": 0,
                "Recurred/Progressed": 1
        }

        return [status_map[s] for s in statuses]

    def progressionFreeSurvival(months):

        #Positive floats, no empty strings, but 'Missing instead'
        return [float(m) if m != "Missing" else MISSING_VALUE for m in months]

    def platinumFreeInterval(months):

        #Positive floats, "" included
        return [float(m) if m else MISSING_VALUE for m in months]

    def platinumStatus(statuses):

        status_map = {
                "Missing": MISSING_VALUE,
                "Resistant": 0,
                "Tooearly": 1,
                "Sensitive": 2
        }
        return [status_map[s] for s in statuses]


    # transpose:
    data_transpose = map(list, zip(*data))

    #0 - barcodes
    data_transpose[0] = bcPatientBarCode(data_transpose[0])
    #1 - age at diagnosis
    data_transpose[1] = ageAtDiagnosis(data_transpose[1])
    #2 - vital status
    data_transpose[2] = vitalStatus(data_transpose[2])
    #3 - tumor stage
    data_transpose[3] = tumorStage(data_transpose[3])
    #4 - tumor grade
    data_transpose[4] = tumorGrade(data_transpose[4])
    #5 - tumor residual disease
    data_transpose[5] = residualDisease(data_transpose[5])
    #6 - primary therapy outcome success
    data_transpose[6] = primaryTherapyOutcomeSuccess(data_transpose[6])
    #7 - person neoplasm cancer status
    data_transpose[7] = personNeoplasmCancerStatus(data_transpose[7])
    #8 - Overall survival (months)
    data_transpose[8] = overallSurvival(data_transpose[8])
    #9 - Progression free status
    data_transpose[9] = progressionFreeStatus(data_transpose[9])
    #10 - Progression free Survival
    data_transpose[10] = progressionFreeSurvival(data_transpose[10])
    #11 - Platinum free interval
    data_transpose[11] = platinumFreeInterval(data_transpose[11])
    #12 - Platinum status
    data_transpose[12] = platinumStatus(data_transpose[12])

    #return the transpose back:
    return map(list, zip(*data_transpose))



#------------------------------------

# exposed method:
def raw_process(path):

    with open(path) as contents:
        lines = contents.read().strip().split('\n')

    feature_names = [f.strip() for f in lines.pop(0).split('\t')]

    data = [row.strip().split('\t') for row in lines]

    data_ready = rawProcess(data)

    raw_data = toDict(data_ready)

    return feature_names, raw_data

# testing:
if __name__ == "__main__":
    from pprint import pprint
    path = "data/TCGA_OV_raw_data.txt"
    feature_names, raw_data = raw_process(path)
    pprint(feature_names)
    pprint(raw_data)
