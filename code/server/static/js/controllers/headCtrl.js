angular.module('app.controllers').controller("headCtrl", [
    '$scope',
    '$location',
    function($scope, $location) {

	//initialise at whatever page it's on:
	$scope.curr_page = $location.$$path.slice(1);
	
	$scope.$on('$locationChangeStart', function(current, previous) {

		$scope.curr_page = $location.$$path.slice(1);
		
	});
    
    $scope.toHome = function() {
        var path = '/projects'
        $location.path(path);
    }

    $scope.log = function() {
        console.log('$location: ', $location);
    }
    $scope.isActive = function(base_path) {
        if ($location.$$path.split('/')[1] == base_path) {
            return true;
        }
        else {
            return false;
        }
    }

}]);
